CREATE OR REPLACE FUNCTION primus5(jobbnummer OUT NUMBER, jobnummer_in NUMBER)
RETURN VARCHAR2
AUTHID CURRENT_USER
IS
BEGIN
  dbms_output.enable;
  BEGIN
    jobbnummer := 1;
    IF jobbnummer_in <= jobbnummer
    THEN
      EXECUTE IMMEDIATE
      'ALTER TABLE SIGNATUR_GRUPPE ADD
      ( OPT_MOTTAK VARCHAR2(2 CHAR)
      , OPT_UTLEVERING VARCHAR(2 CHAR))';
      COMMIT;
      EXECUTE IMMEDIATE 'UPDATE SIGNATUR_GRUPPE SET OPT_MOTTAK = ''-'', OPT_UTLEVERING = ''-''';
      COMMIT;
    END IF;
    jobbnummer := 2;
    IF jobbnummer_in <= jobbnummer
    THEN
      EXECUTE IMMEDIATE
      'ALTER TABLE DOKUMENT ADD
      ( TITTEL VARCHAR2(100 CHAR)
      , PRODUSERT_DATO DATE
      , PRODUSENTID NUMBER(8,0)
      , HID_OPPRETTET NUMBER(8,0)
      , HID_SLETTET NUMBER(8,0)
      , CONSTRAINT FK_DOKUMENT_PRODUSENTID FOREIGN KEY (PRODUSENTID)
          REFERENCES JURPERSON (OBJID)
      , CONSTRAINT FK_DOKUMENT_HID_OPPRETTET FOREIGN KEY (HID_OPPRETTET)
          REFERENCES HENDELSE (HID) ENABLE
      , CONSTRAINT FK_DOKUMENT_HID_SLETTET FOREIGN KEY (HID_SLETTET)
          REFERENCES HENDELSE (HID) ENABLE)';
      COMMIT;
    END IF;
    jobbnummer := 3;
    IF jobbnummer_in <= jobbnummer
    THEN
      EXECUTE IMMEDIATE 'ALTER TABLE DOKUMENT MODIFY (BESKRIVELSE VARCHAR2(1000 CHAR))';
      COMMIT;
    END IF;
    --     jobbnummer := 4;
    --     IF jobnummer_in <= jobbnummer
    --     THEN
    --       EXECUTE IMMEDIATE
    --       'CREATE TABLE USER_SETTINGS_LEVEL_L
    --       (	LEVEL_ID NUMBER NOT NULL ENABLE
    -- 	    ,  CODE VARCHAR2(20 BYTE) NOT NULL ENABLE
    -- 	    ,  NAME VARCHAR2(100 BYTE) NOT NULL ENABLE
    -- 	    ,  DESCRIPTION VARCHAR2(255 BYTE)
    -- 	    , CONSTRAINT PK_USER_SETTINGS_LEVEL PRIMARY KEY (LEVEL_ID))';
    --       COMMIT;
    --     END IF;
    --     jobbnummer := 5;
    --     IF jobnummer_in <= jobbnummer
    --     THEN
    --       EXECUTE IMMEDIATE 'INSERT INTO USER_SETTINGS_LEVEL_L (LEVEL_ID,CODE,NAME,DESCRIPTION) values (1,''system'',''Systemnivå'',''Innstillinger på systemnivå er definert av applikasjonen, og er standard innstilling hvis det ikke eksisterer innstillinger på lavere nivåer.'')';
    --       EXECUTE IMMEDIATE 'INSERT INTO USER_SETTINGS_LEVEL_L (LEVEL_ID,CODE,NAME,DESCRIPTION) values (2,''organization'',''Organisasjonsnivå'',''Innstillinger på organisasjonsnivå er definert av administrator. Hvis en innstilling ikke eksisterer på rollenivå eller brukernivå, vil den settes til organisasjonsnivå, hvis tilgjengelig.'')';
    --       EXECUTE IMMEDIATE 'INSERT INTO USER_SETTINGS_LEVEL_L (LEVEL_ID,CODE,NAME,DESCRIPTION) values (3,''role'',''Rollenivå'',''Innstillinger på rollenivå settes av administrator. Hvis en innstilling ikke eksisterer på brukernivå, vil den settes til rollenivå, hvis den eksisterer.'')';
    --       EXECUTE IMMEDIATE 'INSERT INTO USER_SETTINGS_LEVEL_L (LEVEL_ID,CODE,NAME,DESCRIPTION) values (4,''user'',''Brukernivå'',''Innstillinger på brukernivå settes av brukeren selv. Hvis en innstilling ikke eksisterer på brukernivå, vil den settes til enten rollenivå, organisasjonsnivå eller til slutt systemnivå hvis nivåene under ikke eksisterer.'')';
    --       COMMIT;
    --     END IF;
    RETURN 'OK';
    EXCEPTION
    WHEN OTHERS THEN
    ROLLBACK;
    SYS.DBMS_OUTPUT.PUT_LINE('Primus5 feilet! Jobbnummer: ' || jobbnummer);
    PRIMUSOPPGRADERINGSTOPP(5, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
    RETURN 'FEIL!';
  END;

END;

/

create or replace function primus62(jobbnummer out number, jobbnummer_inn number) 
	return varchar2
	AUTHID CURRENT_USER
	is

  newHID number;
begin  
  dbms_output.enable;
  begin
    jobbnummer := 1;
    
    select hidseq.nextval into newHID from dual;
    insert into hendelse (hid, dato, signid ) 
                  values ( newHID, sysdate, ( SELECT SIGNID from signatur where sign = 'PRIMUS' ) );
    jobbnummer := 2;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 62, jobbnummer );
      execute immediate 'alter table adm_hendelsetype_l add (checkfordelete varchar2( 1 CHAR )
                                     , MERKE varchar2( 1 char ) default ''Y'' not null)';
                                   
    end if;                                 


    --- legg inn jpnr referanse i tittel/objekt_navn.
    jobbnummer := 3;
    if jobbnummer_inn <= jobbnummer then  
      primusOppdJobbNrIVersjon( 62, jobbnummer );
      
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table objekt_navn add ( jpnr number( 8 )
                            , constraint fk_objektnavn_jurperson foreign key (jpnr) references primus.jurperson ( jpnr ) enable )
                        ');
    
    end if;

    --- legger inn undertyper til utlån.
    jobbnummer := 4;
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 62, jobbnummer );
      
      -- Utlån - beholdes uendret
      
      -- Innlån / Inlån - legges til
      IF svenskEllerNorskDatabase = 'NOR' THEN
        insert into adm_hendelse_undertype_l ( typeid, tekst, parent_id )
        values ( (select max(typeid) + 1 from adm_hendelse_undertype_l), 'Innlån', 101);
      else
        insert into adm_hendelse_undertype_l ( typeid, tekst, parent_id )
        values ( (select max(typeid) + 1 from adm_hendelse_undertype_l), 'Inlån', 101);
      end if;
      
      -- Deponert inn / Deposition inn - legges til
      IF svenskEllerNorskDatabase = 'NOR' THEN
        insert into adm_hendelse_undertype_l ( typeid, tekst, parent_id )
        values ( (select max(typeid) + 1 from adm_hendelse_undertype_l), 'Deponert inn', 101);
      else
        insert into adm_hendelse_undertype_l ( typeid, tekst, parent_id )
        values ( (select max(typeid) + 1 from adm_hendelse_undertype_l), 'Deposition in', 101);
      end if;

      -- Deponert ut / Deposition ut - oppdaterer eksisterende med ny tekst.
      IF svenskEllerNorskDatabase = 'NOR' THEN
        update adm_hendelse_undertype_l 
           set tekst = 'Deponert ut'
         where tekst = 'Depositum'
           and parent_id = 101;
      else
        update adm_hendelse_undertype_l 
           set tekst = 'Deposition ut'
         where tekst = 'Deposition'
           and parent_id = 101;
      end if;
    end if;
    
    --- Nye rollekoder for personer som er besluttningstakere.
    jobbnummer := 5;
    if jobbnummer_inn <= jobbnummer then
      primusOppdJobbNrIVersjon( 62, jobbnummer );
      
      if svenskEllerNorskDatabase = 'NOR' then
        insert into rollejp_l
         ( rollekode, beskrivelse, rolleid, checkfordelete, merke, uuid, hid_opprettet )
         values
         ( '246', 'Ansvarlig for vedtak', 246, 'X', 'Y', 'b748256c-3fef-4502-9f92-23adbecdff61', newHID ); 
      else
        insert into rollejp_l
         ( rollekode, beskrivelse, rolleid, checkfordelete, merke, uuid, hid_opprettet )
         values
         ( '246', 'Ansvarig for beslut', 246, 'X', 'Y', 'b748256c-3fef-4502-9f92-23adbecdff61', newHID ); 
      end if;
    end if;
    
    
    --- legg inn logikk for jpnr knyttet til påført tekst. Mulig 1:N tabell er nødvendig.
    --- sjekk denne opp mot getty 

    
    jobbnummer := 100;
		commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus62 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(62, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
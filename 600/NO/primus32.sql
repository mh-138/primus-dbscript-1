CREATE OR REPLACE FUNCTION primus32(jobbnummer OUT NUMBER, jobnummer_in NUMBER)
  RETURN VARCHAR2
AUTHID CURRENT_USER
IS
  BEGIN
    dbms_output.enable;

    /*
    Intervall tabeller
    */

    BEGIN
      jobbnummer := 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'create table intervall_l
          ( typeid number(3, 0)
          , tekst varchar2(50 char) not null enable
          , filterfelt varchar(5 char)
          , constraint pk_intervall_l primary key (typeid)
        ) tablespace primusdt';
      END IF;
      jobbnummer := 2;
      IF jobnummer_in <= jobbnummer
      THEN
        IF svenskEllerNorskDatabase = 'NOR'
        THEN
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(1, ''Daglig'', ''GJNTG'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(2, ''Ukentlig'', ''GJNTG'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(3, ''Månedlig'', ''GJNTG'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(4, ''Årlig'', ''GJNTG'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(5, ''Dag i måneden'', ''MANED'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(6, ''Dag i uka'', ''MANED'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(7, ''Aldri'', ''STOP'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(8, ''Etter'', ''STOP'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(9, ''Dato'', ''STOP'')';
        ELSE
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(1, ''Dagligen'', ''GJNTG'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(2, ''Varje vecka'', ''GJNTG'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(3, ''Varje månad'', ''GJNTG'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(4, ''Årligen'', ''GJNTG'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(5, ''Dag i månaden'', ''MANED'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(6, ''Veckodag'', ''MANED'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(7, ''Aldrig'', ''STOP'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(8, ''Efter'', ''STOP'')';
          EXECUTE IMMEDIATE 'insert into intervall_l(typeid, tekst, filterfelt) values(9, ''Datum'', ''STOP'')';
        END IF;
      END IF;
      jobbnummer := 3;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'create sequence intervallidseq INCREMENT BY 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCACHE ';
      END IF;
      jobbnummer := 4;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'create table intervall
          ( intervallid number(8,0)
            , hid number(8,0)
            , hid_slettet number(8,0)
            , intervalltypeid number(8,0)
            , intervall number(2,0)
            , dager number(3,0)
            , maanedtypeid number(8,0)
            , startdato date
            , stoptypeid number(8,0)
            , antall number(5,0)
            , teller number(5,0)
            , stopdato date
            , constraint pk_intervall primary key (intervallid)
            , constraint fk_intervall_hendelse foreign key (hid)
                references hendelse (hid) enable
            , constraint fk_intervall_hendelse_s foreign key (hid_slettet)
                references hendelse (hid) enable
            , constraint fk_intervall_type_id foreign key (intervalltypeid)
                references intervall_l (typeid) enable
            , constraint fk_intervall_stop_type_id foreign key (stoptypeid)
                references intervall_l (typeid) enable
          ) tablespace primusdt';

      END IF;
      jobbnummer := 5;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'create table adm_hend_intervall
          ( objid NUMBER(8,0) NOT NULL ENABLE
            , intervallid NUMBER(8,0) NOT NULL ENABLE
            , CONSTRAINT PK_A_HEND_INTERVALL PRIMARY KEY (objid, intervallid)
            , CONSTRAINT FK_A_HEND_INTERVALL_OBJID FOREIGN KEY (objid)
                REFERENCES adm_hendelse (objid)
            , CONSTRAINT FK_A_HEND_INTERVALL_IID FOREIGN KEY (intervallid)
                REFERENCES intervall (intervallid) ENABLE
        ) tablespace primusdt';

      END IF;
      jobbnummer := 6;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE
        'insert into superobjekt_status_l (statusid, statustekst, superobjekttypeid, uuid)
         values(4, ''Planlagt'', 3, getoracleuuid())';

      END IF;
      jobbnummer := 7;
      IF jobnummer_in <= jobbnummer
      THEN
        IF svenskEllerNorskDatabase = 'NOR'
        THEN
          EXECUTE IMMEDIATE
          'update superobjekt_status_l set statustekst=''Ny'' where statusid=1';
        ELSE
          EXECUTE IMMEDIATE
          'update superobjekt_status_l set statustekst=''Nytt'' where statusid=1';
        END IF;
      END IF;
      jobbnummer := 8;
      COMMIT;
      jobbnummer := 100;
      RETURN 'OK';
      EXCEPTION
      WHEN OTHERS THEN
      SYS.DBMS_OUTPUT.PUT_LINE('Primus32 feilet! Jobbnummer: ' || jobbnummer);
      PRIMUSOPPGRADERINGSTOPP(32, 'FEIL', jobbnummer,
                              'Oppgradering feilet: ' || SQLERRM);
      RETURN 'FEIL!';
    END;

  END;
/
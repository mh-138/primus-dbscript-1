create or replace function primus59(jobbnummer out number, jobbnummer_inn number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
    Oppretter nye funksjoner for "hovedfeltet"
	*/
	begin
    jobbnummer := 1;
    if jobbnummer_inn <= jobbnummer then
      execute immediate( '
      create or replace type num_array as table of number(3);
      ');
    end if;
	  
    jobbnummer := 2; 
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function XML_ELEMENT_TILSTAND(objid_in number) return xmltype as objectxml xmltype;
      begin
        SELECT XMLAGG(XMLELEMENT(tilstand, XMLFOREST( pt.tilstandskode as tilstandskode
             , tl.navn as tilstand
             , pt.beskrivelse as tilstandbeskrivelse )))
          INTO objectxml
             from (
                select oah.objid
                     , ah.objid as admobjid
                     , ah.hid_tilstand as hidTilst
                     , sod.fradato
                     , sod.tildato
                from objekt_adm_hendelse oah
                inner join adm_hendelse ah
                        on ah.objid = oah.admh_objid
                       and ah.hovedtype = 11
                       and ah.hid_slettet is null
                inner join superobjekt_datering_h sod
                        on sod.objid = ah.objid
                       and sod.hid = ah.hid_datering
                where oah.objid_siste = objid_in
                  and oah.hid_slettet is null
                order by sod.fradato desc) gyldig
        left outer join Tilstand pt
                     on pt.objid = gyldig.admobjid
                    and pt.hid = Gyldig.hidTilst
        left outer join tilstand_l tl
                     on tl.kode = pt.tilstandskode
        where rownum = 1;
        
        return Objectxml;
      end;
      ');
    end if;
    
    jobbnummer := 3;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function xml_element_datering(objid_inn number, array_inn num_array) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
      begin
        SELECT XMLAGG(
                 XMLELEMENT( PDATERING
                           , XMLFOREST( CONVERTDATE( pdh.DATO_FRA, SUBSTR( TO_CHAR( pdh.DATO_FRA, ''SYYYY'' ), 1, 5 ) ) AS DATOF 
                                      , CONVERTDATE( pdh.DATO_TIL, SUBSTR( TO_CHAR( pdh.DATO_TIL, ''SYYYY'' ), 1, 5 ) ) AS DATOT
                                      )
                           )
                     )
				  INTO OBJECTXML
          FROM PRIMUS.DATERING_H pdh 
             , PRIMUS.HISTORIKK ph
			   WHERE ph.HID_DATERING = pdh.HID
			     AND ph.HISTID = pdh.HISTID
			     AND ph.OBJID = objid_inn
			     AND ph.HID_SLETTET IS NULL
			     AND ph.HISTTYPE IN ( select * from table( array_inn ) );
			  
        return OBJECTXML;
      end;
      ');

    end if;
    
    jobbnummer := 4;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function xml_element_klausul( objid_inn number ) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
      begin
        SELECT XMLAGG(
                 XMLELEMENT( KLAUSUL
                           , XMLFOREST( RL.BESKRIVELSE AS KLAUSUL_TEKST 
                                      , RD.FRA_DATO AS FRADATO
                                      , RD.TIL_DATO AS TILDATO 
                                      )
                           )
                     )	
				  INTO objectxml
          FROM PRIMUS.RETTIGHET R
             , PRIMUS.RETTIGHET_L RL
             , PRIMUS.RETTIGHET_DATERING RD
				 WHERE R.OBJID = objid_inn 
				   AND R.RETTIGHETTYPEID = RL.RETTIGHETTYPEID
				   AND R.RETTIGHETID=RD.RETTIGHETID(+)
				   AND R.HID_KOMMENTAR=RD.HID(+)	
				   AND RL.typeid=3
				   AND R.HID_SLETTET  IS NULL
				   AND (RD.FRA_DATO < SYSDATE OR RD.FRA_DATO IS NULL)
				   AND (RD.TIL_DATO > SYSDATE OR RD.TIL_DATO IS NULL);
			  
        return objectxml;
      end;
      ');

    end if;

    jobbnummer := 5;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function XML_ELEMENT_JURPERSON( objid_inn number, array_inn num_array) RETURN xmltype as objectxml xmltype;
      begin
        SELECT XMLAGG( 
                 XMLELEMENT( JP_NAVN
                           , XMLFOREST( JP.JPNAVN AS NAVN 
                                      , RL.beskrivelse AS ROLLEBESKRIVELSE
                                      , RSL.JURPERSONSTATUS as JURPERSONSTATUS	
                                      )
                           )
                     )
				  INTO objectxml
          FROM primus.person_objekt po
             , primus.historikk h
             , primus.jurperson jP
             , primus.rollejp_l RL
             , ROLLEJPSTATUS_L RSL
				 WHERE po.histid_siste = h.histid 
				   AND po.rollekode = RL.rollekode
				   AND po.rolleid = RL.rolleid	
				   AND PO.ROLLEJPSTATUSID = RSL.STATUSID(+)
				   AND po.jpnr = jP.jpnr
				   AND po.histid_siste is not null
				   AND h.hid_slettet is null
				   AND h.OBJID = objid_inn
				   AND RL.ROLLEID IN ( select * from table( array_inn ) );
        
        return objectxml;
      end;
      ');
      
    end if;
    
    jobbnummer := 6;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function XML_ELEMENT_SAMLING(gruppeid_inn number) return xmltype as objectxml xmltype;
    	begin
        select XMLAGG( 
                 XMLELEMENT( GRUPPE
                           , XMLFOREST( grp.betegnelse as SAMLING
                                      , E.MUSEUM_NAVN as EIER
                                      )
                           )
                     )
				  INTO objectxml
          FROM PRIMUS.gruppe grp
             , PRIMUS.Eier E 
				 WHERE grp.gruppeid = gruppeid_inn 
				   AND grp.Eierid = E.Eierid;
        
        return objectxml;
      end;
      ');
      
    end if;
    
    jobbnummer := 7;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function XML_ELEMENT_REGNIVA( objid_inn number ) return xmltype as objectxml xmltype;
			begin
        SELECT XMLAGG( 
                 XMLELEMENT( REGNIVAA
                           , GT_L.BESKRIVELSE
                           )
                     )
				  INTO objectxml
          FROM PRIMUS.GRUPPERINGSTYPE_L GT_L
             , primus.gruppering_h gh
             , primus.objekt po 
				 WHERE GT_L.TYPEID = gh.TYPEID 
				   AND po.objid = gh.objid 
           AND po.hid_gruppering = gh.hid
           AND po.objid = objid_inn;
        
        return objectxml;

      end;
      ');
      

    end if;
    
    jobbnummer := 8;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function XML_ELEMENT_INGORI( objid_inn number ) return xmltype as objectxml xmltype;				
      begin
        SELECT XMLAGG( 
                 XMLELEMENT ( INGORI
                            , OBJEKT.MUSEUMSNR
                            )
                     )
				  INTO objectxml
          FROM PRIMUS.OBJEKT
             , PRIMUS.ANTALL 
				 WHERE ANTALL.OBJID = OBJEKT.OBJID 
           AND OBJEKT.HID_SLETTET IS NULL   
           AND ANTALL.OBJEKTID = objid_inn;
      
        return objectxml;
      end;
      ');
      
    end if;
    
    jobbnummer := 9;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function XML_ELEMENT_BETEGNELSE( objid_inn number ) return xmltype as objectxml xmltype;
      begin
        SELECT XMLAGG( 
                 XMLELEMENT( BETEGNELSE
                           , bl.beskrivelse 
                           )
                     )
				  INTO objectxml
          FROM primus.OBJEKT_BETEGNELSE obb
             , primus.betegnelse_l bl
				 WHERE obb.objid_siste = objid_inn 
				   AND obb.betid = bl.kode;
         
         return objectxml;
      end;
      ');
      
    end if;

    jobbnummer := 10;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function XML_ELEMENT_PRESBET( objid_inn number ) return xmltype as objectxml xmltype;
      begin
        SELECT XMLAGG( 
                 XMLELEMENT( PBETEGNELSE
                           , typ.beskrivelse
                           )
                     )   
          INTO objectxml           
			    FROM primus.objekt_type objt 
             , primus.type_l typ
			   WHERE objt.type=typ.kode 
			     AND objt.objid_siste = objid_inn;
        
        return objectxml;
      end;
      ');
      
    end if;
    
    jobbnummer := 11;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '    
        create or replace function xml_element_gabnr( objid_in number ) return xmltype as objectxml xmltype;
        begin
          select xmlelement( gabnr, xmlforest( pbgh.gabnr as gabnr ) ) 
          into objectxml
          from primus.bygning_gabnr_h pbgh
          inner join primus.bygning pb 
                  on pb.objid = pbgh.objid
                 and pb.hid_gabnr = pbgh.hid
                 and pb.objid = objid_in;
          return objectxml;
        end;
        ');
    end if;
        
    jobbnummer := 12;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
        create or replace function xml_element_bygningtype(objid_in number) return xmltype as objectxml xmltype;
        begin
          select xmlelement( bygningstype, pbtl.beskrivelse ) 
                 into objectxml
                 from bygning_bygningstype_h pbth 
                 inner join primus.bygning_bygningstype_l pbtl on pbth.bygningtypeid = Pbtl.Id
                 inner join primus.bygning pb 
                         on pb.objid = pbth.objid
                        and pb.hid_bygningstype = pbth.hid
                        and pb.objid = objid_in;
                 return objectxml;
        end;
        ' );
    end if;
        
    jobbnummer := 13;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
        create or replace function xml_element_bygningbetegnelse( objid_in number ) return xmltype as objectxml xmltype;
        begin
          select XMLAGG(XMLELEMENT(BBETEGNELSE,	XMLFOREST(bl.beskrivelse as BESKRIVELSE,  obb.KODE as BKODE)))
                  into objectxml
                  FROM primus.OBJEKT_BYGNINGBETEGNELSE obb, primus.betegnelse_l bl
                  WHERE obb.OBJID=objid_in 
                  AND obb.betid = bl.kode 
                  AND obb.objid_siste is not null;
          return objectxml;
        end;
      ' );
    end if;
    
    jobbnummer := 14;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '      
        create or replace function xml_element_objektbeskrivelse( objid_in number ) return xmltype as objectxml xmltype;
        begin
          select xmlelement( beskrivelse, pobh.beskrivelse )
            into objectxml
            from primus.obj_beskrivelse_h pobh
           inner join primus.objekt po 
                   on po.objid = pobh.objid
                  and po.hid_beskrivelse = pobh.hid
                  and po.objid = objid_in;
            
            return objectxml;
        end;
      ' );
    end if;
    
    jobbnummer := 15;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '      
        create or replace function xml_element_bygningvernetype(objid_in number) return xmltype as objectxml xmltype;
        begin
          select xmlelement( bygningvernetype, pbtl.beskrivelse  )
                 into objectxml
                 from primus.bygning_vernetype_h pbth 
                 inner join primus.bygning_vernetype_l pbtl on pbth.vernetypeid = Pbtl.Id
                 inner join primus.bygning pb 
                         on pb.objid = pbth.objid
                        and pb.hid_vernetype = pbth.hid
                        and pb.objid = objid_in;
                 return objectxml;
        end;
      ' );
    end if;
    
    jobbnummer := 16;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then
      execute immediate( '
        create or replace function xml_bygning( objid_in number ) return xmltype as objectxml xmltype;
        begin

          select xmlelement(objekt, 
                    xmlforest( ob.museumsnr as museumsnummer
                             , ob.objtypeid as objtypeid)
                    , xml_element_bygningvernetype( objid_in )
                    , xml_element_objektbeskrivelse( objid_in )
                    , xml_element_bygningbetegnelse( objid_in )
                    , xml_element_bygningtype( objid_in )
                    , xml_element_gabnr( objid_in )
                    , xml_element_regniva( objid_in )
                    , xml_element_samling ( ob.gruppe )
                    , xml_element_jurperson( objid_in,   num_array(14,15,16,18,181,191,26,64,67, 90, 961,103,100,101, 102,109,111,112,113,114,115, 116, 117, 118, 141,142,143, 76, 208, 213))
                    , xml_element_klausul( objid_in )
                    , xml_element_datering( objid_in, num_array(100))
                    , xml_element_tilstand( objid_in )
          )
          INTO objectxml 
          FROM PRIMUS.OBJEKT OB 
          WHERE OB.OBJID=OBJID_IN ;
                
          return objectxml;
        end xml_bygning;
        ');
    end if;

    jobbnummer := 17;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then
      execute immediate( 'create index idx_objekt_adm_hend_admobjid on primus.objekt_adm_hendelse( admh_objid ) tablespace primusix');
    end if;
    
    jobbnummer := 18;
    PrimusOppdJobbNrIVersjon( 59, jobbnummer );
    if jobbnummer_inn <= jobbnummer then
      execute immediate( 'create index idx_antall_objektid on primus.antall( objid ) tablespace primusix' );
    end if;
    jobbnummer:= 100;		
    return 'OK';
	exception
		when others then
		    ROLLBACK;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus59 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(59, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
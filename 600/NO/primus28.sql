create or replace function primus28(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
	Behandling metode
	*/

	begin
		jobbnummer:=1;
    if jobnummer_in <= jobbnummer then
      -- Adding field "description" to table BEHANDLING_METODE
      EXECUTE IMMEDIATE 'alter table behandling_metode add beskrivelse varchar2(1000)';
      jobbnummer:=2;
    End if;
		if jobnummer_in <= jobbnummer then
		  EXECUTE IMMEDIATE
        'create table behandling_metode_datering
          ( behmetodeid number (8, 0) not null
          , hid number(8,0) not null
          , fra_dato date
          , til_dato date
          , kommentar varchar2(1000 char)
          , constraint pk_behandling_metode_dtng primary key (behmetodeid, hid)
          , constraint fk_behandling_metode_dtng_bid foreign key (behmetodeid) references behandling_metode(behmetodeid)
          , constraint fk_behandling_metode_dtng_hid foreign key (hid) references hendelse(hid)
        ) tablespace primusdt';
      jobbnummer:=3;
    End if;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'create table behandling_metode_ressursbruk
          ( behmetodeid number (8, 0) not null
          , hid number(8,0) not null
          , ressursbruk varchar2(100)
          , constraint pk_behandling_metode_rssbruk primary key (behmetodeid, hid)
          , constraint fk_behandling_metode_rss_bid foreign key (behmetodeid) references behandling_metode(behmetodeid)
          , constraint fk_behandling_metode_rss_hid foreign key (hid) references hendelse(hid)
        ) tablespace primusdt';
      jobbnummer:=4;
    End if;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'create table behandling_metode_kostnad
          ( behmetodeid number(8, 0) not null
          , hid number(8, 0) not null
          , kostnad number(10, 2)
          , constraint pk_behandling_metode_kstnd primary key (behmetodeid, hid)
          , constraint fk_behandling_metode_kstnd_bid foreign key (behmetodeid) references behandling_metode(behmetodeid)
          , constraint fk_behandling_metode_kstnd_hid foreign key (hid) references hendelse(hid)
        ) tablespace primusdt';
      jobbnummer:=5;
    End if;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'create table behandling_metode_beskrivelse
          ( behmetodeid number(8, 0) not null
          , hid number(8, 0) not null
          , beskrivelse varchar2(1000 char)
          , constraint pk_behandling_metode_besk primary key (behmetodeid, hid)
          , constraint fk_behandling_metode_besk_bid foreign key (behmetodeid) references behandling_metode(behmetodeid)
          , constraint fk_behandling_metode_besk_hid foreign key (hid) references hendelse(hid)
        ) tablespace primusdt';
      jobbnummer:=6;
    End if;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'alter table behandling_metode add
          ( hid_slettet number(8,0)
          , hid_datering number(8,0)
          , hid_ressursbruk number(8,0)
          , hid_kostnad number(8,0)
          , hid_beskrivelse  number(8,0)
          , constraint fk_hid_slettet foreign key (hid_slettet) references hendelse(hid)
          , constraint fk_BM_hid_datering foreign key (behmetodeid, hid_datering) references behandling_metode_datering(behmetodeid, hid)
          , constraint fk_BM_hid_ressursbruk foreign key (behmetodeid , hid_ressursbruk) references behandling_metode_ressursbruk(behmetodeid, hid)
          , constraint fk_BM_hid_kostnad foreign key (behmetodeid , hid_kostnad) references behandling_metode_kostnad(behmetodeid , hid)
          , constraint fk_BM_hid_beskrivelse foreign key (behmetodeid , hid_beskrivelse) references behandling_metode_beskrivelse(behmetodeid , hid)
          )';
      jobbnummer:=7;
    End if;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE 'alter table behandling_metode drop (ressursbruk, kostnad, beskrivelse)';
    End if;
		jobbnummer:=100;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus28 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(28, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
create or replace function primus35(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
    2013.09.27:
    Table and sequence necessary to make task tracking possible.
    
	Changes necessary to the Primus database for PrimusWeb to work
    2013.09.27:
    Table and sequence necessary to make task tracking possible.
	*/

	begin

    jobbnummer:=1;  
	if jobnummer_in <= jobbnummer then
	 begin
      EXECUTE IMMEDIATE 'create sequence system_oppgaveseq INCREMENT BY 1 MAXVALUE 999999999999999999999999999 MINVALUE 1 NOCACHE ';
	 Exception
	   WHEN OTHERS THEN
        IF SQLCODE = -955 THEN
          NULL; -- suppresses ORA-00955 exception
        ELSE
         RAISE;
        END IF;
	 End;	
     End if;	  
	jobbnummer:=2;  
    if jobnummer_in <= jobbnummer then
    begin
       EXECUTE IMMEDIATE
        'create table system_oppgave(
          oppgaveid number(8,0) primary key not null,
          oppgave_type varchar2(50 Char) not null,
          registrert_sign varchar2(8 Char) not null,
          registrert_dato timestamp not null,
          posisjon number(8,0),
          antall number(8,0),
          endret_dato timestamp null,
          ferdig_dato timestamp null,
          beskrivelse varchar2(512 Char) null
        )';
	 Exception
	   WHEN OTHERS THEN
        IF SQLCODE = -955 THEN
          NULL; -- suppresses ORA-00955 exception
        ELSE
         RAISE;
        END IF;
	 End;		
    End if;	 




	jobbnummer:=3;
    if jobnummer_in <= jobbnummer then
	 begin
      EXECUTE IMMEDIATE
        'CREATE SEQUENCE  ETIKETTIDSEQ  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 109 NOCACHE  NOORDER  NOCYCLE';
    Exception
	   WHEN OTHERS THEN
        IF SQLCODE = -955 THEN
          NULL; -- suppresses ORA-00955 exception
        ELSE
         RAISE;
        END IF;
	 End;		
    End if;	 
	jobbnummer:=4;
	if jobnummer_in <= jobbnummer then
	 begin
      EXECUTE IMMEDIATE
        'CREATE TABLE ETIKETT
          (	ETIKETTID NUMBER(8,0) NOT NULL ENABLE,
            ETIKETT VARCHAR2(100 CHAR) NOT NULL ENABLE,
            UUID VARCHAR2(40 char) NOT NULL ENABLE,
            AUTORITET VARCHAR2(10 char),
            AUTORITET_STATUS VARCHAR2(1 char),
            HID_OPPRETTET NUMBER(8,0),
            HID_SLETTET NUMBER(8,0),
            AUTORITET_DATASET VARCHAR2(100 char),
            AUTORITET_KILDE VARCHAR2(100 char),
            CONSTRAINT I01_ETIKETT PRIMARY KEY (ETIKETTID),
            CONSTRAINT UK_ETIKETT_UUID UNIQUE (UUID)
        )';
		
		Exception
	   WHEN OTHERS THEN
        IF SQLCODE = -955 THEN
          NULL; -- suppresses ORA-00955 exception
        ELSE
         RAISE;
        END IF;
	 End;		
    End if;	 
	jobbnummer:=5;
    if jobnummer_in <= jobbnummer then
	 begin
      EXECUTE IMMEDIATE
        'CREATE TABLE OBJEKT_ETIKETT
          (	OBJID NUMBER(8,0) NOT NULL ENABLE,
            HID NUMBER(8,0) NOT NULL ENABLE,
            NR NUMBER(7,0) NOT NULL ENABLE,
            ETIKETTID NUMBER(8,0),
            OBJID_SISTE NUMBER(8,0),
            CONSTRAINT I01_OBJ_ETIKETT PRIMARY KEY (OBJID, HID, NR),
            CONSTRAINT ETIKETT_OBJ_ETIKETT FOREIGN KEY (ETIKETTID)
              REFERENCES ETIKETT (ETIKETTID) ENABLE,
            CONSTRAINT HENDELSE_OBJ_ETIKETT FOREIGN KEY (HID)
              REFERENCES HENDELSE (HID) ENABLE,
            CONSTRAINT SUPEROBJEKT_OBJ_ETIKETT1 FOREIGN KEY (OBJID_SISTE)
              REFERENCES SUPEROBJEKT (OBJID) ENABLE,
            CONSTRAINT SUPEROBJEKT_OBJ_ETIKETT FOREIGN KEY (OBJID)
              REFERENCES SUPEROBJEKT (OBJID) ON DELETE CASCADE ENABLE
        )';
   Exception
	   WHEN OTHERS THEN
        IF SQLCODE = -955 THEN
          NULL; -- suppresses ORA-00955 exception
        ELSE
         RAISE;
        END IF;
	 End;		
    End if;	 


	
	/*	

    jobbnummer:=19;	  executeScript(jobbnummer,jobnummer_in,'drop trigger ADM_HENDELSE_PERSON_AUTO_KEY');	
	
	jobbnummer:=9;
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('CREATE TABLE UTLAAN 
			(OBJID NUMBER (8,0)  NOT NULL ,
             ANTALL_OBJ NUMBER (3,0), 
			 CONSTRAINT PK_UTLAAN_OBJID PRIMARY KEY (OBJID),
			 CONSTRAINT FK_UTLAAN_OBJID FOREIGN KEY (OBJID) REFERENCES ADM_HENDELSE (OBJID)
              ) tablespace primusdt');
           End if;
	jobbnummer:=10;
		  if jobnummer_in <= jobbnummer then	
		    execute immediate ('CREATE TABLE UTSTILLING 
			(OBJID NUMBER (8,0)  NOT NULL , 
			 STATUS VARCHAR2 (5 CHAR),
			 CONSTRAINT PK_UTSTILLING_OBJID PRIMARY KEY (OBJID),
			 CONSTRAINT FK_UTSTILLING_OBJID FOREIGN KEY (OBJID) REFERENCES ADM_HENDELSE (OBJID)
              ) tablespace primusdt');
           End if;

  jobbnummer:=18;
	  executeScript(jobbnummer,jobnummer_in,'alter table ADM_HENDELSE_PERSON drop column ID');  





		  select skade_lidseq.nextval into skadeid from dual;
		  skadeidparent := skadeid;
		  execute immediate 'insert into skade_l(id, navn, parent_id, m_path, hid_opprettet, uuid)
		  values (:a, ''Mekanisk skade'', null, ''/''||:b, :c,''4f09cdbe-90c4-4046-93fa-fecd34b91e7e'')' using skadeid, skadeid, nhid;

		  select skade_lidseq.nextval into skadeid from dual;
		  execute immediate 'insert into skade_l(id, navn, parent_id, m_path, hid_opprettet, uuid)
		  values (:a, ''Avflaking'', :e, ''/''||:b||''/''||:c, :d, ''19981082-a2a6-40a3-9a7e-000039d0fcd1'')' using skadeid,skadeidparent, skadeidparent, skadeid, nhid;

		  select skade_lidseq.nextval into skadeid from dual;
		  execute immediate 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid)
		  values (:a, ''Avriving'', :e,  ''/''||:b||''/''||:c, :d, ''f6f6a338-dfce-45c2-b1fc-4d9d38e4e94f'')' using skadeid, skadeidParent, skadeidparent, skadeid, nhid;


		  select skade_lidseq.nextval into skadeid from dual;
		  execute immediate 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid)
		  values (:a, ''Annen skade'', null, ''/''||:b, :c,''7b6e76e5-3c16-4739-aa84-d5f95bf3eac2'')' using skadeid,  skadeid, nhid;

		  select skade_lidseq.nextval into skadeid from dual;
		  execute immediate 'insert into skade_l
			(id, navn, parent_id, m_path, hid_opprettet, uuid)
		  values
			(:a, ''Biologisk skade'', null,''/''||:b, :c,''3462a383-b154-45e7-951c-c00c4422a69f'')' using skadeid, skadeid, nhid;

		  select skade_lidseq.nextval into skadeid from dual;
		  execute immediate 'insert into skade_l
			(id, navn, parent_id, m_path, hid_opprettet, uuid)
		  values
			(:a, ''Kjemisk skade'', null, ''/''||:b, :c,''c99a0357-70b0-4e14-af3e-f831297b7ab6'')' using skadeid, skadeid, nhid;
		
		
			 
		
        select skade_lidseq.nextval into skadeid from dual;            
         execute immediate 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Skader'', null, ''/''||:b, :c,''8fc41d70-02b8-11e4-9191-0800200c9a66'')' using skadeid, skadeid, nhid;  
            
            -- retter opp m_path n�r det legges et nytt element under alle andre elementer
         execute immediate 'update skade_l set m_path = ''/'' || :a || m_path  where id <> :b' using skadeid, skadeid;
            
            -- retter opp parent_id for alle elementer
         execute immediate 'update skade_l set parent_id = :a where id <> :a and parent_id is null' using skadeid, skadeid;
              
            --- setter inn grunn element for avvik.
         select skade_lidseq.nextval into skadeid from dual;
            
         execute immediate 'insert into skade_l (id, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Avvik'', null, ''/''||:b, :c,''6c467680-02b9-11e4-9191-0800200c9a66'')' using skadeid, skadeid, nhid;  


    jobbnummer:= 6;	
	    if jobnummer_in <= jobbnummer then	    
			DECLARE
			  h_id Integer;
			  i_d Integer;
			  CURSOR C_Admn  IS  SELECT DISTINCT HID FROM MUSHENDELSE Where BESKRIVENDE_OBJEKT  is NULL  Order by HID;   
			BEGIN  
			  OPEN C_Admn;
			  LOOP
				FETCH C_Admn INTO h_id;
				EXIT WHEN C_Admn%NOTFOUND;
				 Select OBJIDSEQ.NEXTVAL into i_d from dual;
				 execute immediate 'Insert into SUPEROBJEKT (OBJID, SUPEROBJEKTTYPEID, SO_TYPE) values (:a, 3, ''ADM_HEND'')' using  i_d;
				 execute immediate 'Insert into SUPEROBJEKT_HENDELSE (OBJID, HID) values (:a, :b)' using  i_d, h_id;
				 execute immediate 'update MUSHENDELSE set BESKRIVENDE_OBJEKT = :a  Where Hid = :b' using i_d, h_id;	  
			  END LOOP;
			 CLOSE C_Admn;
			END;
		End if;	

    jobbnummer:= 7;	
 	commit;
	
	jobbnummer:= 8;	
	  executeScript(jobbnummer,jobnummer_in, 'Insert Into ADM_HENDELSE (OBJID, HID_OPPRETTET, HOVEDTYPE) (Select M.Beskrivende_Objekt, M.Hid, Al.Typeid  From Mushendelse M, Adm_Hendelsetype_L Al Where Al.Tekst = M.Hovedtype)');
   
	jobbnummer:= 9;	
 	commit;  

	jobbnummer:= 10;	
		  executeScript(jobbnummer,jobnummer_in, 'update ADM_HENDELSE set UNDERTYPE =(select TYPEID from ADM_HENDELSETYPE_L WHERE TEKST=''Fast'') where HID_OPPRETTET in (select distinct HID from MUSHENDELSE where UNDERTYPE=''Fast'')');	   
	jobbnummer:= 11;	
		  executeScript(jobbnummer,jobnummer_in, 'update ADM_HENDELSE set UNDERTYPE =(select TYPEID from ADM_HENDELSETYPE_L WHERE TEKST=''Midlertidig'')   where HID_OPPRETTET in (select distinct HID from MUSHENDELSE where UNDERTYPE=''Midlertidig'')');	   
	jobbnummer:= 12;	
		  executeScript(jobbnummer,jobnummer_in, 'update ADM_HENDELSE set UNDERTYPE =(select TYPEID from ADM_HENDELSETYPE_L WHERE TEKST=''Filvedlegg'')    where HID_OPPRETTET in (select distinct HID from MUSHENDELSE where UNDERTYPE=''Filvedlegg'')');	   
	jobbnummer:= 13;	
		  executeScript(jobbnummer,jobnummer_in, 'update ADM_HENDELSE set UNDERTYPE =(select TYPEID from ADM_HENDELSETYPE_L WHERE TEKST=''Media'')   where HID_OPPRETTET in (select distinct HID from MUSHENDELSE where UNDERTYPE=''Media'')');	   
	jobbnummer:= 14;	
		  executeScript(jobbnummer,jobnummer_in, 'update ADM_HENDELSE set UNDERTYPE =(select TYPEID from ADM_HENDELSETYPE_L WHERE TEKST=''Objekt'')   where HID_OPPRETTET in (Select Distinct HID from MUSHENDELSE Where UNDERTYPE=''Objekt'')');	   

     jobbnummer:=18;
	  executeScript(jobbnummer,jobnummer_in,'alter table ADM_HENDELSE_PERSON add (constraint FK_ADM_HENDELSE_PERSON_OBJIDS foreign key(OBJID_SISTE) references ADM_HENDELSE(OBJID), constraint FK_ADM_HENDELSE_PERSON_OBJID foreign key(OBJID) references ADM_HENDELSE(OBJID))');
    jobbnummer:=19;
	  executeScript(jobbnummer,jobnummer_in,'alter table ADM_HENDELSE_STED add (constraint FK_ADM_HENDELSE_STED_OBJIDS foreign key(OBJID_SISTE) references ADM_HENDELSE(OBJID), constraint FK_ADM_HENDELSE_STED_OBJID foreign key(OBJID) references ADM_HENDELSE(OBJID))');

  jobbnummer:=15;	   
		if jobnummer_in <= jobbnummer then
		  EXECUTE IMMEDIATE 'alter table adm_hendelse_person drop constraint fk_hid_ahp';    
		End if;
*/
		
		jobbnummer:=100;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus35 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(35, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
create or replace function primus63(jobbnummer out number, jobbnummer_inn number) 
	return varchar2
	AUTHID CURRENT_USER
	is

  newHID number;
begin  
  dbms_output.enable;
  begin
    jobbnummer := 1;

    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 63, jobbnummer );

      execute immediate ('create or replace FUNCTION  XML_UTSTILLING(ADMH_OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
           BEGIN
            SELECT XMLELEMENT(OBJEKT
                 , XMLFOREST(''UL'' AS OBJTYPEID)
                 , XMLFOREST(AH.SAKSNUMMER AS SAKSNUMMER)	
                 , XMLFOREST(SIDN.IDENTIFIKASJONSNR as MUSEUMSNUMMER)
                 , XMLFOREST(AHFM.FORMAAL AS FORMAAL)
                 , XMLELEMENT(
                      DATO,  
                        XMLFOREST(SD.FRADATO as FRADATO), 
                        XMLFOREST(SD.TILDATO AS TILDATO) )
                 , 
              ( select XMLAGG(TT.nxml)	FROM (Select HENTOBJEKTTITTEL(ADMH_OBJID_IN) as nxml from Dual)  TT  )
      
      
                , (
                  select xmlagg(
                XMLELEMENT(JP_NAVN, XMLFOREST(JP4.JPNAVN AS NAVN) ) )
        from PRIMUS.ADM_HENDELSE_PERSON AHPLNR  
        LEFT OUTER JOIN PRIMUS.JURPERSON JP4  
                     On AHPLNR.JPNR=JP4.JPNR 			
                  where AH.objid = AHPLNR.OBJID_SISTE 
                    and AHPLNR.JPNR is not null 
                   and AHPLNR.rolleid in (100, 101, 106)
                    and AHPLNR.OBJID_SISTE is not NULL
      
      
      )
                 , (  
               SELECT XMLFOREST(COUNT(OBU.OBJID_SISTE) AS ANTALLOBJEKTER)
             FROM PRIMUS.OBJEKT_ADM_HENDELSE OBU  
             INNER JOIN PRIMUS.OBJEKT OB  
                     ON OBU.OBJID_SISTE = OB.OBJID 
                    AND OB.HID_SLETTET IS NULL
                  where OBU.ADMH_OBJID=AH.OBJID 
                    AND OBU.HID_SLETTET is NULL 
           )
          )
      INTO OBJECTXML 
      
      FROM PRIMUS.ADM_HENDELSE AH 
        LEFT OUTER JOIN PRIMUS.SUPEROBJEKT_IDNR_H SIDN     
                     On AH.objid = SIDN.OBJID 
                    and AH.HID_IDNR = SIDN.HID
        LEFT OUTER JOIN PRIMUS.ADM_H_FORMAAL_H AHFM  
                     On AH.objid = AHFM.OBJID 
                    and AH.HID_FORMAAL = AHFM.HID    
        LEFT OUTER JOIN PRIMUS.SUPEROBJEKT_DATERING_H SD   
                     On AH.objid = SD.OBJID 
                    and AH.HID_DATERING = SD.HID
        WHERE AH.OBJID = ADMH_OBJID_IN 
          AND AH.HID_SLETTET is NULL;
        RETURN OBJECTXML;
      END XML_UTSTILLING;');

    end if;

		commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus63 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(63, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/


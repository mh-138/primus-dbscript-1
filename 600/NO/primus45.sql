create or replace function primus45(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
		Metadata tabell
	*/

	begin

	jobbnummer:=1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table materiale_h add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table objekt_etikett add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table objekt_klass add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table motivklass_h add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table objekt_maal add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table objekt_navn add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table referanse add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table objekt_emneord add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table objekt_teknikk add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table objekt_betegnelse add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table objekt_type add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table saernemn_h add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table objekt_bygningbetegnelse add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table doknotat_superobjekt add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table objekt_sted add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table motiv_sted add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table objekt_video add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table objekt_motivtype add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table motivord_h add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table motiv_objekt add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table adm_hendelse_person add rekkefolge number(3,0)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table altnr add rekkefolge number(3,0)';
	End if;
	jobbnummer:=100;
    return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus45 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(45, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/

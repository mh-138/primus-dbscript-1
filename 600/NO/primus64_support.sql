select * from bilde;

insert into bilde_tekst (bildeid) values (4070);
select * from bilde_tekst;

insert into bilde_tekst_h ( bildeid, sprakkode, hid, bildetekst) values ( 4071, 'NOR', 13, 'Dette er bildeteksten' );
update bilde_tekst set hid_bildetekst = 13 where bildeid = 4070;

commit;
rollback;

select * from bilde_tekst_h;

alter table bilde_tekst_h drop constraint FK_BILDETEKST_BILDE_BILDEID ;

alter table bilde_tekst_h add ( constraint FK_BILDETEKST_BILDE_BILDEID foreign key ( bildeid, sprakkode ) references bilde_tekst ( bildeid, sprakkode ) );

commit;

update bilde_tekst set Hid_Bildetekst = null;
delete bilde_tekst;





--------------------------------------------
-- script for oppdatering 
alter table primus.BILDE_TEKST rename to bilde_tekst_h;

-- norske baser
alter table primus.bilde_tekst_h add ( sprakkode varchar2( 3 char ) default 'NOR' constraint CK_BILDETEKST_H_SPRKKD not null );
-- svenske baser
alter table primus.bilde_tekst_h add ( sprakkode varchar2( 3 char ) default 'SWE' constraint CK_BILDETEKST_H_SPRKKD not null );


alter table bilde_tekst_h drop primary key cascade;
alter table bilde_tekst_h drop constraint CK_BILDETEKST_HID_NOT_NULL;
drop index PK_BILDETEKST;
alter table bilde_tekst_h add ( constraint PK_BILDE_TEKST_H primary key ( bildeid, sprakkode, hid ) );
alter table bilde_tekst_h add ( constraint CK_BILDETEKST_HID_NOT_NULL check ( hid is not null ) );

-- norske baser
create table bilde_tekst ( bildeid number( 8 )
                         , sprakkode varchar2( 3 char ) default 'NOR' constraint CK_BILDETEKST_SPRKKD not null
                         , hid_bildetekst number( 9 ) 
                         , constraint PK_BILDETEKST primary key ( bildeid, sprakkode )
                         , constraint FK_BILDE_TEKST_HIDBLDTXT foreign key ( hid_bildetekst ) references hendelse ( hid )
                         , constraint FK_BILDE_TEKST_BILDE foreign key ( bildeid ) references bilde ( bildeid )
                         , constraint FK_BILDE_TEKST_BLDT_TXT_H foreign key ( bildeid, sprakkode, hid_bildetekst ) references bilde_tekst_h( bildeid, sprakkode, hid ));
-- svenske baser
create table bilde_tekst ( bildeid number( 8 )
                         , sprakkode varchar2( 3 char ) default 'SWE' constraint CK_BILDETEKST_SPRKKD not null
                         , hid_bildetekst number( 9 ) 
                         , constraint PK_BILDETEKST primary key ( bildeid, sprakkode )
                         , constraint FK_BILDE_TEKST_HIDBLDTXT foreign key ( hid_bildetekst ) references hendelse ( hid )
                         , constraint FK_BILDE_TEKST_BILDE foreign key ( bildeid ) references bilde ( bildeid )
                         , constraint FK_BILDE_TEKST_BLDT_TXT_H foreign key ( bildeid, sprakkode, hid_bildetekst ) references bilde_tekst_h( bildeid, sprakkode, hid ));
                         

alter table bilde_tekst_h drop constraint FK_BILDETEKST_BILDE_BILDEID ;
--  oppdatere data...
insert into bilde_tekst ( bildeid, hid_bildetekst ) select bildeid, Hid_Bildetekst from bilde;

alter table bilde_tekst_h add ( constraint FK_BILDETEKST_BILDE_BILDEID foreign key ( bildeid, sprakkode ) references bilde_tekst ( bildeid, sprakkode ) );

create or replace function primus_xml_element(jobbnummer out number, jobbnummer_inn number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin  
  dbms_output.enable;
  begin
  
    jobbnummer := 1; 
    if jobbnummer_inn <= jobbnummer then		 
      execute immediate( '
      create or replace function XML_ELEMENT_TILSTAND(objid_in number) return xmltype as objectxml xmltype;
      begin
        SELECT XMLAGG(XMLELEMENT(tilstand, XMLFOREST( pt.tilstandskode as tilstandskode
             , tl.navn as tilstand
             , pt.beskrivelse as tilstandbeskrivelse )))
          INTO objectxml
             from (
                select oah.objid
                     , ah.objid as admobjid
                     , ah.hid_tilstand as hidTilst
                     , sod.fradato
                     , sod.tildato
                from objekt_adm_hendelse oah
                inner join adm_hendelse ah
                        on ah.objid = oah.admh_objid
                       and ah.hovedtype = 11
                       and ah.hid_slettet is null
                inner join superobjekt_datering_h sod
                        on sod.objid = ah.objid
                       and sod.hid = ah.hid_datering
                where oah.objid_siste = objid_in
                  and oah.hid_slettet is null
                order by sod.fradato desc) gyldig
        left outer join Tilstand pt
                     on pt.objid = gyldig.admobjid
                    and pt.hid = Gyldig.hidTilst
        left outer join tilstand_l tl
                     on tl.kode = pt.tilstandskode
        where rownum = 1;
        
        return Objectxml;
      end;
      ');
    end if;
    
    jobbnummer := 2; 
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function xml_element_datering(objid_inn number, array_inn num_array) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
      begin
        SELECT XMLAGG(
                 XMLELEMENT( PDATERING
                           , XMLFOREST( CONVERTDATE( pdh.DATO_FRA, SUBSTR( TO_CHAR( pdh.DATO_FRA, ''SYYYY'' ), 1, 5 ) ) AS DATOF 
                                      , CONVERTDATE( pdh.DATO_TIL, SUBSTR( TO_CHAR( pdh.DATO_TIL, ''SYYYY'' ), 1, 5 ) ) AS DATOT
                                      )
                           )
                     )
				  INTO OBJECTXML
          FROM PRIMUS.DATERING_H pdh 
             , PRIMUS.HISTORIKK ph
			   WHERE ph.HID_DATERING = pdh.HID
			     AND ph.HISTID = pdh.HISTID
			     AND ph.OBJID = objid_inn
			     AND ph.HID_SLETTET IS NULL
			     AND ph.HISTTYPE IN ( select * from table( array_inn ) );
			  
        return OBJECTXML;
      end;
      ');

    end if;
    
    jobbnummer := 3;   
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function xml_element_klausul( objid_inn number ) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
      begin
        SELECT XMLAGG(
                 XMLELEMENT( KLAUSUL
                           , XMLFOREST( RL.BESKRIVELSE AS KLAUSUL_TEKST 
                                      , RD.FRA_DATO AS FRADATO
                                      , RD.TIL_DATO AS TILDATO 
                                      )
                           )
                     )	
				  INTO objectxml
          FROM PRIMUS.RETTIGHET R
             , PRIMUS.RETTIGHET_L RL
             , PRIMUS.RETTIGHET_DATERING RD
				 WHERE R.OBJID = objid_inn 
				   AND R.RETTIGHETTYPEID = RL.RETTIGHETTYPEID
				   AND R.RETTIGHETID=RD.RETTIGHETID(+)
				   AND R.HID_KOMMENTAR=RD.HID(+)	
				   AND RL.typeid=3
				   AND R.HID_SLETTET  IS NULL
				   AND (RD.FRA_DATO < SYSDATE OR RD.FRA_DATO IS NULL)
				   AND (RD.TIL_DATO > SYSDATE OR RD.TIL_DATO IS NULL);
			  
        return objectxml;
      end;
      ');

    end if;

    jobbnummer := 4;   
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function XML_ELEMENT_JURPERSON( objid_inn number, array_inn num_array) RETURN xmltype as objectxml xmltype;
      begin
        SELECT XMLAGG( 
                 XMLELEMENT( JP_NAVN
                           , XMLFOREST( JP.JPNAVN AS NAVN 
                                      , RL.beskrivelse AS ROLLEBESKRIVELSE
                                      , RSL.JURPERSONSTATUS as JURPERSONSTATUS	
                                      )
                           )
                     )
				  INTO objectxml
          FROM primus.person_objekt po
             , primus.historikk h
             , primus.jurperson jP
             , primus.rollejp_l RL
             , ROLLEJPSTATUS_L RSL
				 WHERE po.histid_siste = h.histid 
				   AND po.rollekode = RL.rollekode
				   AND po.rolleid = RL.rolleid	
				   AND PO.ROLLEJPSTATUSID = RSL.STATUSID(+)
				   AND po.jpnr = jP.jpnr
				   AND po.histid_siste is not null
				   AND h.hid_slettet is null
				   AND h.OBJID = objid_inn
				   AND RL.ROLLEID IN ( select * from table( array_inn ) );
        
        return objectxml;
      end;
      ');
      
    end if;
    
    jobbnummer := 5;    
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function XML_ELEMENT_SAMLING(gruppeid_inn number) return xmltype as objectxml xmltype;
    	begin
        select XMLAGG( 
                 XMLELEMENT( GRUPPE
                           , XMLFOREST( grp.betegnelse as SAMLING
                                      , E.MUSEUM_NAVN as EIER
                                      )
                           )
                     )
				  INTO objectxml
          FROM PRIMUS.gruppe grp
             , PRIMUS.Eier E 
				 WHERE grp.gruppeid = gruppeid_inn 
				   AND grp.Eierid = E.Eierid;
        
        return objectxml;
      end;
      ');
      
    end if;
    
    jobbnummer := 6;
    
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function XML_ELEMENT_REGNIVA( objid_inn number ) return xmltype as objectxml xmltype;
			begin
        SELECT XMLAGG( 
                 XMLELEMENT( REGNIVAA
                           , GT_L.BESKRIVELSE
                           )
                     )
				  INTO objectxml
          FROM PRIMUS.GRUPPERINGSTYPE_L GT_L
             , primus.gruppering_h gh
             , primus.objekt po 
				 WHERE GT_L.TYPEID = gh.TYPEID 
				   AND po.objid = gh.objid 
           AND po.hid_gruppering = gh.hid
           AND po.objid = objid_inn;
        
        return objectxml;

      end;
      ');
      

    end if;
    
    jobbnummer := 7;
     if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function XML_ELEMENT_INGORI( objid_inn number ) return xmltype as objectxml xmltype;				
      begin
        SELECT XMLAGG( 
                 XMLELEMENT ( INGORI
                            , OBJEKT.MUSEUMSNR
                            )
                     )
				  INTO objectxml
          FROM PRIMUS.OBJEKT
             , PRIMUS.ANTALL 
				 WHERE ANTALL.OBJID = OBJEKT.OBJID 
           AND OBJEKT.HID_SLETTET IS NULL   
           AND ANTALL.OBJEKTID = objid_inn;
      
        return objectxml;
      end;
      ');
      
    end if;
    
    jobbnummer := 8;    
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function XML_ELEMENT_BETEGNELSE( objid_inn number ) return xmltype as objectxml xmltype;
      begin
        SELECT XMLAGG( 
                 XMLELEMENT( BETEGNELSE
                           , bl.beskrivelse 
                           )
                     )
				  INTO objectxml
          FROM primus.OBJEKT_BETEGNELSE obb
             , primus.betegnelse_l bl
				 WHERE obb.objid_siste = objid_inn 
				   AND obb.betid = bl.kode;
         
         return objectxml;
      end;
      ');
      
    end if;

    jobbnummer := 9;   
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
      create or replace function XML_ELEMENT_PRESBET( objid_inn number ) return xmltype as objectxml xmltype;
      begin
        SELECT XMLAGG( 
                 XMLELEMENT( PBETEGNELSE, typ.beskrivelse)
                     )   
          INTO objectxml           
			    FROM primus.objekt_type objt 
             , primus.type_l typ
			   WHERE objt.type=typ.kode 
			     AND objt.objid_siste = objid_inn;
        
        return objectxml;
      end;
      ');
      
    end if;
    
    jobbnummer := 10;  
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '    
        create or replace function xml_element_gabnr( objid_in number ) return xmltype as objectxml xmltype;
        begin
          select xmlelement( gabnr, xmlforest( pbgh.gabnr as gabnr ) ) 
          into objectxml
          from primus.bygning_gabnr_h pbgh
          inner join primus.bygning pb 
                  on pb.objid = pbgh.objid
                 and pb.hid_gabnr = pbgh.hid
                 and pb.objid = objid_in;
          return objectxml;
        end;
        ');
    end if;
        
    jobbnummer := 11; 
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
        create or replace function xml_element_bygningtype(objid_in number) return xmltype as objectxml xmltype;
        begin
          select xmlelement( bygningstype, pbtl.beskrivelse ) 
                 into objectxml
                 from bygning_bygningstype_h pbth 
                 inner join primus.bygning_bygningstype_l pbtl on pbth.bygningtypeid = Pbtl.Id
                 inner join primus.bygning pb 
                         on pb.objid = pbth.objid
                        and pb.hid_bygningstype = pbth.hid
                        and pb.objid = objid_in;
                 return objectxml;
        end;
        ' );
    end if;
        
    jobbnummer := 12; 
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '
        create or replace function xml_element_bygningbetegnelse( objid_in number ) return xmltype as objectxml xmltype;
        begin
          select XMLAGG(XMLELEMENT(BBETEGNELSE,	XMLFOREST(bl.beskrivelse as BESKRIVELSE,  obb.KODE as BKODE)))
                  into objectxml
                  FROM primus.OBJEKT_BYGNINGBETEGNELSE obb, primus.betegnelse_l bl
                  WHERE obb.OBJID=objid_in 
                  AND obb.betid = bl.kode 
                  AND obb.objid_siste is not null;
          return objectxml;
        end;
      ' );
    end if;
    
    jobbnummer := 13;   
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '      
        create or replace function xml_element_objektbeskrivelse( objid_in number ) return xmltype as objectxml xmltype;
        begin
          select xmlelement( beskrivelse, pobh.beskrivelse )
            into objectxml
            from primus.obj_beskrivelse_h pobh
           inner join primus.objekt po 
                   on po.objid = pobh.objid
                  and po.hid_beskrivelse = pobh.hid
                  and po.objid = objid_in;
            
            return objectxml;
        end;
      ' );
    end if;
    
    jobbnummer := 14;    
    if jobbnummer_inn <= jobbnummer then	
      execute immediate( '      
        create or replace function xml_element_bygningvernetype(objid_in number) return xmltype as objectxml xmltype;
        begin
          select xmlelement( bygningvernetype, pbtl.beskrivelse  )
                 into objectxml
                 from primus.bygning_vernetype_h pbth 
                 inner join primus.bygning_vernetype_l pbtl on pbth.vernetypeid = Pbtl.Id
                 inner join primus.bygning pb 
                         on pb.objid = pbth.objid
                        and pb.hid_vernetype = pbth.hid
                        and pb.objid = objid_in;
                 return objectxml;
        end;
      ' );
    end if;
    
    jobbnummer := 15;  
    if jobbnummer_inn <= jobbnummer then
      execute immediate( '
        create or replace function xml_bygning( objid_in number ) return xmltype as objectxml xmltype;
        begin

          select xmlelement(objekt, 
                    xmlforest( ob.museumsnr as museumsnummer
                             , ob.objtypeid as objtypeid)
                    , xml_element_bygningvernetype( objid_in )
                    , xml_element_objektbeskrivelse( objid_in )
                    , xml_element_bygningbetegnelse( objid_in )
                    , xml_element_bygningtype( objid_in )
                    , xml_element_gabnr( objid_in )
                    , xml_element_regniva( objid_in )
                    , xml_element_samling ( ob.gruppe )
                    , xml_element_jurperson( objid_in,   num_array(14,15,16,18,181,191,26,64,67, 90, 961,103,100,101, 102,109,111,112,113,114,115, 116, 117, 118, 141,142,143, 76, 208, 213))
                    , xml_element_klausul( objid_in )
                    , xml_element_datering( objid_in, num_array(100))
                    , xml_element_tilstand( objid_in )
          )
          INTO objectxml 
          FROM PRIMUS.OBJEKT OB 
          WHERE OB.OBJID=OBJID_IN ;
                
          return objectxml;
        end xml_bygning;
        ');
    end if;
	
   jobbnummer:= 16;
	if  jobbnummer_inn <= jobbnummer then		  
	     EXECUTE IMMEDIATE '
		  create or replace FUNCTION  XML_UTLAAN(ADMH_OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
		   BEGIN
			  SELECT  XMLELEMENT(OBJEKT
			   ,XMLFOREST(''UL'' AS OBJTYPEID)
			   ,XMLFOREST(AH.SAKSNUMMER AS SAKSNUMMER)	
			   ,XMLFOREST(SIDN.IDENTIFIKASJONSNR as UTLAANSNR)
			   ,XMLFOREST(AHFM.FORMAAL AS FORMAAL)
			  ,XMLELEMENT(DATO, 
			    XMLFOREST(SD.FRADATO as FRADATO), 
				XMLFOREST(SD.TILDATO AS TILDATO))
			  ,XMLELEMENT( LAANTAKER, 
			    XMLFOREST(JP4.JPNAVN AS LAANTAKERP), 
				XMLFOREST(JP3.JPNAVN AS LAANTAKERI))
			  ,XMLELEMENT( LAANGIVER, 
			    XMLFOREST(JP2.JPNAVN AS LAANGIVERP), 
				XMLFOREST(JP1.JPNAVN AS LAANGIVERI))
			  ,
			  ( 
			     SELECT XMLFOREST(COUNT(OBU.OBJID_SISTE) AS ANTALL)
				 FROM PRIMUS.OBJEKT_ADM_HENDELSE OBU  
				 INNER JOIN PRIMUS.OBJEKT OB  ON OBU.OBJID_SISTE = OB.OBJID AND OB.HID_SLETTET IS NULL
			     where OBU.ADMH_OBJID=AH.OBJID AND OBU.HID_SLETTET is NULL 
				)
			 ,
			  (
				SELECT XMLFOREST(COUNT(OBU.OBJID_SISTE) AS UNRETURNED)FROM PRIMUS.OBJEKT_ADM_HENDELSE OBU 
				INNER JOIN PRIMUS.OBJEKT OB  ON OBU.OBJID_SISTE = OB.OBJID AND OB.HID_SLETTET IS NULL                
				Where OBU.ADMH_OBJID=AH.OBJID  AND OBU.HID_SLETTET is NULL AND OBU.INNLEVERT is NULL  
				AND ((SD.TILDATO <Sysdate) OR (SD.TILDATO is NULL))
			 )		  

				)
				INTO OBJECTXML FROM PRIMUS.ADM_HENDELSE AH 
			LEFT OUTER JOIN PRIMUS.SUPEROBJEKT_IDNR_H SIDN     On AH.objid = SIDN.OBJID and AH.HID_IDNR = SIDN.HID
			LEFT OUTER JOIN PRIMUS.ADM_H_FORMAAL_H AHFM  On AH.objid = AHFM.OBJID and AH.HID_FORMAAL = AHFM.HID    
			LEFT OUTER JOIN PRIMUS.SUPEROBJEKT_DATERING_H SD   On AH.objid = SD.OBJID and AH.HID_DATERING = SD.HID
			LEFT OUTER JOIN PRIMUS.ADM_HENDELSE_PERSON AHPLNR  On AH.objid = AHPLNR.OBJID_SISTE and AHPLNR.JPNR is not null and AHPLNR.ROLLEID=244
			LEFT OUTER JOIN PRIMUS.JURPERSON JP4  On AHPLNR.JPNR=JP4.JPNR and AHPLNR.OBJID_SISTE is not NULL
			LEFT OUTER JOIN PRIMUS.ADM_HENDELSE_PERSON AHPLNI  On AH.objid = AHPLNI.OBJID_SISTE and AHPLNI.JPNR is not null and AHPLNI.ROLLEID=243
			LEFT OUTER JOIN PRIMUS.JURPERSON JP3  On AHPLNI.JPNR=JP3.JPNR and AHPLNI.OBJID_SISTE is not NULL
			LEFT OUTER JOIN PRIMUS.ADM_HENDELSE_PERSON AHPUTNR  On AH.objid = AHPUTNR.OBJID_SISTE and AHPUTNR.JPNR is not null and AHPUTNR.ROLLEID=242
			LEFT OUTER JOIN PRIMUS.JURPERSON JP2  On AHPUTNR.JPNR=JP2.JPNR and AHPUTNR.OBJID_SISTE is not NULL
			LEFT OUTER JOIN PRIMUS.ADM_HENDELSE_PERSON AHPUTNI  On AH.objid = AHPUTNI.OBJID_SISTE and AHPUTNI.JPNR is not null and AHPUTNI.ROLLEID=241
			LEFT OUTER JOIN PRIMUS.JURPERSON JP1  On AHPUTNI.JPNR=JP1.JPNR and AHPUTNI.OBJID_SISTE is not NULL			
			WHERE AH.OBJID=ADMH_OBJID_IN AND AH.HID_SLETTET is NULL;
				RETURN OBJECTXML;
          END XML_UTLAAN;';
		End if;
	

     jobbnummer:= 17;
    if jobbnummer_inn <= jobbnummer then      
      execute immediate ('create or replace FUNCTION  XML_UTSTILLING(ADMH_OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
           BEGIN
            SELECT XMLELEMENT(OBJEKT
                 , XMLFOREST(''UL'' AS OBJTYPEID)
                 , XMLFOREST(AH.SAKSNUMMER AS SAKSNUMMER)	
                 , XMLFOREST(SIDN.IDENTIFIKASJONSNR as MUSEUMSNUMMER)
                 , XMLFOREST(AHFM.FORMAAL AS FORMAAL)
                 , XMLELEMENT(
                      DATO,  
                        XMLFOREST(SD.FRADATO as FRADATO), 
                        XMLFOREST(SD.TILDATO AS TILDATO) )
                 , 
              ( select XMLAGG(TT.nxml)	FROM (Select HENTOBJEKTTITTEL(ADMH_OBJID_IN) as nxml from Dual)  TT  )
      
      
                , (
                  select xmlagg(
                XMLELEMENT(JP_NAVN, XMLFOREST(JP4.JPNAVN AS NAVN) ) )
        from PRIMUS.ADM_HENDELSE_PERSON AHPLNR  
        LEFT OUTER JOIN PRIMUS.JURPERSON JP4  
                     On AHPLNR.JPNR=JP4.JPNR 			
                  where AH.objid = AHPLNR.OBJID_SISTE 
                    and AHPLNR.JPNR is not null 
                   and AHPLNR.rolleid in (100, 101, 106)
                    and AHPLNR.OBJID_SISTE is not NULL
      
      
      )
                 , (  
               SELECT XMLFOREST(COUNT(OBU.OBJID_SISTE) AS ANTALLOBJEKTER)
             FROM PRIMUS.OBJEKT_ADM_HENDELSE OBU  
             INNER JOIN PRIMUS.OBJEKT OB  
                     ON OBU.OBJID_SISTE = OB.OBJID 
                    AND OB.HID_SLETTET IS NULL
                  where OBU.ADMH_OBJID=AH.OBJID 
                    AND OBU.HID_SLETTET is NULL 
           )
          )
      INTO OBJECTXML 
      
      FROM PRIMUS.ADM_HENDELSE AH 
        LEFT OUTER JOIN PRIMUS.SUPEROBJEKT_IDNR_H SIDN     
                     On AH.objid = SIDN.OBJID 
                    and AH.HID_IDNR = SIDN.HID
        LEFT OUTER JOIN PRIMUS.ADM_H_FORMAAL_H AHFM  
                     On AH.objid = AHFM.OBJID 
                    and AH.HID_FORMAAL = AHFM.HID    
        LEFT OUTER JOIN PRIMUS.SUPEROBJEKT_DATERING_H SD   
                     On AH.objid = SD.OBJID 
                    and AH.HID_DATERING = SD.HID
        WHERE AH.OBJID = ADMH_OBJID_IN 
          AND AH.HID_SLETTET is NULL;
        RETURN OBJECTXML;
      END XML_UTSTILLING;');

    end if;
	
   jobbnummer:= 18;
    if jobbnummer_inn <= jobbnummer then      
      execute immediate ('	
	  create or replace FUNCTION  XML_AKSESJON(AKSESJONID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
		  BEGIN
			SELECT  XMLELEMENT(OBJEKT, XMLFOREST(''AK'' AS OBJTYPEID)
			, XMLFOREST(UT.AKSESJONSNR AS AKSESJONSNR)
			, XMLFOREST(UT.INNHOLD AS INNHOLD)
			, XMLFOREST(UT.PLASSERING AS PLASSERING)
			, XMLFOREST(UT.FERDIG AS FERDIG)
			, XMLELEMENT(AKSESJONSDATO, XMLFOREST(TO_CHAR(UT.INNKOMSTDATO, ''HH24'') as INNDATOTIME),
				   XMLFOREST(UT.INNKOMSTDATO as INNDATO),
				   XMLFOREST(TO_CHAR(UT.AKSESJONSDATO, ''HH24'') as AKSDATOTIME),
				   XMLFOREST(UT.AKSESJONSDATO AS AKSDATO)
			   )
			,(
			SELECT XMLFOREST(Count(ANT.AKSESJONID) AS AKSANTALL)
			FROM PRIMUS.ANTALL ANT where ANT.AKSESJONID=UT.AKSESJONID
			 )

		  )
		  INTO OBJECTXML FROM PRIMUS.AKSESJON UT WHERE UT.AKSESJONID=AKSESJONID_IN;
		  RETURN OBJECTXML;
		  END XML_AKSESJON;');
	End if;

	
	
jobbnummer := 19; 
    if jobbnummer_inn <= jobbnummer then		 
      execute immediate( '	
  CREATE OR REPLACE FORCE VIEW "PRIMUS"."OBJEKT_V" ("OBJID", "OBJTYPEID", "MUSEUMSNR", "HID_MAALTEKST", 
    "HID_TEKNIKKTEKST", "HID_MATERIALETEKST", "HID_MOTIV", "ANTALL", "MUSNR_TALL", 
    "REG_DATO", "GRUPPERINGSTYPEID", "REG_NIVA", "SIGN", "SAMLING", "EIER", HID_PROVENIENSDTRNG, HID_PRODUKSJONSDTRNG) AS 
  (
    select o.objid
         , o.objtypeID
         , o.museumsnr
         , o.hid_maaltekst
         , o.hid_teknikktekst
         , o.hid_materialetekst
         , o.hid_motiv
         , a_h.antall
         , o.musnr_tall
         , o.reg_dato
         , g_h.typeid
		 , pgtl.beskrivelse
         , s.sign
         , pg.betegnelse as samling
         , pe.museum_navn
		 , o.HID_PRODUKSJONSDTRNG
		 , o.HID_PROVENIENSDTRNG
      from primus.objekt o
         , primus.antall_h a_h
         , primus.gruppering_h g_h
         , primus.signatur s
         , primus.gruppe pg
         , primus.eier pe
		 , primus.grupperingstype_l pgtl
    where (o.signid = s.signid)
      and (o.hid_antall=a_h.hid(+))
      and (o.objid = a_h.objid(+))
      and (o.hid_gruppering=g_h.hid(+))
      and (o.objid=g_h.objid(+))
      and (o.hid_slettet is null)
      and (o.gruppe = pg.gruppeid(+))
      and (pg.eierid = pe.eierid(+))
	  and (g_h.typeid = pgtl.typeid(+))
      )');
	End if; 	

	 jobbnummer:= 20;
    if jobbnummer_inn <= jobbnummer then      
      execute immediate ('			
	create or replace FUNCTION  XML_BYGNINGSDEL(OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE; BEGIN
			  SELECT  XMLELEMENT(OBJEKT, XMLFOREST(OB.MUSEUMSNR AS MUSEUMSNUMMER, OB.OBJTYPEID AS OBJTYPEID),
				  (
				select XMLAGG(XMLELEMENT(GRUPPE, XMLFOREST(grp.betegnelse as SAMLING, E.MUSEUM_NAVN as EIER)))
				FROM PRIMUS.gruppe grp, PRIMUS.Eier E
				WHERE OB.gruppe=grp.gruppeid
				AND grp.Eierid=E.Eierid
				),
			  (
				select XMLAGG(XMLELEMENT(REGNIVAA, GT_L.BESKRIVELSE))
				FROM PRIMUS.GRUPPERINGSTYPE_L GT_L, primus.gruppering_h gh
				WHERE GT_L.TYPEID=gh.TYPEID
				And OB.objid = gh.objid AND OB.hid_gruppering = gh.hid
				),
			  (
				SELECT XMLAGG(XMLELEMENT(INGORI, OBJEKT.MUSEUMSNR))
				FROM PRIMUS.OBJEKT , PRIMUS.ANTALL
				WHERE ( ANTALL.OBJID = OBJEKT.OBJID )
				AND ( OBJEKT.HID_SLETTET IS NULL )
				AND ( ANTALL.OBJEKTID = OB.OBJID )
			  ),
			  (
			   SELECT XMLAGG(XMLELEMENT(BETEGNELSE,	bl.beskrivelse ))
				FROM primus.OBJEKT_BETEGNELSE obb, primus.betegnelse_l bl
				WHERE obb.objid_siste=OB.OBJID
				AND obb.betid = bl.kode
			 ),
			 (
				SELECT XMLAGG(XMLELEMENT(PBETEGNELSE,	typ.beskrivelse))
				From primus.objekt_type objt , primus.type_l typ
				where objt.type=typ.kode
				and objt.objid_siste = OB.OBJID
				),
			  (
				SELECT XMLAGG(XMLELEMENT(VAREMERKE,	V.VAREMERKE))
				FROM PRIMUS.VAREMERKE_H V WHERE V.OBJID_SISTE=OB.OBJID
				),
				(
				SELECT XMLAGG(XMLELEMENT(JP_NAVN, XMLFOREST(JP.JPNAVN AS NAVN , RL.beskrivelse AS ROLLEBESKRIVELSE, RSL.JURPERSONSTATUS as JURPERSONSTATUS		)))
				FROM primus.person_objekt po, primus.historikk h, primus.jurperson jP, primus.rollejp_l RL, ROLLEJPSTATUS_L RSL
				WHERE po.histid_siste = h.histid
				AND   po.rollekode = RL.rollekode
				AND   po.rolleid   = RL.rolleid
				AND   PO.ROLLEJPSTATUSID = RSL.STATUSID(+)
				AND   po.jpnr = jP.jpnr
				AND   po.histid_siste is not null
				AND   h.hid_slettet is null
				AND   h.OBJID=OB.OBJID
				AND   RL.ROLLEID IN (14,15,16,18,181,191,26,64,67, 90, 961,103,100,101,
									 102,109,111,112,113,114,115, 116, 117, 118, 141,142,143, 76, 208, 213)
			  ),
			  (
				SELECT XMLAGG(XMLELEMENT(KLAUSUL, XMLFOREST( RL.BESKRIVELSE AS KLAUSUL_TEKST , RD.FRA_DATO AS FRADATO , RD.TIL_DATO AS TILDATO )	))
				FROM PRIMUS.RETTIGHET R,    PRIMUS.RETTIGHET_L RL, PRIMUS.RETTIGHET_DATERING RD
				WHERE R.OBJID=OB.OBJID
				AND R.RETTIGHETTYPEID = RL.RETTIGHETTYPEID
				AND R.RETTIGHETID=RD.RETTIGHETID(+)
				AND R.HID_KOMMENTAR=RD.HID(+)
				AND RL.typeid=3
				AND R.HID_SLETTET  IS NULL
				AND (RD.FRA_DATO < SYSDATE OR RD.FRA_DATO IS NULL)
				AND (RD.TIL_DATO > SYSDATE OR RD.TIL_DATO IS NULL)
			  ),
			  (
			  SELECT XMLAGG(XMLELEMENT(PDATERING,
							XMLFOREST(CONVERTDATE(DH.DATO_FRA, SUBSTR(TO_CHAR( DH.DATO_FRA, ''SYYYY''),1,5)) AS DATOF ,
									  CONVERTDATE(DH.DATO_TIL, SUBSTR(TO_CHAR( DH.DATO_TIL, ''SYYYY''),1,5)) AS DATOT
				  )
					))
				FROM PRIMUS.DATERING_H DH , PRIMUS.HISTORIKK HK
			  WHERE HK.HID_DATERING= DH.HID
			  AND HK.HISTID=DH.HISTID
			  AND HK.OBJID=OB.OBJID
			  AND HK.HID_SLETTET IS NULL
			  AND (HK.HISTTYPE=100)
			  )
			)
			INTO OBJECTXML FROM PRIMUS.OBJEKT OB WHERE OB.OBJID=OBJID_IN and OBJTYPEID=6 and OB.HID_SLETTET is NULL;
			RETURN OBJECTXML;
			END XML_BYGNINGSDEL;');
End if;	

	 jobbnummer:= 21;
    if jobbnummer_inn <= jobbnummer then      
      execute immediate ('
          create or replace FUNCTION  XML_FOTO(OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
			BEGIN
			  SELECT  XMLELEMENT(OBJEKT, XMLFOREST(OB.MUSEUMSNR AS MUSEUMSNUMMER, OB.OBJTYPEID AS OBJTYPEID),
			  ( select XMLAGG(TT.nxml)	FROM (Select HENTOBJEKTTITTEL(objid_in) as nxml from Dual)  TT  ),
			  (
				select XMLAGG(XMLELEMENT(GRUPPE, XMLFOREST(grp.betegnelse as SAMLING, E.MUSEUM_NAVN as EIER)))
				FROM PRIMUS.gruppe grp, PRIMUS.Eier E
				WHERE grp.gruppeid=OB.gruppe
				AND grp.Eierid=E.Eierid
				),
			  (
				select XMLAGG(XMLELEMENT(REGNIVAA, GT_L.BESKRIVELSE))
				FROM PRIMUS.GRUPPERINGSTYPE_L GT_L, primus.gruppering_h gh
				WHERE GT_L.TYPEID=gh.TYPEID
				And OB.objid = gh.objid AND OB.hid_gruppering = gh.hid
				),
				(
				SELECT XMLAGG(XMLELEMENT(JP_NAVN, XMLFOREST(JP.JPNAVN AS NAVN , RL.beskrivelse AS ROLLEBESKRIVELSE, RSL.JURPERSONSTATUS as JURPERSONSTATUS	)))
				FROM primus.person_objekt po, primus.historikk h, primus.jurperson jP, primus.rollejp_l RL, ROLLEJPSTATUS_L RSL
				WHERE po.histid_siste = h.histid
				AND   po.rollekode = RL.rollekode
				AND   po.rolleid   = RL.rolleid
				AND   PO.ROLLEJPSTATUSID = RSL.STATUSID(+)
				AND   po.jpnr = jP.jpnr
				AND   po.histid_siste is not null
				AND   h.hid_slettet is null
				AND   h.OBJID=OB.OBJID
				AND   RL.ROLLEID IN (100, 101,102)
			  ),
			  (
				SELECT XMLAGG(XMLELEMENT(MOTIV,	OMH.MOTIV2000))
				FROM PRIMUS.OBJ_MOTIV_H OMH WHERE OMH.OBJID=OB.OBJID AND OMH.HID=OB.HID_MOTIV
				),
			 (
				SELECT XMLAGG(XMLELEMENT(KLAUSUL, XMLFOREST( RL.BESKRIVELSE AS KLAUSUL_TEKST , RD.FRA_DATO AS FRADATO , RD.TIL_DATO AS TILDATO )	))
				FROM PRIMUS.RETTIGHET R,    PRIMUS.RETTIGHET_L RL, PRIMUS.RETTIGHET_DATERING RD
				WHERE R.OBJID=OB.OBJID
				AND R.RETTIGHETTYPEID = RL.RETTIGHETTYPEID
				AND R.RETTIGHETID=RD.RETTIGHETID(+)
				AND R.HID_KOMMENTAR=RD.HID(+)
				AND RL.typeid=3
				AND R.HID_SLETTET  IS NULL
				AND (RD.FRA_DATO < SYSDATE OR RD.FRA_DATO IS NULL)
				AND (RD.TIL_DATO > SYSDATE OR RD.TIL_DATO IS NULL)
			  ),
			(
			  SELECT XMLAGG(XMLELEMENT(PDATERING,
							XMLFOREST(CONVERTDATE(DH.DATO_FRA, SUBSTR(TO_CHAR( DH.DATO_FRA, ''SYYYY''),1,5)) AS DATOF ,
									  CONVERTDATE(DH.DATO_TIL, SUBSTR(TO_CHAR( DH.DATO_TIL, ''SYYYY''),1,5)) AS DATOT
				  )
					))
				FROM PRIMUS.DATERING_H DH , PRIMUS.HISTORIKK HK
			  WHERE HK.HID_DATERING= DH.HID
			  AND HK.HISTID=DH.HISTID
			  AND HK.OBJID=OB.OBJID
			  AND HK.HID_SLETTET IS NULL
			  AND HK.HISTTYPE in (101, 100)
			  )

			)
			INTO OBJECTXML FROM PRIMUS.OBJEKT OB WHERE OB.OBJID=OBJID_IN ;
			RETURN OBJECTXML;
			END XML_FOTO;');
  End if;			
	jobbnummer:= 22;
    if jobbnummer_inn <= jobbnummer then 
      execute immediate ('create or replace FUNCTION  XML_PERSON(JPNR_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
				BEGIN
				  SELECT  XMLELEMENT(OBJEKT, XMLFOREST(''JP'' AS OBJTYPEID)
				  , XMLFOREST(UT.JPNAVN AS JPNAVN)
				  , XMLFOREST(UT.NASJONALITET AS NASJONALITET)
				  , XMLFOREST(UT.KJONN AS KJONN)
				  , XMLFOREST(UT.JPIDENTITY AS JPIDENTITET)
				  , XMLFOREST(UT.AUTORITET AS AUTORITET)
				  , XMLFOREST(UT.AUTORITET_DATASET AS AUTORITET_DATASET)
				  , XMLFOREST(UT.AUTORITET_KILDE AS AUTORITET_KILDE)
				  , XMLFOREST(UT.AUTORITET_STATUS AS AUTORITET_STATUS)
				  , XMLFOREST(UT.STATUS AS STATUS)
				  , XMLELEMENT(JPDATO, XMLFOREST(UT.FOEDT_ETABL_AAR as JPFRADATO),
									 XMLFOREST(UT.DOED_NEDL_AAR AS JPTILDATO))
				  ,(
					SELECT XMLFOREST(Lst.JURPERSONTYPE AS JPTYPE)
					FROM PRIMUS.JURPERSONTYPE_L LST where LST.typeid=ut.jptypeid
				   )
				  ,(
					SELECT XMLAGG(XMLELEMENT(JPALTNAVN, XMLFOREST(alt.ALTNAVN AS ALTNAVN),
														XMLFOREST(alt.KOMMENTAR AS JPKOMM)))
														FROM PRIMUS.JP_ALTNAVN alt where alt.JPNR=UT.JPNR
				   )
				)
				INTO OBJECTXML FROM PRIMUS.JURPERSON UT WHERE UT.JPNR=JPNR_IN;
				RETURN OBJECTXML;
				End Xml_Person; ');
End if;

jobbnummer:= 23;
    if jobbnummer_inn <= jobbnummer then 
      execute immediate (
	  'create or replace FUNCTION  XML_FOTOEKSEMPLAR(OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
			BEGIN
			  SELECT  XMLELEMENT(OBJEKT, XMLFOREST(OB.MUSEUMSNR AS MUSEUMSNUMMER, OB.OBJTYPEID AS OBJTYPEID),
			  (
				select XMLAGG(XMLELEMENT(GRUPPE, XMLFOREST(grp.betegnelse as SAMLING, E.MUSEUM_NAVN as EIER)))
				FROM PRIMUS.gruppe grp, PRIMUS.Eier E
				WHERE grp.gruppeid=OB.gruppe
				AND grp.Eierid=E.Eierid
				),
			  (
				select XMLAGG(XMLELEMENT(REGNIVAA, GT_L.BESKRIVELSE))
				FROM PRIMUS.GRUPPERINGSTYPE_L GT_L, primus.gruppering_h gh
				WHERE GT_L.TYPEID=gh.TYPEID
				And OB.objid = gh.objid AND OB.hid_gruppering = gh.hid
				),
				(
				SELECT XMLAGG(XMLELEMENT(JP_NAVN, XMLFOREST(JP.JPNAVN AS NAVN , RL.beskrivelse AS ROLLEBESKRIVELSE, RSL.JURPERSONSTATUS as JURPERSONSTATUS	)))
				FROM primus.person_objekt po, primus.historikk h, primus.jurperson jP, primus.rollejp_l RL, ROLLEJPSTATUS_L RSL
				WHERE po.histid_siste = h.histid
				AND   po.rollekode = RL.rollekode
				AND   po.rolleid   = RL.rolleid
				AND   PO.ROLLEJPSTATUSID = RSL.STATUSID(+)
				AND   po.jpnr = jP.jpnr
				AND   po.histid_siste is not null
				AND   h.hid_slettet is null
				AND   h.OBJID=OB.OBJID
				AND   RL.ROLLEID IN (100, 101,102)
			  ),
			  (
				SELECT XMLAGG(XMLELEMENT(FEMOTIV,	XMLFOREST(OMH.MOTIV2000 as FMOTIV, OBFFE.MUSEUMSNR AS FMUSEUMSNUMMER )))
				FROM PRIMUS.OBJ_MOTIV_H OMH
				INNER JOIN PRIMUS.OBJEKT OBFFE on OMH.OBJID = OBFFE.OBJID AND OMH.HID=OBFFE.HID_MOTIV
				INNER JOIN PRIMUS.FOTO_FOTOEKS FFE on OMH.OBJID=FFE.OBJID
				WHERE FFE.EKSID=OB.OBJID
				),
			  (
			  SELECT XMLAGG(XMLELEMENT(FETEKNIKK,  XMLFOREST(BL.TEKST as BLTEKST, BL.KODE as BLKODE) ))
				FROM PRIMUS.FOTOEKS_TEKNIKK FT, PRIMUS.BUNNMATERIALE_L BL
				WHERE FT.OBJID_SISTE=OB.OBJID
				AND FT.TYPEID=BL.TYPEID
				AND FT.TYPEID IS NOT NULL
			  ),
			 (
				SELECT XMLAGG(XMLELEMENT(KLAUSUL, XMLFOREST( RL.BESKRIVELSE AS KLAUSUL_TEKST , RD.FRA_DATO AS FRADATO , RD.TIL_DATO AS TILDATO )	))
				FROM PRIMUS.RETTIGHET R,    PRIMUS.RETTIGHET_L RL, PRIMUS.RETTIGHET_DATERING RD
				WHERE R.OBJID=OB.OBJID
				AND R.RETTIGHETTYPEID = RL.RETTIGHETTYPEID
				AND R.RETTIGHETID=RD.RETTIGHETID(+)
				AND R.HID_KOMMENTAR=RD.HID(+)
				AND RL.typeid=3
				AND R.HID_SLETTET  IS NULL
				AND (RD.FRA_DATO < SYSDATE OR RD.FRA_DATO IS NULL)
				AND (RD.TIL_DATO > SYSDATE OR RD.TIL_DATO IS NULL)
			  ),
			(
			  SELECT XMLAGG(XMLELEMENT(PDATERING,
							XMLFOREST(CONVERTDATE(DH.DATO_FRA, SUBSTR(TO_CHAR( DH.DATO_FRA, ''SYYYY''),1,5)) AS DATOF ,
									  CONVERTDATE(DH.DATO_TIL, SUBSTR(TO_CHAR( DH.DATO_TIL, ''SYYYY''),1,5)) AS DATOT
				  )
					))
				FROM PRIMUS.DATERING_H DH , PRIMUS.HISTORIKK HK
			  WHERE HK.HID_DATERING= DH.HID
			  AND HK.HISTID=DH.HISTID
			  AND HK.OBJID=OB.OBJID
			  AND HK.HID_SLETTET IS NULL
			  AND HK.HISTTYPE in (101, 100)
			  )

			)
			INTO OBJECTXML FROM PRIMUS.OBJEKT OB WHERE OB.OBJID=OBJID_IN ;
			RETURN OBJECTXML;
			END XML_FOTOEKSEMPLAR;');
end if;
 jobbnummer:= 24;
    if jobbnummer_inn <= jobbnummer then 
      execute immediate ('
          create or replace FUNCTION  XML_GJENSTAND(OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
			BEGIN
			  SELECT  XMLELEMENT(OBJEKT, XMLFOREST(OB.MUSEUMSNR AS MUSEUMSNUMMER, OB.OBJTYPEID AS OBJTYPEID),
				( select XMLAGG(TT.nxml)	FROM (Select HENTOBJEKTTITTEL(objid_in) as nxml from Dual)  TT  ),
				  (
				select XMLAGG(XMLELEMENT(GRUPPE, XMLFOREST(grp.betegnelse as SAMLING, E.MUSEUM_NAVN as EIER)))
				FROM PRIMUS.gruppe grp, PRIMUS.Eier E
				WHERE OB.gruppe=grp.gruppeid
				AND grp.Eierid=E.Eierid
				),
			  (
				select XMLAGG(XMLELEMENT(REGNIVAA, GT_L.BESKRIVELSE))
				FROM PRIMUS.GRUPPERINGSTYPE_L GT_L, primus.gruppering_h gh
				WHERE GT_L.TYPEID=gh.TYPEID
				And OB.objid = gh.objid AND OB.hid_gruppering = gh.hid
				),
			  (
				SELECT XMLAGG(XMLELEMENT(INGORI, OBJEKT.MUSEUMSNR))
				FROM PRIMUS.OBJEKT , PRIMUS.ANTALL
				WHERE ( ANTALL.OBJID = OBJEKT.OBJID )
				AND ( OBJEKT.HID_SLETTET IS NULL )
				AND ( ANTALL.OBJEKTID = OB.OBJID )
			  ),
			  (
			   SELECT XMLAGG(XMLELEMENT(BETEGNELSE,	bl.beskrivelse ))
				FROM primus.OBJEKT_BETEGNELSE obb, primus.betegnelse_l bl
				WHERE obb.objid_siste=OB.OBJID
				AND obb.betid = bl.kode
			 ),
			 (
			  SELECT XMLAGG(XMLELEMENT(PBETEGNELSE,	typ.beskrivelse))
			  From primus.objekt_type objt , primus.type_l typ
			  where objt.type=typ.kode
			  and objt.objid_siste = OB.OBJID
				),
			  (
				SELECT XMLAGG(XMLELEMENT(VAREMERKE,	V.VAREMERKE))
				FROM PRIMUS.VAREMERKE_H V WHERE V.OBJID_SISTE=OB.OBJID
				),
				(
				SELECT XMLAGG(XMLELEMENT(JP_NAVN, XMLFOREST(JP.JPNAVN AS NAVN , RL.beskrivelse AS ROLLEBESKRIVELSE, RSL.JURPERSONSTATUS as JURPERSONSTATUS	)))
				FROM primus.person_objekt po, primus.historikk h, primus.jurperson jP, primus.rollejp_l RL, ROLLEJPSTATUS_L RSL
				WHERE po.histid_siste = h.histid
				AND   po.rollekode = RL.rollekode
				AND   po.rolleid   = RL.rolleid
				AND   PO.ROLLEJPSTATUSID = RSL.STATUSID(+)
				AND   po.jpnr = jP.jpnr
				AND   po.histid_siste is not null
				AND   h.hid_slettet is null
				AND   h.OBJID=OB.OBJID
				AND   RL.ROLLEID IN (14,15,16,18,181,191,26,64,67, 90, 961,103,100, 102,101,
									 109,111,112,113,114,115, 116, 117, 118, 141,142,143, 76, 208, 213)
			  ),
			  (
				SELECT XMLAGG(XMLELEMENT(KLAUSUL, XMLFOREST( RL.BESKRIVELSE AS KLAUSUL_TEKST , RD.FRA_DATO AS FRADATO , RD.TIL_DATO AS TILDATO )	))
				FROM PRIMUS.RETTIGHET R,    PRIMUS.RETTIGHET_L RL, PRIMUS.RETTIGHET_DATERING RD
				WHERE R.OBJID=OB.OBJID
				AND R.RETTIGHETTYPEID = RL.RETTIGHETTYPEID
				AND R.RETTIGHETID=RD.RETTIGHETID(+)
				AND R.HID_KOMMENTAR=RD.HID(+)
				AND RL.typeid=3
				AND R.HID_SLETTET  IS NULL
				AND (RD.FRA_DATO < SYSDATE OR RD.FRA_DATO IS NULL)
				AND (RD.TIL_DATO > SYSDATE OR RD.TIL_DATO IS NULL)
			  ),
			  (
			  SELECT XMLAGG(XMLELEMENT(PDATERING,
							XMLFOREST(CONVERTDATE(DH.DATO_FRA, SUBSTR(TO_CHAR( DH.DATO_FRA, ''SYYYY''),1,5)) AS DATOF ,
									  CONVERTDATE(DH.DATO_TIL, SUBSTR(TO_CHAR( DH.DATO_TIL, ''SYYYY''),1,5)) AS DATOT
				  )
					))
				FROM PRIMUS.DATERING_H DH , PRIMUS.HISTORIKK HK
			  WHERE HK.HID_DATERING= DH.HID
			  AND HK.HISTID=DH.HISTID
			  AND HK.OBJID=OB.OBJID
			  AND HK.HID_SLETTET IS NULL
			  AND (HK.HISTTYPE=100)
			  )
			)
			INTO OBJECTXML FROM PRIMUS.OBJEKT OB WHERE OB.OBJID=OBJID_IN and OBJTYPEID=6 and OB.HID_SLETTET is NULL;
			RETURN OBJECTXML;
			END XML_GJENSTAND;');
	End if;		
 jobbnummer:= 25;
    if jobbnummer_inn <= jobbnummer then 
      execute immediate ('	
	create or replace FUNCTION  XML_KOPI(OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
		BEGIN
		  SELECT  XMLELEMENT(OBJEKT,	XMLFOREST(OB.MUSEUMSNR AS MUSEUMSNUMMER,  OB.OBJTYPEID AS OBJTYPEID),
		  ( select XMLAGG(TT.nxml)	FROM (Select HENTOBJEKTTITTEL(objid_in) as nxml from Dual)  TT  ),
		  (
			select XMLAGG(XMLELEMENT(GRUPPE, XMLFOREST(grp.betegnelse as SAMLING, E.MUSEUM_NAVN as EIER)))
			FROM PRIMUS.gruppe grp, PRIMUS.Eier E
			WHERE grp.gruppeid=OB.gruppe
			AND grp.Eierid=E.Eierid
			),
		  (
			select XMLAGG(XMLELEMENT(REGNIVAA, GT_L.BESKRIVELSE))
			FROM PRIMUS.GRUPPERINGSTYPE_L GT_L, primus.gruppering_h gh
			WHERE GT_L.TYPEID=gh.TYPEID
			And OB.objid = gh.objid AND OB.hid_gruppering = gh.hid
			),
			(
			SELECT XMLAGG(XMLELEMENT(JP_NAVN, XMLFOREST(JP.JPNAVN AS NAVN , RL.beskrivelse AS ROLLEBESKRIVELSE, RSL.JURPERSONSTATUS as JURPERSONSTATUS	)))
			FROM primus.person_objekt po, primus.historikk h, primus.jurperson jP, primus.rollejp_l RL, ROLLEJPSTATUS_L RSL
			WHERE po.histid_siste = h.histid
			AND   po.rollekode = RL.rollekode
			AND   po.rolleid   = RL.rolleid
			AND   PO.ROLLEJPSTATUSID = RSL.STATUSID(+)
			AND   po.jpnr = jP.jpnr
			AND   po.histid_siste is not null
			AND   h.hid_slettet is null
			AND   h.OBJID=OB.OBJID
			AND   RL.ROLLEID IN (100, 101,102)
		  ),
		 (
			SELECT XMLAGG(XMLELEMENT(KLAUSUL, XMLFOREST( RL.BESKRIVELSE AS KLAUSUL_TEKST , RD.FRA_DATO AS FRADATO , RD.TIL_DATO AS TILDATO )	))
			FROM PRIMUS.RETTIGHET R,    PRIMUS.RETTIGHET_L RL, PRIMUS.RETTIGHET_DATERING RD
			WHERE R.OBJID=OB.OBJID
			AND R.RETTIGHETTYPEID = RL.RETTIGHETTYPEID
			AND R.RETTIGHETID=RD.RETTIGHETID(+)
			AND R.HID_KOMMENTAR=RD.HID(+)
			AND RL.typeid=3
			AND R.HID_SLETTET  IS NULL
			AND (RD.FRA_DATO < SYSDATE OR RD.FRA_DATO IS NULL)
			AND (RD.TIL_DATO > SYSDATE OR RD.TIL_DATO IS NULL)
		  ),
		  (
		  SELECT XMLAGG(XMLELEMENT(PDATERING,
						XMLFOREST(CONVERTDATE(DH.DATO_FRA, SUBSTR(TO_CHAR( DH.DATO_FRA, ''SYYYY''),1,5)) AS DATOF ,
								  CONVERTDATE(DH.DATO_TIL, SUBSTR(TO_CHAR( DH.DATO_TIL, ''SYYYY''),1,5)) AS DATOT
			  )
				))
			FROM PRIMUS.DATERING_H DH , PRIMUS.HISTORIKK HK
		  WHERE HK.HID_DATERING= DH.HID
		  AND HK.HISTID=DH.HISTID
		  AND HK.OBJID=OB.OBJID
		  AND HK.HID_SLETTET IS NULL
		  AND HK.HISTTYPE in (101, 100)
		  )
		)
		INTO OBJECTXML FROM PRIMUS.OBJEKT OB WHERE OB.OBJID=OBJID_IN ;
		RETURN OBJECTXML;
		END XML_KOPI;');
End if;	
jobbnummer := 26; 
    if jobbnummer_inn <= jobbnummer then		 
      execute immediate( '
      create or replace function XML_ELEMENT_PRODUKSJONDATERING(objid_in number, hid_in number) 
	  return xmltype as objectxml xmltype;
      begin
	      SELECT XMLAGG(XMLELEMENT(BDATERING, BD.VERDI)) INTO objectxml
				FROM PRIMUS.OBJ_PRODUKSJONSDATERING_H BD
				WHERE BD.OBJID=objid_in and BD.HID=hid_in;	     
        return Objectxml;
      end XML_ELEMENT_PRODUKSJONDATERING;
      ');
    end if;	

jobbnummer:= 27;
    if jobbnummer_inn <= jobbnummer then 
      execute immediate ('
	  create or replace FUNCTION  XML_OBJECT_BYGING(OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
     BEGIN
   SELECT  XMLELEMENT(OBJEKT,	XMLFOREST(OV.MUSEUMSNR AS MUSEUMSNUMMER ,  OV.OBJTYPEID AS OBJTYPEID, OV.ANTALL as ANTALLOBJ ), 
	(
    SELECT XMLAGG(XMLELEMENT(GABNROGBYGNINGTYPE,
      XMLFOREST(BF.TEKST GABNROGBYGNINGTYPE
      )
    ))
    FROM PRIMUS.BYGNING_FELTER BF WHERE BF.OBJID_SISTE =OBJID_IN AND BF.FELTNR IN(1,2 ,10)
  ),
  (
    SELECT XMLAGG(XMLELEMENT(BETEGNELSE,
      XMLFOREST(MH.MATERIALE BETEGNELSE
      )
    ))
    FROM PRIMUS.MATERIALE_H  MH WHERE MH.OBJID_SISTE =OBJID_IN
  ),
    (
    SELECT XMLAGG(XMLELEMENT(SAMLING,
      XMLFOREST(PO.GRUPPE
      )
    ))
    FROM PRIMUS.OBJEKT PO WHERE PO.OBJID = OBJID_IN
  )    
)
INTO OBJECTXML FROM PRIMUS.OBJEKT_V OV WHERE OV.OBJID=OBJID_IN ;
RETURN OBJECTXML;
END XML_OBJECT_BYGING;');
End if;	  
	  
jobbnummer:= 28;
    if jobbnummer_inn <= jobbnummer then 
			  execute immediate ('create or replace FUNCTION  XML_OBJECT_FOTO(OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
				BEGIN
		  SELECT  XMLELEMENT(OBJEKT,	XMLFOREST(OV.MUSEUMSNR AS MUSEUMSNUMMER ,  OV.OBJTYPEID AS OBJTYPEID, OV.ANTALL as ANTALLOBJ, OV.SAMLING AS SAMLING ),
			(
			SELECT XMLAGG(XMLELEMENT(TITTELNAN,	XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,
			O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
					)
				))
			FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) IN (''NOR'', ''NOB'', ''NNY'') AND O_TV.STATUSID IN(''1'')
			),
		  (
				SELECT XMLAGG(XMLELEMENT(TITTELEAN,
					XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,  O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
					)
				))
			FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) IN (''ENG'') AND O_TV.STATUSID IN (''1'')
			),
		  (
				SELECT XMLAGG(XMLELEMENT(TITTELOTHAN,
					XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,  O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
					)
				))
			FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) NOT IN (''NOR'', ''NOB'', ''NNY'', ''ENG'') AND O_TV.STATUSID IN (''1'')
			),
			(
				SELECT XMLAGG(XMLELEMENT(TITTELNOR,
					XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,  O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
					)
				))
			FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) IN (''NOR'', ''NOB'', ''NNY'') AND O_TV.STATUSID IN(''7'')
			),
		  (
				SELECT XMLAGG(XMLELEMENT(TITTELOTH,
					XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,  O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
					)
				))
			FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) NOT IN (''NOR'', ''NOB'', ''NNY'') AND O_TV.STATUSID IN(''7'')
			),
		  (
				SELECT XMLAGG(XMLELEMENT(REG_NIVA,		XMLFOREST(GT_L.BESKRIVELSE AS REG_NIVA
					)
				))
			FROM PRIMUS.GRUPPERINGSTYPE_L GT_L WHERE GT_L.TYPEID=OV.GRUPPERINGSTYPEID
			),
		  (

				SELECT XMLAGG(XMLELEMENT(JP,	XMLFOREST(OJP_V.NAVN AS NAVN)))
			FROM PRIMUS.OBJEKT_JPROLLE_V OJP_V WHERE OJP_V.OBJID=OV.OBJID
			AND OJP_V.ROLLEID IN (100,101)
		  ),
		  (
				SELECT XMLAGG(XMLELEMENT(MOTIV,			XMLFOREST(O_M_H.MOTIV2000 AS MOTIV
					)
				))
			FROM PRIMUS.OBJ_MOTIV_H O_M_H WHERE O_M_H.OBJID=OV.OBJID AND O_M_H.HID=OV.HID_MOTIV
			),
		 (
		  SELECT XMLAGG(XMLELEMENT(KLAUSUL,
					XMLFOREST( O_KV.TEKST AS KLAUSUL_TEKST , O_KV.FDATO AS FRADATO , O_KV.TDATO AS TILDATO
			  )
				))
			FROM PRIMUS.RETTIGHET_V O_KV WHERE O_KV.OBJID=OV.OBJID AND (O_KV.FDATO < SYSDATE OR O_KV.FDATO IS NULL) AND (O_KV.TDATO > SYSDATE OR O_KV.TDATO IS NULL)
		  )

		)
		INTO OBJECTXML FROM PRIMUS.OBJEKT_V OV WHERE OV.OBJID=OBJID_IN ;
		RETURN OBJECTXML;
		END XML_OBJECT_FOTO; ');
End if;			

jobbnummer:= 29;
    if jobbnummer_inn <= jobbnummer then 
      execute immediate ('create or replace FUNCTION  XML_OBJECT_OPPTAK(OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
BEGIN
  SELECT  XMLELEMENT(OBJEKT,	XMLFOREST(OV.MUSEUMSNR AS MUSEUMSNUMMER ,  OV.OBJTYPEID AS OBJTYPEID, OV.ANTALL as ANTALLOBJ, OV.SAMLING AS SAMLING ),
	(
  	SELECT XMLAGG(XMLELEMENT(TITTELNAN,	XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,  O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
			)
		))
	FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) IN (''NOR'', ''NOB'', ''NNY'') AND O_TV.STATUSID IN(''1'')
	),
  (
		SELECT XMLAGG(XMLELEMENT(TITTELEAN,
			XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,  O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
			)
		))
	FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) IN (''ENG'') AND O_TV.STATUSID IN (''1'')
	),
  (
		SELECT XMLAGG(XMLELEMENT(TITTELOTHAN,
			XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,  O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
			)
		))
	FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) NOT IN (''NOR'', ''NOB'', ''NNY'', ''ENG'') AND O_TV.STATUSID IN (''1'')
	),
  	(
		SELECT XMLAGG(XMLELEMENT(TITTELNOR,
			XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,  O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
			)
		))
	FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) IN (''NOR'', ''NOB'', ''NNY'') AND O_TV.STATUSID IN(''7'')
	),
  (
		SELECT XMLAGG(XMLELEMENT(TITTELOTH,
			XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,  O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
			)
		))
	FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) NOT IN (''NOR'', ''NOB'', ''NNY'') AND O_TV.STATUSID IN(''7'')
	),
  (
		SELECT XMLAGG(XMLELEMENT(REG_NIVA,		XMLFOREST(GT_L.BESKRIVELSE AS REG_NIVA
			)
		))
	FROM PRIMUS.GRUPPERINGSTYPE_L GT_L WHERE GT_L.TYPEID=OV.GRUPPERINGSTYPEID
	),
 (
		SELECT XMLAGG(XMLELEMENT(JP,	XMLFOREST(OJP_V.NAVN AS NAVN)))
    FROM PRIMUS.OBJEKT_JPROLLE_V OJP_V WHERE OJP_V.OBJID=OV.OBJID
    AND OJP_V.ROLLEID IN (100, 101)
  ),
  (
  SELECT XMLAGG(XMLELEMENT(KLAUSUL,
			XMLFOREST( O_KV.TEKST AS KLAUSUL_TEKST , O_KV.FDATO AS FRADATO , O_KV.TDATO AS TILDATO
      )
		))
	FROM PRIMUS.RETTIGHET_V O_KV WHERE O_KV.OBJID=OV.OBJID AND (O_KV.FDATO < SYSDATE OR O_KV.FDATO IS NULL)
AND (O_KV.TDATO > SYSDATE OR O_KV.TDATO IS NULL)
  ),
  (
  SELECT XMLAGG(XMLELEMENT(TEKNIKK,
			XMLFOREST(TEKNIKKTEKST.TEKNIKKTEKST AS TEKNIKKTEKST
      )
		))
	FROM PRIMUS.TEKNIKKTEKST_H TEKNIKKTEKST WHERE TEKNIKKTEKST.OBJID=OV.OBJID AND TEKNIKKTEKST.HID=OV.HID_TEKNIKKTEKST AND NOT(TEKNIKKTEKST.TEKNIKKTEKST IS NULL)
  )


)
INTO OBJECTXML FROM PRIMUS.OBJEKT_V OV WHERE OV.OBJID=OBJID_IN ;
RETURN OBJECTXML;
END XML_OBJECT_OPPTAK; ');
End if;

jobbnummer:= 30;
    if jobbnummer_inn <= jobbnummer then 
      execute immediate ('create or replace FUNCTION  XML_OPPTAK(OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE;
			BEGIN
			  SELECT  XMLELEMENT(OBJEKT,	XMLFOREST(OB.MUSEUMSNR AS MUSEUMSNUMMER,  OB.OBJTYPEID AS OBJTYPEID),
			  ( select XMLAGG(TT.nxml)	FROM (Select HENTOBJEKTTITTEL(objid_in) as nxml from Dual)  TT  ),
			  (
				select XMLAGG(XMLELEMENT(GRUPPE, XMLFOREST(grp.betegnelse as SAMLING, E.MUSEUM_NAVN as EIER)))
				FROM PRIMUS.gruppe grp, PRIMUS.Eier E
				WHERE grp.gruppeid=OB.gruppe
				AND grp.Eierid=E.Eierid
				),
			  (
				select XMLAGG(XMLELEMENT(REGNIVAA, GT_L.BESKRIVELSE))
				FROM PRIMUS.GRUPPERINGSTYPE_L GT_L, primus.gruppering_h gh
				WHERE GT_L.TYPEID=gh.TYPEID
				And OB.objid = gh.objid AND OB.hid_gruppering = gh.hid
				),
				(
				SELECT XMLAGG(XMLELEMENT(JP_NAVN, XMLFOREST(JP.JPNAVN AS NAVN , RL.beskrivelse AS ROLLEBESKRIVELSE, RSL.JURPERSONSTATUS as JURPERSONSTATUS	)))
				FROM primus.person_objekt po, primus.historikk h, primus.jurperson jP, primus.rollejp_l RL, ROLLEJPSTATUS_L RSL
				WHERE po.histid_siste = h.histid
				AND   po.rollekode = RL.rollekode
				AND   po.rolleid   = RL.rolleid
				AND   PO.ROLLEJPSTATUSID = RSL.STATUSID(+)
				AND   po.jpnr = jP.jpnr
				AND   po.histid_siste is not null
				AND   h.hid_slettet is null
				AND   h.OBJID=OB.OBJID
				AND   RL.ROLLEID IN (100, 101,102)
			  ),
			 (
				SELECT XMLAGG(XMLELEMENT(KLAUSUL, XMLFOREST( RL.BESKRIVELSE AS KLAUSUL_TEKST , RD.FRA_DATO AS FRADATO , RD.TIL_DATO AS TILDATO )	))
				FROM PRIMUS.RETTIGHET R,    PRIMUS.RETTIGHET_L RL, PRIMUS.RETTIGHET_DATERING RD
				WHERE R.OBJID=OB.OBJID
				AND R.RETTIGHETTYPEID = RL.RETTIGHETTYPEID
				AND R.RETTIGHETID=RD.RETTIGHETID(+)
				AND R.HID_KOMMENTAR=RD.HID(+)
				AND RL.typeid=3
				AND R.HID_SLETTET  IS NULL
				AND (RD.FRA_DATO < SYSDATE OR RD.FRA_DATO IS NULL)
				AND (RD.TIL_DATO > SYSDATE OR RD.TIL_DATO IS NULL)
			  ),
			  (
			  SELECT XMLAGG(XMLELEMENT(TEKNIKK,	TEKNIKKTEKST.TEKNIKKTEKST))
				FROM PRIMUS.TEKNIKKTEKST_H TEKNIKKTEKST
				WHERE TEKNIKKTEKST.OBJID=OB.OBJID
				AND TEKNIKKTEKST.HID=OB.HID_TEKNIKKTEKST
				AND TEKNIKKTEKST.TEKNIKKTEKST IS NOT NULL
			  ),
			  (
			  SELECT XMLAGG(XMLELEMENT(PDATERING,
							XMLFOREST(CONVERTDATE(DH.DATO_FRA, SUBSTR(TO_CHAR( DH.DATO_FRA, ''SYYYY''),1,5)) AS DATOF ,
									  CONVERTDATE(DH.DATO_TIL, SUBSTR(TO_CHAR( DH.DATO_TIL, ''SYYYY''),1,5)) AS DATOT
				  )
					))
				FROM PRIMUS.DATERING_H DH , PRIMUS.HISTORIKK HK
			  WHERE HK.HID_DATERING= DH.HID
			  AND HK.HISTID=DH.HISTID
			  AND HK.OBJID=OB.OBJID
			  AND HK.HID_SLETTET IS NULL
			  AND HK.HISTTYPE in (101, 100)
			  )
			)
			INTO OBJECTXML FROM PRIMUS.OBJEKT OB WHERE OB.OBJID=OBJID_IN ;
			RETURN OBJECTXML;
			END XML_OPPTAK; ');
End if;


jobbnummer := 31; 
    if jobbnummer_inn <= jobbnummer then		 
      execute immediate( '
      create or replace function XML_ELEMENT_PROVENIENSDATERING(objid_in number, hid_in number) 
	  return xmltype as objectxml xmltype;
      begin
	  	      SELECT XMLAGG(XMLELEMENT(ERVERVELSE, OPD.VERDI)) INTO objectxml
				FROM PRIMUS.OBJ_PROVENIENSDATERING_H OPD
				WHERE OPD.OBJID=OBJID_IN and OPD.HID=hid_in;		
				
        return Objectxml;
      end XML_ELEMENT_PROVENIENSDATERING;');
    end if;			
	

 jobbnummer:= 32;
    if jobbnummer_inn <= jobbnummer then      
      execute immediate ('
        create or replace FUNCTION  XML_BLK_ARK_DES(OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE; BEGIN
			  SELECT  XMLELEMENT(OBJEKT, XMLFOREST(OB.MUSEUMSNR AS MUSEUMSNUMMER, OB.OBJTYPEID AS OBJTYPEID),
			  ( select XMLAGG(TT.nxml)	FROM (Select HENTOBJEKTTITTEL(objid_in) as nxml from Dual)  TT  ),
			  (
				SELECT XMLAGG(XMLELEMENT(JP_NAVN, XMLFOREST(JP.JPNAVN AS NAVN , RL.beskrivelse AS ROLLEBESKRIVELSE, RSL.JURPERSONSTATUS as JURPERSONSTATUS)))
				FROM primus.person_objekt po, primus.historikk h, primus.jurperson jP, primus.rollejp_l RL, ROLLEJPSTATUS_L RSL
				WHERE po.histid_siste = h.histid
				AND   po.rollekode = RL.rollekode
				AND   po.rolleid   = RL.rolleid
				AND   PO.ROLLEJPSTATUSID = RSL.STATUSID(+)
				AND   po.jpnr = jP.jpnr
				AND   po.histid_siste is not null
				AND   h.hid_slettet is null
				AND   h.OBJID=OB.OBJID
				AND   RL.ROLLEID IN (14,15,16,18,181,191,26,64,67, 90, 961,103,100, 102,
									 109,111,112,113,114,115, 116, 117, 118, 141,142,143, 76, 208, 213)
			  ),
			  (
				SELECT XMLAGG(XMLELEMENT(KUNSTNER, XMLFOREST(JP.JPNAVN AS NAVN , RL.beskrivelse AS ROLLEBESKRIVELSE	, RSL.JURPERSONSTATUS as JURPERSONSTATUS)))
				FROM  primus.person_objekt po, primus.historikk h, primus.jurperson jP, primus.rollejp_l RL, ROLLEJPSTATUS_L RSL
				WHERE po.histid_siste = h.histid
				AND   po.rollekode = RL.rollekode
				AND   po.rolleid   = RL.rolleid
				AND   PO.ROLLEJPSTATUSID = RSL.STATUSID(+)
				AND   po.jpnr = jP.jpnr
				AND   po.histid_siste is not null
				AND   h.hid_slettet is null
				AND   h.OBJID=OB.OBJID
				AND   RL.ROLLEID IN (101)
				AND   PO.ROLLEJPSTATUSID <> 4
			  ),
			 (
			   SELECT XMLAGG(XMLELEMENT(STED,	XMLFOREST(SD.STEDSNAVN AS NAVN , RL.BESKRIVELSE AS ROLLEBESKRIVELSE	, RSL.STEDSTATUS as STEDSTATUS)))
				from primus.objekt_sted os, primus.sted sd, primus.rollested_l RL, primus.historikk h, ROLLESTEDSTATUS_L RSL
				where os.rollekode = rl.rollekode
				and os.rolleid = rl.rolleid
				AND OS.ROLLESTEDSTATUSID = RSL.STATUSID(+)
				and os.stedid = sd.stedid
				AND os.HISTID_SISTE IS NOT NULL
				AND os.HISTID_SISTE = H.HISTID
				And h.hid_slettet is null
				AND h.objid = ob.objid
				AND rl.ROLLEID IN(10)
				),
			  (
			   SELECT XMLAGG(XMLELEMENT(BETEGNELSE,	bl.beskrivelse ))
				FROM primus.OBJEKT_BETEGNELSE obb, primus.betegnelse_l bl
				WHERE obb.objid_siste=OB.OBJID
				AND obb.betid = bl.kode
			 ),
			  (
				  SELECT XMLAGG(XMLELEMENT(PBETEGNELSE,	typ.beskrivelse))
				  From primus.objekt_type objt , primus.type_l typ
				  where objt.type=typ.kode
				  and objt.objid_siste = OB.OBJID
				),
			  (
				SELECT XMLAGG(XMLELEMENT(KLAUSUL, XMLFOREST( RL.BESKRIVELSE AS KLAUSUL_TEKST , RD.FRA_DATO AS FRADATO , RD.TIL_DATO AS TILDATO )	))
				FROM PRIMUS.RETTIGHET R,    PRIMUS.RETTIGHET_L RL, PRIMUS.RETTIGHET_DATERING RD
				WHERE R.OBJID=OB.OBJID
				AND R.RETTIGHETTYPEID = RL.RETTIGHETTYPEID
				AND R.RETTIGHETID=RD.RETTIGHETID(+)
				AND R.HID_KOMMENTAR=RD.HID(+)
				AND RL.typeid=3
				AND R.HID_SLETTET  IS NULL
				AND (RD.FRA_DATO < SYSDATE OR RD.FRA_DATO IS NULL)
				AND (RD.TIL_DATO > SYSDATE OR RD.TIL_DATO IS NULL)
			  )
			  ,(XML_ELEMENT_PROVENIENSDATERING(OBJID_IN, OB.HID_PROVENIENSDTRNG))
              ,(XML_ELEMENT_PRODUKSJONDATERING(OBJID_IN, OB.HID_PRODUKSJONSDTRNG))
			  ,(
			  SELECT XMLAGG(XMLELEMENT(MAAL, MAALTEKST.MAALTEKST))
				FROM PRIMUS.MAALTEKST_H MAALTEKST
				WHERE MAALTEKST.OBJID=OB.OBJID
				AND   MAALTEKST.HID=OB.HID_MAALTEKST
				AND   MAALTEKST.MAALTEKST IS not NULL
			  ),
			  (
			  SELECT XMLAGG(XMLELEMENT(TEKNIKK,TEKNIKKTEKST.TEKNIKKTEKST))
				FROM PRIMUS.TEKNIKKTEKST_H TEKNIKKTEKST
				WHERE TEKNIKKTEKST.OBJID=OB.OBJID
				AND TEKNIKKTEKST.HID=OB.HID_TEKNIKKTEKST
				AND TEKNIKKTEKST.TEKNIKKTEKST IS NOT NULL
			  ),
			  (
			  SELECT XMLAGG(XMLELEMENT(MATERIALE,MATERIALETEKST.MATERIALETEKST))
				FROM PRIMUS.MATERIALETEKST_H MATERIALETEKST
				WHERE MATERIALETEKST.OBJID=OB.OBJID
				AND MATERIALETEKST.HID=OB.HID_MATERIALETEKST
				AND MATERIALETEKST.MATERIALETEKST IS NOT NULL
			  )
			)
			INTO OBJECTXML FROM PRIMUS.OBJEKT OB WHERE OB.OBJID=OBJID_IN and OBJTYPEID in (1,10,11) and OB.HID_SLETTET is NULL;
			RETURN OBJECTXML;
			END XML_BLK_ARK_DES;');
End if;		

jobbnummer:= 33;
    if jobbnummer_inn <= jobbnummer then 
      execute immediate ('	
    create or replace FUNCTION  XML_OBJECT(OBJID_IN NUMBER) RETURN XMLTYPE AS OBJECTXML XMLTYPE; BEGIN
  SELECT  XMLELEMENT(OBJEKT,	XMLFOREST(OV.MUSEUMSNR AS MUSEUMSNUMMER,  OV.OBJTYPEID AS OBJTYPEID, OV.ANTALL as ANTALLOBJ, OV.SAMLING AS SAMLING ),
	(SELECT XMLAGG(XMLELEMENT(TITTELNAN,	XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,  O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE)))
	 FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) IN (''NOR'', ''NOB'', ''NNY'') AND O_TV.STATUSID IN(''1'')
	),
  (
		SELECT XMLAGG(XMLELEMENT(TITTELEAN,
			XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,
			O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
			)
		))
	FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) IN (''ENG'') AND O_TV.STATUSID IN (''1'')
	),
  (
		SELECT XMLAGG(XMLELEMENT(TITTELOTHAN,
			XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,  O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
			)
		))
	FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) NOT IN (''NOR'', ''NOB'', ''NNY'', ''ENG'') AND O_TV.STATUSID IN (''1'')
	),
  	(
		SELECT XMLAGG(XMLELEMENT(TITTELNOR,
			XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,  O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
			)
		))
	FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) IN (''NOR'', ''NOB'', ''NNY'') AND O_TV.STATUSID IN(''7'')
	),
  (
		SELECT XMLAGG(XMLELEMENT(TITTELOTH,
			XMLFOREST(O_TV.TITTEL AS TITTEL , O_TV.STATUS AS STATUS , O_TV.FRADATO AS FRADATO ,  O_TV.TILDATO AS TILDATO , O_TV.KODE AS SKODE
			)
		))
	FROM PRIMUS.OBJEKT_TITTEL_V O_TV WHERE O_TV.OBJID=OV.OBJID AND TRIM(UPPER(O_TV.KODE)) NOT IN (''NOR'', ''NOB'', ''NNY'') AND O_TV.STATUSID IN(''7'')
	),
  (
	select XMLAGG(xmlelement(REG_NIVA,		xmlforest(GT_L.BESKRIVELSE as REG_NIVA)	))
	  FROM PRIMUS.GRUPPERINGSTYPE_L GT_L WHERE GT_L.TYPEID=OV.GRUPPERINGSTYPEID
	),
  (
    SELECT XMLAGG(XMLELEMENT(INGORI,		XMLFOREST( OBJEKT.MUSEUMSNR AS INGORI
			)
		))
	FROM PRIMUS.OBJEKT , PRIMUS.ANTALL WHERE ( ANTALL.OBJID = OBJEKT.OBJID )
  AND ( OBJEKT.HID_SLETTET IS NULL )
  AND ( ANTALL.OBJEKTID = OV.OBJID )
  ),
  	(
		SELECT XMLAGG(XMLELEMENT(BETEGNELSE,			XMLFOREST(B_V.BETEGNELSE AS BET
			)
		))
	FROM PRIMUS.BETEGNELSE_V B_V WHERE B_V.OBJID=OV.OBJID
	),
  (
		SELECT XMLAGG(XMLELEMENT(PBETEGNELSE,			XMLFOREST(PB_V.PRESISERTBETEGNELSE AS PBET
			)
		))
	FROM PRIMUS.PRESISERTBETEGNELSE_V PB_V WHERE PB_V.OBJID=OV.OBJID
	),
  (
		SELECT XMLAGG(XMLELEMENT(VAREMERKE,			XMLFOREST(V.VAREMERKE AS  VAREMERKE
			)
		))
	FROM PRIMUS.VAREMERKE_H V WHERE V.OBJID_SISTE=OV.OBJID
	),
	(
		SELECT XMLAGG(XMLELEMENT(JP,	XMLFOREST(OJP_V.NAVN AS NAVN)))
    FROM PRIMUS.OBJEKT_JPROLLE_V OJP_V WHERE OJP_V.OBJID=OV.OBJID
    AND OJP_V.ROLLEID IN (14,15,16,18,181,191,26,64,67, 90, 961,103,100, 102,101,109,111,112,113,114,115, 116, 117, 118, 141,142,143, 76)
  ),
  (
   SELECT XMLAGG(XMLELEMENT(KUNSTNER,	XMLFOREST(OJP_V.NAVN AS NAVN , OJP_V.ROLLEBESKRIVELSE AS ROLLEBESKRIVELSE	)		))
	 FROM PRIMUS.OBJEKT_JPROLLE_V OJP_V WHERE OJP_V.OBJID=OV.OBJID AND OJP_V.ROLLEID IN(101) AND OJP_V.ROLLEJPSTATUSID <> 4
	),
  (
  SELECT XMLAGG(XMLELEMENT(KLAUSUL,
			XMLFOREST( O_KV.TEKST AS KLAUSUL_TEKST , O_KV.FDATO AS FRADATO , O_KV.TDATO AS TILDATO
      )
		))
	FROM PRIMUS.RETTIGHET_V O_KV WHERE O_KV.OBJID=OV.OBJID AND (O_KV.FDATO < SYSDATE OR O_KV.FDATO IS NULL)
AND (O_KV.TDATO > SYSDATE OR O_KV.TDATO IS NULL)
  ),
  (
    SELECT XMLAGG(XMLELEMENT(EIER,
      XMLFOREST(E.MUSEUM_NAVN
      )
    ))
    FROM PRIMUS.EIER E WHERE E.MUSEUM_NAVN = OV.EIER
  )
  , (XML_ELEMENT_PROVENIENSDATERING(OBJID_IN, OV.HID_PROVENIENSDTRNG))
  , (XML_ELEMENT_PRODUKSJONDATERING(OBJID_IN, OV.HID_PRODUKSJONSDTRNG))
  ,
  (
  SELECT XMLAGG(XMLELEMENT(PDATERING,
				XMLFOREST(CONVERTDATE(DH.DATO_FRA, SUBSTR(TO_CHAR( DH.DATO_FRA, ''SYYYY''),1,5)) AS DATOF ,
                          CONVERTDATE(DH.DATO_TIL, SUBSTR(TO_CHAR( DH.DATO_TIL, ''SYYYY''),1,5)) AS DATOT
      )
		))
	FROM PRIMUS.DATERING_H DH , PRIMUS.HISTORIKK HK
  WHERE HK.HID_DATERING= DH.HID
  AND HK.HISTID=DH.HISTID
  AND HK.OBJID=OV.OBJID
  AND HK.HID_SLETTET IS NULL
  AND (HK.HISTTYPE=100)
  ),
  (
  SELECT XMLAGG(XMLELEMENT(MAAL,
			XMLFOREST(MAALTEKST.MAALTEKST AS MAALTEKST
      )
		))
	FROM PRIMUS.MAALTEKST_H MAALTEKST WHERE MAALTEKST.OBJID=OV.OBJID
  AND MAALTEKST.HID=OV.HID_MAALTEKST AND  NOT( MAALTEKST.MAALTEKST IS NULL)
  ),
  (
  SELECT XMLAGG(XMLELEMENT(TEKNIKK,
			XMLFOREST(TEKNIKKTEKST.TEKNIKKTEKST AS TEKNIKKTEKST
      )
		))
	FROM PRIMUS.TEKNIKKTEKST_H TEKNIKKTEKST WHERE TEKNIKKTEKST.OBJID=OV.OBJID
  AND TEKNIKKTEKST.HID=OV.HID_TEKNIKKTEKST AND NOT(TEKNIKKTEKST.TEKNIKKTEKST IS NULL)
  ),
  (
  SELECT XMLAGG(XMLELEMENT(MATERIALE,
			XMLFOREST(MATERIALETEKST.MATERIALETEKST AS MATERIALETEKST
      )
		))
	FROM PRIMUS.MATERIALETEKST_H MATERIALETEKST WHERE MATERIALETEKST.OBJID=OV.OBJID
  AND MATERIALETEKST.HID=OV.HID_MATERIALETEKST AND NOT(MATERIALETEKST.MATERIALETEKST IS NULL)
  )
)
INTO OBJECTXML FROM PRIMUS.OBJEKT_V OV WHERE OV.OBJID=OBJID_IN ;
RETURN OBJECTXML;
END XML_OBJECT;');

End if;	
	
	

			
		
		jobbnummer:= 34;
		if  jobbnummer_inn <= jobbnummer then		  
	     EXECUTE IMMEDIATE 'CREATE OR REPLACE FORCE VIEW PRIMUS.TMP_UTLAAN_XML (OBJID, XML_DATA) AS 
                           SELECT ADMH_OBJID, XML_UTLAAN(ADMH_OBJID) XML_DATA FROM UTLAAN';
        End if;	
		
		
		 commit;
		return 'OK';
	exception
		when others then			
			SYS.DBMS_OUTPUT.PUT_LINE('primus_xml_element feilet! Jobbnummer: ' || jobbnummer);			
			return 'FEIL!';
	end;
	
end;
/




create or replace function primus40(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
     check if Column DIMUKODE
	*/

	begin

  
	jobbnummer:=1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE
		'CREATE TABLE kommentar
			(	objid NUMBER(8,0)
			, materialbruk VARCHAR2(4000 char)
			, CONSTRAINT pk_kommentar PRIMARY KEY (objid)
		) TABLESPACE primusdt';
	End if;
	jobbnummer:=100;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus40 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(40, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/	

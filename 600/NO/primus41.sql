create or replace function primus41(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
     check if Column DIMUKODE
	*/

	begin

	jobbnummer:=1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE
		'create table video(
        objid number(8,0) not null enable,
        hid number(8,0) not null enable,
        hid_slettet number(8,0),
        uuid varchar2(40) not null enable,
        mimetype varchar2(100 char),
        format_kode varchar2(50 char),
        storrelse number(8,0),
        filnavn varchar2(256 char),
        filtype varchar2(4 char),
        bredde number(6,0),
        hoeyde number(6,0),
        varighet number(10,0),
        opploesning number(5,0),
        fotograf number(8,0),
        beskrivelse varchar2(4000 char),
        bilde blob,
        constraint pk_video primary key (objid),
        constraint fk_video_superobjekt foreign key (objid)
          references superobjekt(objid) enable,
        constraint fk_video_hendelse foreign key (hid)
          references hendelse (hid) enable,
        constraint fk_video_hendelse_slettet foreign key (hid_slettet)
          references hendelse (hid) enable,
        constraint fk_video_fotograf foreign key (fotograf)
          references jurperson (jpnr) enable) tablespace primusdt';
	End if;

  jobbnummer:=2;
  if jobnummer_in <= jobbnummer then
    EXECUTE IMMEDIATE
    'create table video_bfile(
      objid number(8,0) not null enable,
      hid number(8,0) not null enable,
      originalfilnavn varchar2(256 char),
      fil bfile,
      constraint pk_video_bfile primary key (objid),
      constraint fk_video_bfile_objekt foreign key (objid)
        references video(objid) enable,
      constraint fk_video_bfile_hendelse foreign key (hid)
          references hendelse (hid) enable) tablespace primusdt';
  End if;

  jobbnummer:=3;
  if jobnummer_in <= jobbnummer then
    EXECUTE IMMEDIATE
    'insert into superobjekttype_l(superobjekttypeid, beskrivelse)
     values(7, ''VIDEO'')';
  End if;

  jobbnummer:=4;
  if jobnummer_in <= jobbnummer then
    EXECUTE IMMEDIATE
    'create table objekt_video(
      objid number(8,0) not null enable,
      hid number(8,0) not null enable,
      nr number(7,0) not null enable,
      video_objid number(8,0),
      objid_siste number(8,0),
      constraint pk_objekt_video primary key (objid, hid, nr),
      constraint fk_objekt_video_so_id foreign key (objid)
        references superobjekt (objid) on delete cascade enable,
      constraint fk_objekt_video_so_id_siste foreign key (objid_siste)
        references superobjekt (objid) on delete cascade enable,
      constraint fk_objekt_video_hendelse foreign key (hid)
        references hendelse(hid) enable,
      constraint fk_objekt_video_video_objid foreign key (video_objid)
        references video(objid) enable)';
  End if;

  jobbnummer:=100;
      return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus41 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(41, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/	

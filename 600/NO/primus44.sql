create or replace function primus44(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
		Metadata tabell
	*/

	begin

	jobbnummer:=1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE
		'CREATE TABLE metadata_field
			( signid number(6,0) not null
  			, object_type varchar2(50) not null
  			, field_name varchar2(50) not null
  			, field_display varchar2(3) null
  			, field_edit varchar2(4) null
  			, field_order number(3,0)
  			, constraint pk_metadata_field primary key
    				(signid, object_type, field_name)
  			, constraint fk_metadata_field_signid foreign key (signid)
    				references signatur (signid) enable
			) TABLESPACE primusdt';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
			EXECUTE IMMEDIATE
			'CREATE TABLE annotering
			  ( OBJID NUMBER(8,0) NOT NULL ENABLE
			    , BILDE BLOB
		      , CONSTRAINT PK_ANNOTERING PRIMARY KEY (OBJID)
		      , CONSTRAINT FK_ANNOTERING_ADMH_OBJID FOREIGN KEY (OBJID)
			        REFERENCES ADM_HENDELSE (OBJID) ENABLE)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
			EXECUTE IMMEDIATE 'alter table dokument add (bilde blob)';
	End if;
	jobbnummer:= jobbnummer+1;
	if jobnummer_in <= jobbnummer then
		EXECUTE IMMEDIATE 'alter table dokument add (mimetype varchar2(100 char))';
	End if;
	jobbnummer:=100;
    return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus44 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(44, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/

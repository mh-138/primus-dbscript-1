create or replace function primus39(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
     check if Column DIMUKODE
	*/

	begin

  
	jobbnummer:=1;	
	if jobnummer_in <= jobbnummer then
     BEGIN
			  EXECUTE IMMEDIATE
			  'alter table bilde add
			  	( HID_SLETTET NUMBER(8,0),
		 				CONSTRAINT HENDELSE_SLETTET_BILDE FOREIGN KEY (HID_SLETTET)
		 					REFERENCES HENDELSE (HID) ENABLE)';
	 Exception
	   WHEN OTHERS THEN
        IF SQLCODE = -1430 THEN
           NULL; -- suppresses ORA-01430 exception
        ELSE
         RAISE;
        END IF;
	 End;		
	END IF;
	jobbnummer:=100;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus39 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(39, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/	

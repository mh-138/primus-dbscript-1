create or replace function primus53(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
		Data overføring fra MUSHENDELSE til ADM_HENDELSE.
	*/

	begin
	
		jobbnummer := 1; --------- Revsjon  -------------
		PrimusOppdJobbNrIVersjon( 53, jobbnummer );
		
		if jobnummer_in <= jobbnummer then		
			execute immediate '
			  create or replace PROCEDURE SAVE_AH_REVBESKRIVELSE(nAHobjid NUMBER, nhid NUMBER, besk varchar2) IS     			
			  BEGIN			 			 
				 Insert Into OBJ_BESKRIVELSE_H(OBJID, HID, BESKRIVELSE) values (nAHobjid, nhid, besk); 
				 UPDATE ADM_HENDELSE set HID_BESKRIVELSE = nHid where objid = nAHobjid;			 
			  END SAVE_AH_REVBESKRIVELSE;';
		End if;	
		
		jobbnummer := 2;	
    PrimusOppdJobbNrIVersjon( 53, jobbnummer );
		if jobnummer_in <= jobbnummer then	
		  DECLARE		  
			iId Integer;		  
		  BEGIN  
			 
			  For TH in (select DISTINCT mp.objid
                                 , mp.hid
                                 , mh.dato
                                 , mh.til_dato
                                 , mh.newhovedtype
                                 , mh.newundertype
                                 , mh.beskrivende_objekt
                                 , obr.oppl AS tekst
                              FROM superobjekt_hendelse mp
                                 , mushendelse mh
                                 , objekt_revisjon obr 
                             WHERE mp.hid = mh.hid
                               AND mp.objid = obr.objid
                               AND mh.dato = obr.revidert_dato
                               AND mh.hovedtypekode = 10004
                               AND mh.merke is NULL)
			  Loop	
	            
          iId := 0;    
				
          if th.beskrivende_objekt is null then
            execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;				
          else
            iId := th.beskrivende_objekt;
          end if;
        
          execute immediate 'Insert INTO ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE) 
								   values (:a, :b, :c)' 
								   using iId, TH.HID, TH.NEWHOVEDTYPE;
								   
          execute immediate 'BEGIN SAVE_AHUT_DATERING(:a, :b, :C, :d); END;' using iId, TH.hid, th.dato,  th.dato;
          execute immediate 'Insert Into OBJEKT_ADM_HENDELSE(ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR, FRADATO, TILDATO) 
								   values (:a, :b, :c, :d, :e, :f, :g)' using iId, TH.OBJID, TH.OBJID, TH.Hid, 1,  TH.DATO, TH.TIL_DATO;
                				
          if TH.tekst is not null then
            execute immediate 'BEGIN SAVE_AH_REVBESKRIVELSE(:a, :b, :c); END;' using iId, TH.hid, Th.tekst;
          End if;						
				
          execute immediate 'update MUSHENDELSE set MERKE=''X'' Where HID = :b ' using TH.HID;									
        END LOOP;		 
      commit;
		  END;	
    			
		End if;	
				
		
	
    jobbnummer:= 100;		
    return 'OK';
	exception
		when others then
			ROLLBACK;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus53 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(53, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);			
			return 'FEIL!';
	end;

end;
/
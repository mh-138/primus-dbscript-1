CREATE OR REPLACE FUNCTION primus76(jobbnummer OUT NUMBER, jobnummer_in NUMBER)
RETURN VARCHAR2
AUTHID CURRENT_USER
IS
BEGIN
  dbms_output.enable;
  BEGIN
  
    jobbnummer := 1;
    IF jobnummer_in <= jobbnummer  THEN
	primusOppdJobbNrIVersjon( 76, jobbnummer );
      EXECUTE IMMEDIATE
      'ALTER TABLE SIGNATUR_GRUPPE ADD
      ( OPT_MOTTAK VARCHAR2(2 CHAR)
      , OPT_UTLEVERING VARCHAR(2 CHAR))';
      COMMIT;
      EXECUTE IMMEDIATE 'UPDATE SIGNATUR_GRUPPE SET OPT_MOTTAK = ''-'', OPT_UTLEVERING = ''-''';
      COMMIT;
    END IF;
    jobbnummer := 2;
    IF jobnummer_in <= jobbnummer
    THEN
	primusOppdJobbNrIVersjon( 76, jobbnummer );
      EXECUTE IMMEDIATE
      'ALTER TABLE DOKUMENT ADD
      ( TITTEL VARCHAR2(100 CHAR)
      , PRODUSERT_DATO DATE
      , PRODUSENTID NUMBER(8,0)
      , HID_OPPRETTET NUMBER(8,0)
      , HID_SLETTET NUMBER(8,0)
      , CONSTRAINT FK_DOKUMENT_PRODUSENTID FOREIGN KEY (PRODUSENTID)
          REFERENCES JURPERSON (OBJID)
      , CONSTRAINT FK_DOKUMENT_HID_OPPRETTET FOREIGN KEY (HID_OPPRETTET)
          REFERENCES HENDELSE (HID) ENABLE
      , CONSTRAINT FK_DOKUMENT_HID_SLETTET FOREIGN KEY (HID_SLETTET)
          REFERENCES HENDELSE (HID) ENABLE)';
      COMMIT;
    END IF;
    jobbnummer := 3;
    IF jobnummer_in <= jobbnummer
    THEN
	primusOppdJobbNrIVersjon( 76, jobbnummer );
      EXECUTE IMMEDIATE 'ALTER TABLE DOKUMENT MODIFY (BESKRIVELSE VARCHAR2(1000 CHAR))';
      COMMIT;
    END IF;
	
  jobbnummer := 4;
    IF jobnummer_in <= jobbnummer
    THEN
	primusOppdJobbNrIVersjon( 76, jobbnummer );	
	EXECUTE IMMEDIATE '
		CREATE TABLE BYGNING_ROM_TMP
	( OBJID NUMBER(8,0) NOT NULL ENABLE
	, LOPENR NUMBER(8,0) NOT NULL ENABLE
	, ROMNR VARCHAR2(10 CHAR)
	, ROMNAVN VARCHAR2(100 CHAR)
	, ETASJE NUMBER(3,0)
	, KOMMENTAR VARCHAR2(500 CHAR))';
    end if;	
 jobbnummer := 5;
    IF jobnummer_in <= jobbnummer
    THEN
	primusOppdJobbNrIVersjon( 76, jobbnummer );	
	EXECUTE IMMEDIATE '
		INSERT INTO BYGNING_ROM_TMP(OBJID, LOPENR, ROMNR, ROMNAVN, ETASJE, KOMMENTAR)
		SELECT OBJID, LOPENR, ROMNR, ROMNAVN, ETASJE, KOMMENTAR FROM BYGNING_ROM';
	End if; 	
jobbnummer := 6;
    IF jobnummer_in <= jobbnummer
    THEN
	primusOppdJobbNrIVersjon( 76, jobbnummer );	
	EXECUTE IMMEDIATE ' DROP TABLE BYGNING_ROM';
	End if;

	
 jobbnummer := 7;
    IF jobnummer_in <= jobbnummer  THEN
		primusOppdJobbNrIVersjon( 76, jobbnummer );	
		EXECUTE IMMEDIATE '	
		CREATE TABLE BYGNING_ROM
		( OBJID NUMBER(8,0) NOT NULL ENABLE
		, HID NUMBER(8,0) NOT NULL ENABLE
		, NR NUMBER(8,0) NOT NULL ENABLE
		, OBJID_SISTE NUMBER(8,0)
		, ROMNR VARCHAR2(10 CHAR)
		, ROMNAVN VARCHAR2(100 CHAR)
		, ETASJE NUMBER(3,0)
		, KOMMENTAR VARCHAR2(500 CHAR)
		, REKKEFOLGE NUMBER(3,0)
		, CONSTRAINT PK_BYGNING_ROM PRIMARY KEY (OBJID, HID, NR) 
		, CONSTRAINT FK_BYGNING_ROM_OBJID FOREIGN KEY (OBJID) REFERENCES SUPEROBJEKT (OBJID) ENABLE 
		, CONSTRAINT FK_BYGNING_ROM_OBJID_SISTE FOREIGN KEY (OBJID_SISTE) REFERENCES SUPEROBJEKT (OBJID) ENABLE 
		, CONSTRAINT FK_BYGNING_ROM_HID FOREIGN KEY (HID)  REFERENCES HENDELSE (HID) ENABLE)';
    End if;
jobbnummer := 8;
    IF jobnummer_in <= jobbnummer  THEN
		primusOppdJobbNrIVersjon( 76, jobbnummer );	
		EXECUTE IMMEDIATE '	
		DECLARE
		   NHID NUMBER := 0;
		BEGIN
		   SELECT HIDSEQ.NEXTVAL
		   INTO NHID
		   FROM DUAL;
		   INSERT INTO HENDELSE(HID, DATO, SIGNID) VALUES (NHID, SYSDATE, 1);
		   COMMIT;
		   FOR REC IN
			 (SELECT OBJID, LOPENR, ROMNR, ROMNAVN, ETASJE, KOMMENTAR
			  FROM BYGNING_ROM_TMP
			  ORDER BY OBJID, LOPENR)
		   LOOP
			 INSERT INTO BYGNING_ROM(OBJID, NR, HID, OBJID_SISTE, ROMNR, ROMNAVN, ETASJE, KOMMENTAR) VALUES(REC.OBJID, REC.LOPENR, NHID, REC.OBJID, REC.ROMNR, REC.ROMNAVN, REC.ETASJE, REC.KOMMENTAR);
		   END LOOP;
		END;';
	End if;	
jobbnummer := 9;
    IF jobnummer_in <= jobbnummer  THEN
		primusOppdJobbNrIVersjon( 76, jobbnummer );	
		EXECUTE IMMEDIATE 'DROP TABLE BYGNING_ROM_TMP';
	End if;
jobbnummer := 10;
    IF jobnummer_in <= jobbnummer  THEN
		primusOppdJobbNrIVersjon( 76, jobbnummer );
    EXECUTE IMMEDIATE 'ALTER TABLE ADM_HENDELSE drop CONSTRAINT FK_ADM_HENDELSE_UNDERTYPE';
    COMMIT;
  End if;
jobbnummer := 11;
  IF jobnummer_in <= jobbnummer  THEN	primusOppdJobbNrIVersjon( 76, jobbnummer );	
   EXECUTE IMMEDIATE 'ALTER TABLE ADM_HENDELSE_UNDERTYPE_L drop CONSTRAINT UN_ADM_HENDELSE_UT_L_TKT_PID';     
   COMMIT;
  End if;
jobbnummer := 12;
  IF jobnummer_in <= jobbnummer  THEN	primusOppdJobbNrIVersjon( 76, jobbnummer );	
    begin
      EXECUTE IMMEDIATE 'drop index PRIMUS.UN_ADM_HENDELSE_UT_L_TKT_PID';	     
	  COMMIT;
	except 
    end;		
  End if;
  
jobbnummer := 13;
  IF jobnummer_in <= jobbnummer  THEN	primusOppdJobbNrIVersjon( 76, jobbnummer );	    
	EXECUTE IMMEDIATE 'ALTER TABLE ADM_HENDELSE_UNDERTYPE disable CONSTRAINT FK_ADM_HENDELSE_UT_TYPEID';
	EXECUTE IMMEDIATE 'ALTER TABLE ADM_HENDELSE_UNDERTYPE_L disable CONSTRAINT PK_ADM_HENDELSE_UNDERTYPE_L';
    EXECUTE IMMEDIATE 'update ADM_HENDELSE_UNDERTYPE_L set parent_id=104 Where TEKST=''Filvedlegg'' ';	
    COMMIT;
  End if;  
jobbnummer := 14;
    IF jobnummer_in <= jobbnummer  THEN
		primusOppdJobbNrIVersjon( 76, jobbnummer );
		IF svenskEllerNorskDatabase = 'NOR'
      THEN
		EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (101,''Fast'',100)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (102,''Midlertidig'',100)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (103,''Utlån'',101)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (104,''Innlån'',101)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (105,''Depositum'',101)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (106,''Deponert ut'',101)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (107,''Deponert inn'',101)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (108,''Intern'',102)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (109,''Ekstern'',102)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (110,''Media'',104)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (111,''Filvedlegg'',104)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (112,''Objekt'',104)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (113,''Midlertidig'',104)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (114,''Levert av fraktleverandør'',200)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (115,''Levert av eier/innskyter'',200)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (116,''Hentet av medarbeider'',200)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (117,''Hentes av fraktleverandør'',201)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (118,''Hentes av mottaker'',201)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (119,''Kastes'',201)';
		  ELSE
		    EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (101,''Fast'',100)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (102,''Tillfällig'',100)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (103,''Utlån'',101)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (104,''Inlån'',101)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (105,''Depositum'',101)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (106,''Deposition ut'',101)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (107,''Deposition in'',101)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (108,''Intern'',102)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (109,''Extern'',102)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (110,''Media'',104)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (111,''Filvedlegg'',104)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (112,''Objekt'',104)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (113,''Tillfällig'',104)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (114,''Levererat av transportör'',200)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (115,''Levererat av giver'',200)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (116,''Hämtat av medarbetare'',200)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (117,''Hämtas av transportör'',201)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (118,''Hämtas av mottagare'',201)';
        EXECUTE IMMEDIATE 'Insert into ADM_HENDELSE_UNDERTYPE_L (TYPEID,TEKST,PARENT_ID) values (119,''Kassera'',201)';
		  END IF;
      COMMIT;
	End if;
jobbnummer := 15;
    IF jobnummer_in <= jobbnummer  THEN
		primusOppdJobbNrIVersjon( 76, jobbnummer );
		IF svenskEllerNorskDatabase = 'NOR'
      THEN
        EXECUTE IMMEDIATE '
        DECLARE
           OLD_TYPE_ID NUMBER := 0;
        BEGIN
           BEGIN
            SELECT TYPEID INTO OLD_TYPE_ID
            FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Fast'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 101 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
            SELECT TYPEID INTO OLD_TYPE_ID
            FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Midlertidig'' AND TYPEID < 100 AND PARENT_ID = 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 102 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Utlån'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 103 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Innlån'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 104 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Depositum'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 105 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Deponert ut'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 106 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Deponert inn'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 107 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Intern'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 108 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''intern'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 108 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Ekstern'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 109 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''ekstern'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 109 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Media'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 110 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Filvedlegg'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 111 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Objekt'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 112 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Midlertidig'' AND TYPEID < 100 AND PARENT_ID = 104;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 113 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Levert av fraktleverandør'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 114 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Levert av eier/innskyter'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 115 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Hentet av medarbeider'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 116 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Hentes av fraktleverandør'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 117 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Hentes av mottaker'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 118 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Kastes'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 119 WHERE TYPEID = OLD_TYPE_ID;
        END;';
      ELSE
        EXECUTE IMMEDIATE '
        DECLARE
           OLD_TYPE_ID NUMBER := 0;
        BEGIN
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Fast'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 101 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Tillfällig'' AND TYPEID < 100 AND PARENT_ID = 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 102 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Utlån'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 103 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Inlån'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 104 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Innlån'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 104 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Depositum'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 105 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Deposition ut'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 106 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Deposition'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 106 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Deposition in'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 107 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Intern'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 108 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Extern'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 109 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Media'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 110 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Filvedlegg'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 111 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Objekt'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 112 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Tillfällig'' AND TYPEID < 100 AND PARENT_ID = 104;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 113 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Levererat av transportör'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 114 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Levererat av giver'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 115 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Hämtat av av medarbetare'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 116 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Hämtas av transportör'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 117 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Hämtas av mottagare'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 118 WHERE TYPEID = OLD_TYPE_ID;
           BEGIN
             SELECT TYPEID INTO OLD_TYPE_ID
             FROM ADM_HENDELSE_UNDERTYPE_L WHERE TEKST = ''Kassera'' AND TYPEID < 100;
           EXCEPTION WHEN no_data_found THEN OLD_TYPE_ID := 0;
           END;
           UPDATE ADM_HENDELSE_UNDERTYPE SET TYPEID = 119 WHERE TYPEID = OLD_TYPE_ID;
        END;';
      END IF;
      COMMIT;
	End if;
jobbnummer := 16;
    IF jobnummer_in <= jobbnummer  THEN
		primusOppdJobbNrIVersjon( 76, jobbnummer );
    EXECUTE IMMEDIATE 'DELETE FROM ADM_HENDELSE_UNDERTYPE_L WHERE TYPEID < 100';
    COMMIT;
  End if;
 
jobbnummer := 17;
    IF jobnummer_in <= jobbnummer  THEN
		primusOppdJobbNrIVersjon( 76, jobbnummer );
    EXECUTE IMMEDIATE 'alter table ADM_HENDELSE drop column UNDERTYPE';
    COMMIT;
  End if;
jobbnummer := 18;
    IF jobnummer_in <= jobbnummer  THEN
		primusOppdJobbNrIVersjon( 76, jobbnummer );
    EXECUTE IMMEDIATE 'ALTER TABLE ADM_HENDELSE_UNDERTYPE_L add CONSTRAINT UN_ADM_HENDELSE_UT_L_TKT_PID unique(''TEKST'', ''PARENT_ID'')';	 	
    COMMIT;
  End if; 
  jobbnummer := 19;
    IF jobnummer_in <= jobbnummer  THEN
		primusOppdJobbNrIVersjon( 76, jobbnummer );    
	EXECUTE IMMEDIATE 'ALTER TABLE ADM_HENDELSE_UNDERTYPE_L enable CONSTRAINT PK_ADM_HENDELSE_UNDERTYPE_L';		
    COMMIT;
  End if; 
  jobbnummer := 20;
    IF jobnummer_in <= jobbnummer  THEN
		primusOppdJobbNrIVersjon( 76, jobbnummer );    
	EXECUTE IMMEDIATE 'ALTER TABLE ADM_HENDELSE_UNDERTYPE enable CONSTRAINT FK_ADM_HENDELSE_UT_TYPEID';
    COMMIT;
  End if; 
  
  
  jobbnummer := 21;
  if jobnummer_in <= jobbnummer then
		PrimusOppdJobbNrIVersjon( 76, jobbnummer );
    execute immediate ('alter table adm_hendelse disable constraint UK_ADM_HENDELSE_UUID');

    execute immediate ('update /*+ PARALLEL(adm_hendelse) */ adm_hendelse
                           set uuid = getOracleUUID()
                         where uuid is null');

    execute immediate ('alter table adm_hendelse enable constraint UK_ADM_HENDELSE_UUID');
  end if;
  
  jobbnummer := 22;
  if jobnummer_in <= jobbnummer then
		PrimusOppdJobbNrIVersjon( 76, jobbnummer );
    update sted_hierarki 
       set navn = 'Skiptvet'
     where kode = '0217'
       and navn = 'Skiptvedt';

  end if;
  
  jobbnummer := 23;
  if jobnummer_in <= jobbnummer then
		PrimusOppdJobbNrIVersjon( 76, jobbnummer );
    execute immediate 'grant insert
                           , update 
                          on primus.bygning_rom 
                          to pri_reg';
  end if;
    
  jobbnummer := 24;
  if jobnummer_in <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 76, jobbnummer );
    DECLARE
      nhid NUMBER;
      formaaltypeid NUMBER;
    BEGIN
      SELECT hidseq.nextval
      INTO nhid
      FROM dual;
      EXECUTE IMMEDIATE 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' USING nhid;
      IF svenskEllerNorskDatabase = 'NOR'
      THEN
        SELECT nvl(MAX(FORMAALTYPEID), 0) + 1 INTO formaaltypeid FROM ADM_H_FORMAALTYPE_L;
        EXECUTE IMMEDIATE
        'INSERT INTO ADM_H_FORMAALTYPE_L (FORMAALTYPEID, FORMAALTEKST, MERKE, UUID, HID_OPPRETTET)
         VALUES (:f, ''Utlån'', ''Y'', sys_guid(), :a)' USING formaaltypeid, nhid;
        EXECUTE IMMEDIATE 'INSERT INTO ADM_H_FORMAALTYPE_L_FILTER(FILTERID, FORMAALTYPEID, ADMHENDELSETYPEID)
                          VALUES (:f1, :f2, 106)' using formaaltypeid, formaaltypeid;
        SELECT nvl(MAX(FORMAALTYPEID), 0) + 1 INTO formaaltypeid FROM ADM_H_FORMAALTYPE_L;
        EXECUTE IMMEDIATE
        'INSERT INTO ADM_H_FORMAALTYPE_L (FORMAALTYPEID, FORMAALTEKST, MERKE, UUID, HID_OPPRETTET)
         VALUES (:f, ''Utstilling'', ''Y'', sys_guid(), :a)' USING formaaltypeid, nhid;
        EXECUTE IMMEDIATE 'INSERT INTO ADM_H_FORMAALTYPE_L_FILTER(FILTERID, FORMAALTYPEID, ADMHENDELSETYPEID)
                          VALUES (:f1, :f2, 106)' using formaaltypeid, formaaltypeid;
      ELSE
        SELECT nvl(MAX(FORMAALTYPEID), 0) + 1 INTO formaaltypeid FROM ADM_H_FORMAALTYPE_L;
        EXECUTE IMMEDIATE
        'INSERT INTO ADM_H_FORMAALTYPE_L (FORMAALTYPEID, FORMAALTEKST, MERKE, UUID, HID_OPPRETTET)
         VALUES (:f, ''Utlån'', ''Y'', sys_guid(), :a)' USING formaaltypeid, nhid;
        EXECUTE IMMEDIATE 'INSERT INTO ADM_H_FORMAALTYPE_L_FILTER(FILTERID, FORMAALTYPEID, ADMHENDELSETYPEID)
                          VALUES (:f1, :f2, 106)' using formaaltypeid, formaaltypeid;
        SELECT nvl(MAX(FORMAALTYPEID), 0) + 1 INTO formaaltypeid FROM ADM_H_FORMAALTYPE_L;
        EXECUTE IMMEDIATE
        'INSERT INTO ADM_H_FORMAALTYPE_L (FORMAALTYPEID, FORMAALTEKST, MERKE, UUID, HID_OPPRETTET)
         VALUES (:f, ''Utställning'', ''Y'', sys_guid(), :a)' USING formaaltypeid, nhid;
        EXECUTE IMMEDIATE 'INSERT INTO ADM_H_FORMAALTYPE_L_FILTER(FILTERID, FORMAALTYPEID, ADMHENDELSETYPEID)
                          VALUES (:f1, :f2, 106)' using formaaltypeid, formaaltypeid;
      COMMIT;
      END IF;
    END;
    end if;
    
    RETURN 'OK';
    EXCEPTION
    WHEN OTHERS THEN
    ROLLBACK;
    SYS.DBMS_OUTPUT.PUT_LINE('Primus76 feilet! Jobbnummer: ' || jobbnummer);
    PRIMUSOPPGRADERINGSTOPP(76, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
    RETURN 'FEIL!';
  END;

END;

/

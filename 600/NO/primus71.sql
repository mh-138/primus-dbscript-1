CREATE OR REPLACE FUNCTION primus71(jobbnummer OUT NUMBER, jobnummer_in NUMBER)
  RETURN VARCHAR2
AUTHID CURRENT_USER
IS
  BEGIN
    dbms_output.enable;

    BEGIN
      jobbnummer := 1;
      IF jobnummer_in <= jobbnummer
      THEN
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET ADMFILTERFELTID = 11 WHERE ADMFILTERFELTID IS NULL';
        COMMIT;
      END IF;
      RETURN 'OK';
      EXCEPTION
      WHEN OTHERS THEN
      ROLLBACK;
      SYS.DBMS_OUTPUT.PUT_LINE('Primus71 feilet! Jobbnummer: ' || jobbnummer);
      PRIMUSOPPGRADERINGSTOPP(71, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
      RETURN 'FEIL!';
    END;

  END;

/

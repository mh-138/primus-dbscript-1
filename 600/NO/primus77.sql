create or replace function primus77(jobbnummer out number, jobbnummer_inn number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin  
  dbms_output.enable;
  begin
 
	
  jobbnummer := 6;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );		
    alter_constraint('ADM_HENDELSE_PERSON', 'FKEY_HID_AHP'); 
  End if;	

 jobbnummer := 7;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );		
    alter_constraint('MOTIVKLASS_H', 'KLASS_KODE_MOTIVKLASS');  
  End if;


  jobbnummer := 8;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );		       
   alter_constraint('JURPERSONTYPE_L', 'FK_JURPERSTYPE_PARENTID');
   End if;
   
 jobbnummer := 9;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );		   
	 alter_constraint('OBJEKT_KLASS', 'KLASS_KODE_OBJ_KLASS');  		 
  End if;

jobbnummer := 10;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );		  
	 alter_constraint('KLASSKODE', 'KLASS_KODE_KLASS_KODE');
  End if;

jobbnummer := 11;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );		  
	 alter_constraint('AKS_KLAUSUL', 'AKSKLAUSUL_C2');
  end if;
  
  jobbnummer := 12;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );		  
	 alter_constraint('OBJEKT', 'KLAUSUL_OBJEKT');
  End if;

jobbnummer := 13;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );		  
	 alter_constraint('KLAUSUL_H', 'OBJEKT_KLAUSUL');
   End if;

jobbnummer := 14;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
	 alter_constraint('UTSTILLINGSTYPE_H', 'FK_TYPEID_UTH');
  End if;

jobbnummer := 15;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );		
	 alter_constraint('UTSTILLINGSTYPE_L', 'PK_TYPEID_UTL');
 end if;

jobbnummer := 16;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );		 
	 alter_constraint('UTSTILLING', 'FK_HID_UTSTILLINGSTYPE_UT');
  end if;

jobbnummer := 17;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );		  
	 alter_constraint('UTSTILLING', 'FK_UT_PUBLISERINGSNIVAA');
  end if;	 

jobbnummer :=18;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
   drop_sequence('DEKORTEKNIKK_LIDSEQ');
end if;
jobbnummer :=19;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
   drop_sequence('TMP_GRUPPERINGSTYPEIDSEQ');
end if;
jobbnummer :=20;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
    drop_sequence('OVERSETTELSER_HISTORIKK_SEQ');
end if;
jobbnummer :=21;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
    alter_column('OBJEKT', 'KLAUSULID');
end if;
jobbnummer :=22;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('MOTIVKLASS_H', 'KL_KODE');
end if;
jobbnummer :=23;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('MOTIVKLASS_H', 'KL_SYSTEM');
end if;
jobbnummer :=24;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('OBJEKT_KLASS',  'KL_KODE');
end if;
jobbnummer :=25;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('OBJEKT_KLASS',  'KL_SYSTEM');    
end if;
jobbnummer :=26;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('SUPEROBJEKT_HENDELSE',  'MERKE');
end if;
jobbnummer :=27;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('OBJ_OPPLAGSOPPLYSNINGER_H',  'TABLE_KEY');
end if;
jobbnummer :=28;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('OBJ_PRODUKSJONSDATERING_H',  'TABLE_KEY');
end if;
jobbnummer :=29;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('OBJ_PRODUKSJONSHISTORIKK_H',  'TABLE_KEY');
end if;
jobbnummer :=30;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('OBJ_PROVENIENSDATERING_H',  'TABLE_KEY');
end if;
jobbnummer :=31;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('OBJ_PROVENIENSHISTORIKK_H',  'TABLE_KEY');
end if;
jobbnummer :=32;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('OBJ_STILTEKST_H',  'TABLE_KEY');
end if;
jobbnummer :=33;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('GJENSTAND',  'HID_FORMSTIL');
end if;
jobbnummer :=34;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'MERKE');
end if;
jobbnummer :=35;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'SAKSNUMMER');
end if;
jobbnummer :=36;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'FRADATO');
end if;
jobbnummer :=37;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'TILDATO');
end if;
jobbnummer :=38;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'AVSLUTTET');
end if;
jobbnummer :=39;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'LAANER_NAVN');
end if;
jobbnummer :=40;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'LAANER_INST');
end if;
jobbnummer :=41;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'UTLAANER');
end if;
jobbnummer :=42;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'ANTALL_OBJ');
end if;
jobbnummer :=43;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'FORMAAL');
end if;
jobbnummer :=44;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'ANDREOPPL');
end if;
jobbnummer :=45;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'UTLTYPE');
end if;
jobbnummer :=46;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'UTLAANSNR');
end if;
jobbnummer :=47;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'UTLAANSINST');
end if;
jobbnummer :=48;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTLAAN',  'SAKSNUMMER_UID');
end if;
-- Denne er i bruk
-- jobbnummer :=49;
--   if jobbnummer_inn <= jobbnummer then
--     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
--  alter_column('UTLAAN',  'DATO_GODKJENT');
-- end if;
jobbnummer :=50;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('TILSTAND_H',  'MERKE');
end if;
jobbnummer :=51;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('PLASSKODE',  'MERKE');
end if;
jobbnummer :=52;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('MIDLPLASS',  'MERKE');
end if;
jobbnummer :=53;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'MERKE');
end if;
jobbnummer :=54;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'UTSTILLINGSID');
end if;
jobbnummer :=55;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'SAKSNR');
end if;
jobbnummer :=56;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'FRADATO');
end if;
jobbnummer :=57;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'TILDATO');
end if;
jobbnummer :=58;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'STATUS');
end if;
jobbnummer :=59;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'HID_UTSTILLINGSTYPE');
end if;
jobbnummer :=60;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'PUBLISERES');
end if;
jobbnummer :=61;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'DIMUKODE');
end if;
jobbnummer :=62;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'ADMH_OBJID');
end if;
jobbnummer :=63;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'NR');
end if;
jobbnummer :=64;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'HID_PUBLISERBARTEKST');
end if;
jobbnummer :=65;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'HID_PUBLISERBARINGRESS');
end if;
jobbnummer :=66;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('UTSTILLING',  'HID_PUBLISERINGSNIVAA');
end if;
jobbnummer :=67;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 alter_column('BETEGNELSE_L',  'GRUPPERINGSTYPEKODE');
end if;
jobbnummer :=68;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
  drop_table('DEKORTEKNIKK_H');
end if;
jobbnummer :=69;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
  drop_table('DEKORTEKNIKK_L');
end if;
jobbnummer :=70;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('KLASSBESKRIVELSE');
end if;
jobbnummer :=71;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('KLASSKODE');
end if;
jobbnummer :=72;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('MUSHENDELSE');
end if;
jobbnummer :=73;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('PERSON_FELTER_TABLE');
end if;
jobbnummer :=74;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('OBJEKT_UTLAAN');
end if;
jobbnummer :=75;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('OBJEKT_UTSTILLING');
end if;
jobbnummer :=76;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('OBJEKT_FELTER_TABLE');
end if;
jobbnummer :=77;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('SUPEROBJEKT_HENDELSE_FELTER');
end if;
jobbnummer :=78;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('FELT_TYPER');
end if;
jobbnummer :=79;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('TEKNIKK_H');
end if;
jobbnummer :=80;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('UTSTILLINGSTYPE_L');
end if;

jobbnummer :=81;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('UTSTILLINGSTYPE_H');
end if;

jobbnummer :=82;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('TEMP_GRUPPERINGSTYPE_L');
end if;

jobbnummer :=83;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('OVERSETTELSER_HISTORIKK');
end if;

jobbnummer :=84;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('OVERSETTELSER');
end if;

jobbnummer :=85;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('WEB_ROLLEJP');
end if;

jobbnummer :=86;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
   drop_table('WEB_ROLLESTED');
end if;

jobbnummer :=87;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
 drop_table('WEB_ROLLESTED_FELTER');
end if;
 

jobbnummer :=88;
  if jobbnummer_inn <= jobbnummer then
     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
      EXECUTE IMMEDIATE ('update PRIMUS.BEHANDLINGSTYPE_L set PARENT_ID=-1 WHERE PARENT_ID is NULL');
  end if;	  
  
 jobbnummer :=89; 
  if jobbnummer_inn <= jobbnummer then
     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
      EXECUTE IMMEDIATE ('update PRIMUS.SKADE_L set PARENT_ID=-1 WHERE PARENT_ID is NULL');
  end if;
  
  jobbnummer :=90;
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
    EXECUTE IMMEDIATE ('update PRIMUS.ADM_HENDELSE set PARENT_ID=-1 WHERE PARENT_ID is NULL');
  end if;
  
  jobbnummer :=91;
  if jobbnummer_inn <= jobbnummer then
     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
      EXECUTE IMMEDIATE ('update PRIMUS.ANALYSETYPE_L set PARENT_ID=-1 WHERE PARENT_ID is NULL');
   End if;	  
   
  jobbnummer :=92;	  
  if jobbnummer_inn <= jobbnummer then
     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
      EXECUTE IMMEDIATE ('update PRIMUS.BYGNINGSDELSTYPE_L set PARENT_ID=-1 WHERE PARENT_ID is NULL');
  End if;

  jobbnummer :=93;	  
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
    EXECUTE IMMEDIATE ('update PRIMUS.EMNEORD set PARENT_ID=-1 WHERE PARENT_ID is NULL');
  End if;
  
  jobbnummer :=94;	  
  if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
    EXECUTE IMMEDIATE ('update PRIMUS.FARE_L set PARENT_ID=-1 WHERE PARENT_ID is NULL');
  End if;
   
   jobbnummer :=95;	  
 if jobbnummer_inn <= jobbnummer then
    PrimusOppdJobbNrIVersjon( 77, jobbnummer );
   EXECUTE IMMEDIATE ('update PRIMUS.JURPERSONTYPE_L set PARENT_ID=-1 WHERE PARENT_ID is NULL');
 End if;
 
 jobbnummer :=96;	  
 if jobbnummer_inn <= jobbnummer then
     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
      EXECUTE IMMEDIATE ('update PRIMUS.MOTIVORD_L set PARENT_ID=-1 WHERE PARENT_ID is NULL');
 End if;	  
 jobbnummer :=97;	  
 if jobbnummer_inn <= jobbnummer then
     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
      EXECUTE IMMEDIATE ('alter table  PRIMUS.BEHANDLINGSTYPE_L  modify( PARENT_ID default -1)');
  End if;
  
jobbnummer :=98;	  
 if jobbnummer_inn <= jobbnummer then
     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
      EXECUTE IMMEDIATE ('alter table  PRIMUS.SKADE_L  modify( PARENT_ID default -1)');
 END if;

jobbnummer :=99;	  
 if jobbnummer_inn <= jobbnummer then
     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
      EXECUTE IMMEDIATE ('alter table  PRIMUS.ANALYSETYPE_L  modify( PARENT_ID default -1)');
 End if;
 
jobbnummer :=100;	  
 if jobbnummer_inn <= jobbnummer then
     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
      EXECUTE IMMEDIATE ('alter table  PRIMUS.BYGNINGSDELSTYPE_L  modify( PARENT_ID default -1)');
 End if;	  
 
jobbnummer :=101;	  
 if jobbnummer_inn <= jobbnummer then
     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
      EXECUTE IMMEDIATE ('alter table  PRIMUS.EMNEORD  modify( PARENT_ID default -1)');
 End if;
 
jobbnummer :=102;	  
 if jobbnummer_inn <= jobbnummer then
     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
      EXECUTE IMMEDIATE ('alter table  PRIMUS.FARE_L  modify( PARENT_ID default -1)');
 End if;

jobbnummer :=103;	  
 if jobbnummer_inn <= jobbnummer then
     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
      EXECUTE IMMEDIATE ('alter table  PRIMUS.JURPERSONTYPE_L  modify( PARENT_ID default -1)');
 End if;	  

jobbnummer :=104;	  
 if jobbnummer_inn <= jobbnummer then
     PrimusOppdJobbNrIVersjon( 77, jobbnummer );
      EXECUTE IMMEDIATE ('alter table  PRIMUS.MOTIVORD_L  modify( PARENT_ID default -1)');
 End if;

 jobbnummer :=105;	  
	 if jobbnummer_inn <= jobbnummer then
		 PrimusOppdJobbNrIVersjon( 77, jobbnummer );
		  EXECUTE IMMEDIATE ('alter table  PRIMUS.ANALYSETYPE_L  modify( MERKE default ''Y'')');
	 End if;

 jobbnummer :=106;	  
	 if jobbnummer_inn <= jobbnummer then
		 PrimusOppdJobbNrIVersjon( 77, jobbnummer );
		  drop_sequence('DATERINGID_SEQUENCE');
	 End if;
jobbnummer :=107;	  
	 if jobbnummer_inn <= jobbnummer then
		 PrimusOppdJobbNrIVersjon( 77, jobbnummer );
		 drop_sequence('OBJEKT_FELTER_SEQ');     
	 End if;
jobbnummer :=108;	  
	 if jobbnummer_inn <= jobbnummer then
		 PrimusOppdJobbNrIVersjon( 77, jobbnummer );
		 EXECUTE IMMEDIATE 'alter table STIL_L modify (TYPEID NUMBER(6,0))';
	 End if;
jobbnummer :=109;	  
	 if jobbnummer_inn <= jobbnummer then
		 PrimusOppdJobbNrIVersjon( 77, jobbnummer );
		  EXECUTE IMMEDIATE ('alter table FORMSTIL_H modify (TYPEID NUMBER(6,0))');
	 End if;	 
	 
 commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus77 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(77, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/      
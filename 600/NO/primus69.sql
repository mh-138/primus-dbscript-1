create or replace function primus69(jobbnummer out number, jobbnummer_inn number) 
	return varchar2
	AUTHID CURRENT_USER
	is

  nnewHID number;
begin  
  dbms_output.enable;
  begin
    jobbnummer := 1;

    select hidseq.nextval into nnewHID from dual;
    insert into hendelse (hid, signid, dato) values (nnewHID, 1, sysdate);

    jobbnummer := 2;
    
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 69, jobbnummer );
      execute immediate 'create table rollehisthend_l ( ROLLEID NUMBER( 8 )
                             , ROLLEKODE VARCHAR2( 5 CHAR )
                             , BESKRIVELSE VARCHAR2( 200 CHAR )
                             , HID_OPPRETTET NUMBER( 8 )
                             , HID_SLETTET NUMBER( 8 )
                             , MERKE VARCHAR2( 1 CHAR ) default (''Y'') 
                             , CHECKFORDELETE	VARCHAR2( 5 CHAR )
                             , UUID VARCHAR2( 40 CHAR )
                             , AUTORITET VARCHAR2( 10 CHAR )
                             , AUTORITET_STATUS VARCHAR2( 1 CHAR )
                             , AUTORITET_DATASET VARCHAR2( 100 CHAR )
                             , AUTORITET_KILDE VARCHAR2( 100 CHAR )
                             , constraint pk_rollehisthend_l PRIMARY KEY ( rolleid )
                             , constraint CK_rollehisthend_rollekode check ( rollekode is not null ) enable
                             , constraint ck_rollehisthend_beskrivelse check ( beskrivelse is not null ) enable
                             , CONSTRAINT UK_ROLLEhisthend_rollekode UNIQUE ( ROLLEKODE ) 
                             , CONSTRAINT UK_ROLLehisthend_UUID UNIQUE ( UUID )
                             , CONSTRAINT FK_ROLLEHISTHEND_HIDOPPR FOREIGN KEY ( HID_OPPRETTET ) REFERENCES PRIMUS.HENDELSE (HID) ENABLE
                             , CONSTRAINT FK_ROLLEHISTHEND_HIDSLET FOREIGN KEY ( HID_SLETTET ) REFERENCES PRIMUS.HENDELSE ( HID ) ENABLE
                             ) TABLESPACE PRIMUSDT ';
    end if;

    jobbnummer := 3;

    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 69, jobbnummer );
      execute immediate 'create table rollestatushisthend_l (	STATUSID NUMBER( 4 ), 
                                      STATUS VARCHAR2( 50 CHAR ), 
                                      MERKE VARCHAR2( 1 CHAR ) default (''Y''), 
                                      HID_OPPRETTET NUMBER( 8 ), 
                                      HID_SLETTET NUMBER( 8 ), 
                                      CHECKFORDELETE VARCHAR2( 1 CHAR ), 
                                      UUID VARCHAR2( 40 CHAR ), 
                                      AUTORITET VARCHAR2( 10 CHAR ), 
                                      AUTORITET_STATUS VARCHAR2( 1 CHAR ), 
                                      AUTORITET_DATASET VARCHAR2( 100 CHAR ), 
                                      AUTORITET_KILDE VARCHAR2( 100 CHAR ), 
                                       CONSTRAINT PK_rollesthsthnd_L_STATUSID PRIMARY KEY (STATUSID) ENABLE, 
                                       CONSTRAINT UK_rollesthsthnd_UUID UNIQUE (UUID) ENABLE, 
                                       CONSTRAINT FK_rollesthsthnd_HIDOPPR FOREIGN KEY (HID_OPPRETTET)
                                        REFERENCES PRIMUS.HENDELSE (HID) ENABLE, 
                                       CONSTRAINT FK_rollesthsthnd_HIDSLET FOREIGN KEY (HID_SLETTET)
                                        REFERENCES PRIMUS.HENDELSE (HID) ENABLE
                                       ) TABLESPACE PRIMUSDT' ;
    
    end if;
    
    jobbnummer := 4;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 69, jobbnummer );
      execute immediate 'create table objekt_navngitthendelse ( histid number( 8 )
                                     , hid number( 8 )
                                     , nr number( 7 )
                                     , histid_siste number( 8 )
                                     , rolleid number( 8 )
                                     , jpnr number( 8 )
                                     , rollestatusid number( 4 )
                                     , ordernr number( 3 )
                                     , constraint pk_objekt_navngitthendelse primary key ( histid, hid, nr )
                                     , constraint FK_OBJEKT_NAVNH_HISTORIKK foreign key ( histid ) references historikk ( histid )
                                     , constraint FK_OBJEKT_NAVNH_HISTORIKK_sis foreign key ( histid_siste ) references historikk ( histid )
                                     , constraint FK_OBJEKT_NAVNH_JURPERS foreign key ( jpnr ) references jurperson ( jpnr )
                                     , constraint FK_OBJEKT_NAVNH_HISTH_ROLLE foreign key ( rolleid ) references rollehisthend_l ( rolleid )
                                     , constraint FK_OBJEKT_NAVNH_HISTH_STATUS foreign key ( rollestatusid ) references rollestatushisthend_l ( statusid )
                                     , constraint FK_OBJEKT_NAVNH_HENDELSE foreign key ( hid ) references hendelse ( hid )
                                     , constraint CK_OBJEKT_NAVNH_HISTID check ( histid is not null ) enable
                                     , constraint CK_OBJEKT_NAVNH_HID check ( hid is not null ) enable
                                     , constraint CK_OBJEKT_NAVNH_NR check ( nr is not null ) enable
                                     ) tablespace primusdt' ;
    end if;
    
    jobbnummer := 5;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 69, jobbnummer );
      execute immediate 'grant insert, update on objekt_navngitthendelse to pri_reg';
      execute immediate 'grant insert, update on rollestatushisthend_l to pri_reg';
      execute immediate 'grant insert, update on rollehisthend_l to pri_reg';
    end if;

    jobbnummer := 6;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 69, jobbnummer );
      -- her må det legges inn riktige verdier for rollekoder.
      if svenskEllerNorskDatabase = 'NOR' then
        execute immediate 'insert into rollehisthend_l 
                                       ( rolleid
                                       , rollekode
                                       , beskrivelse
                                       , checkfordelete
                                       , uuid 
                                       , hid_opprettet
                                       ) values 
                                       ( 1
                                       , ''01''
                                       , ''brukt''
                                       , ''X''
                                       , ''64ac9d00-609a-46e8-8b62-a19f3425731a''
                                       , :newHID )' using nnewHID;
      else
        execute immediate 'insert into rollehisthend_l 
                                       ( rolleid
                                       , rollekode
                                       , beskrivelse
                                       , checkfordelete
                                       , uuid 
                                       , hid_opprettet
                                       ) values 
                                       ( 1
                                       , ''01''
                                       , ''använd''
                                       , ''X''
                                       , ''64ac9d00-609a-46e8-8b62-a19f3425731a''
                                       , :newHID )' using nnewHID;
      end if;
    end if;

    jobbnummer := 7;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 69, jobbnummer );
      -- her må det legges inn riktige verdier for status.
      if svenskEllerNorskDatabase = 'NOR' then
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (1, ''antatt'', ''X'', ''681f0137-23b3-4fc5-8db3-3896bcd71da0c'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (2, ''antatt sikker'', ''X'', ''ecec335d-c85b-4298-b255-58431319eb1ee'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (3, ''hypotetisk'', ''X'', ''0256f5f5-d997-4fc0-84f5-523fc1be11530'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (4, ''ifølge feilaktig tradisjon'', ''X'', ''e2072725-b57f-440c-af6b-b92c58a25b736'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (5, ''sannsynlig'', ''X'', ''e643953f-3955-4024-8d80-033a75c8e776c'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (6, ''sikker'', ''X'', ''34cf36b7-e50b-4d7f-ba43-32190169af429'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (7, ''tilskrevet'', ''X'', ''e888af4c-a7fa-4507-a1ca-a3d5e862a5b68'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (8, ''usikker'', ''X'', ''7e5bfe11-b8cb-424d-9719-9069a956348ae'', :newHID)' using nnewHID;
      else
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (1, ''förmodad'', ''X'', ''681f0137-23b3-4fc5-8db3-3896bcd71da0c'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (2, ''troligen säker'', ''X'', ''ecec335d-c85b-4298-b255-58431319eb1ee'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (3, ''hypotetisk'', ''X'', ''0256f5f5-d997-4fc0-84f5-523fc1be11530'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (4, ''enligt feilaktig tradition'', ''X'', ''e2072725-b57f-440c-af6b-b92c58a25b736'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (5, ''sannolik'', ''X'', ''e643953f-3955-4024-8d80-033a75c8e776c'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (6, ''säker'', ''X'', ''34cf36b7-e50b-4d7f-ba43-32190169af429'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (7, ''tillskriven'', ''X'', ''e888af4c-a7fa-4507-a1ca-a3d5e862a5b68'', :newHID)' using nnewHID;
        execute immediate 'Insert into rollestatushisthend_l ( STATUSID, STATUS, CHECKFORDELETE, UUID, HID_OPPRETTET ) values (8, ''osäker'', ''X'', ''7e5bfe11-b8cb-424d-9719-9069a956348ae'', :newHID)' using nnewHID;
      end if;
      
      
    end if;
    
    jobbnummer := 8;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 69, jobbnummer );
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table jurpersontype_l add ( parent_id number( 4 ) )' );
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table jurpersontype_l add ( m_path varchar2( 50 char ) )' );
      executeAlterTable( jobbnummer
                       , jobbnummer_inn
                       , 'alter table jurpersontype_l add ( constraint fk_jurperstype_parentid foreign key ( parent_id ) references jurpersontype_l ( typeid ) )' );
    end if;
    
    jobbnummer := 9;
    if jobbnummer_inn <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 69, jobbnummer );
      
      if svenskEllerNorskDatabase = 'NOR' then

        -- her må det legges inn riktige verdier for en ny navntype - hendelse.
        execute immediate 'insert into primus.jurpersontype_l 
               ( typeid, jurpersontype, merke, checkfordelete, uuid, hid_opprettet)
               values
               ( 200, ''Hendelse'', ''Y'', ''X'', ''a8c5c8fe-1d45-4ce5-899b-f6936fddf8cc'', :newhid )' using nnewHID;
      else
        execute immediate 'insert into primus.jurpersontype_l 
               ( typeid, jurpersontype, merke, checkfordelete, uuid, hid_opprettet)
               values
               ( 200, ''Händelse'', ''Y'', ''X'', ''a8c5c8fe-1d45-4ce5-899b-f6936fddf8cc'', :newhid )' using nnewHID;
      end if;
    end if;

    
		commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus69 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(69, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/      
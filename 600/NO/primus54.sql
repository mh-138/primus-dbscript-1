create or replace function primus54(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
		Data overføring fra MUSHENDELSE til ADM_HENDELSE.
	*/

	begin
	
	
	
	
		jobbnummer:=1; --------- Alle Hendelses type untat Plassering, utlån, utstilling, og Tilstandsvurdering går her  -------------	
    PrimusOppdJobbNrIVersjon( 54, jobbnummer );
		if jobnummer_in <= jobbnummer then	
		  DECLARE		  
        iId Integer;
        lastBeskId Integer;
        newnr  Integer;
		  BEGIN  
        lastBeskId := -1;
			  For TH in (select DISTINCT T.objid
                                 , T.HID
                                 , MH.DATO
                                 , MH.TIL_DATO
                                 , MH.NEWHOVEDTYPE
                                 , MH.NEWUNDERTYPE
                                 , MH.beskrivende_objekt
                                 , T.TILSTANDID
                                 , T.KODE
                                 , T.BESKRIVELSE
                                 , T.BESKRIVELSE ||Chr(10)|| To_CHAR(MH.tekst) as tekst
						                  FROM MUSHENDELSE MH 
						                 INNER JOIN TILSTAND_H T 
                                     ON MH.HID=T.HID
						                 WHERE ( ( MH.hovedtype='Tilstandsvurdering' ) 
                                    OR ( MH.hovedtype='Tillståndsvärdering' ) )
						                   AND MH.MERKE is NULL
                               order by mh.beskrivende_objekt, tilstandid desc)
			  Loop	
	            
          iId := 0;    
          if th.beskrivende_objekt is null then
            execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;				
          else
            iId := th.beskrivende_objekt;
          end if;

          if lastBeskId <> iId then
            execute immediate 'Insert INTO ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE) values (:a, :b, :c)' using iId, TH.HID, TH.NEWHOVEDTYPE;								   
            execute immediate 'BEGIN SAVE_AHUT_DATERING(:a, :b, :C, :d); END;' using iId, TH.hid, th.dato, th.til_dato;
  
            if TH.tekst is not NULL then
              execute immediate 'insert into obj_beskrivelse_h(objid, hid, beskrivelse) values (:a, :b, :c)' using iId, TH.hid, th.tekst;
              execute immediate 'update adm_hendelse set hid_beskrivelse = :a where objid = :b' using th.hid, iId;
            End if;	
          
            execute immediate 'Insert Into TILSTAND(OBJID,HID, TILSTANDSKODE, BESKRIVELSE) 
                       values (:a, :b, :c, :d)' using iId, TH.Hid, TH.KODE, TH.tekst;	
            execute immediate 'Update ADM_HENDELSE set HID_TILSTAND=:a Where objid=:b' using TH.HID, iId;						   

            select ( nvl( max( nr ), 0 ) + 1 ) into newNr
                                            from objekt_adm_hendelse 
                                           where objid = th.objid
                                             and admh_objid = iId ;
          
            execute immediate 'Insert Into OBJEKT_ADM_HENDELSE
                                   ( ADMH_OBJID
                                   , OBJID
                                   , OBJID_SISTE
                                   , HID
                                   , NR
                                   , FRADATO
                                   , TILDATO ) 
                                   values ( :a
                                          , :b
                                          , :c
                                          , :d
                                          , :e
                                          , :f
                                          , :g)' 
                                  using iId
                                      , TH.OBJID
                                      , TH.OBJID
                                      , TH.Hid
                                      , newNr
                                      , TH.DATO
                                      , TH.TIL_DATO;                				

            execute immediate 'update TILSTAND_H set MERKE=''X'' Where TILSTANDID = :b ' using TH.TILSTANDID;                
                  
            execute immediate 'update MUSHENDELSE set MERKE=''X'' Where HID = :b ' using  TH.HID;										
          end if;
          
          lastBeskId := iId;
			  END LOOP;	
			
        commit;  
      exception
        when no_data_found then
          dbms_output.put_line( '54 --- no data found; ingen tilstandsvurderinger funnet' );
		  END;	
    			
		End if;	
		
    jobbnummer:= 2;	
		if jobnummer_in <= jobbnummer then
      PrimusOppdJobbNrIVersjon( 54, jobbnummer );    
		  DECLARE		  
        iId Integer;		  
		  BEGIN  
			  For TH in (select DISTINCT MP.objid
                                 , MP.HID
                                 , MH.DATO
                                 , MH.TIL_DATO
                                 , MH.NEWHOVEDTYPE
                                 , MH.NEWUNDERTYPE
                                 , MH.beskrivende_objekt
                                 , dbms_lob.getlength( MH.tekst ) ltekst
                              FROM Superobjekt_hendelse MP
                                 , MUSHENDELSE MH 						 
                             WHERE MP.HID = MH.HID
						                   AND ( ( MH.hovedtype='Tilstandsvurdering' ) 
                                   OR ( MH.hovedtype='Tillståndsvärdering' ) )
                               AND MH.MERKE is NULL)
			  Loop	
	            
				iId := 0;    
        if th.beskrivende_objekt is null then
          execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;				
        else
          iId := th.beskrivende_objekt;
        end if;
        
        execute immediate 'Insert INTO ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE) values (:a, :b, :c)' using iId, TH.HID, TH.NEWHOVEDTYPE;								   
				execute immediate 'BEGIN SAVE_AHUT_DATERING(:a, :b, :C, :d); END;' using iId, TH.hid, th.dato, th.til_dato;
				execute immediate 'Insert Into OBJEKT_ADM_HENDELSE(ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR, FRADATO, TILDATO) 
				                   values (:a, :b, :c, :d, :e, :f, :g)' using iId, TH.OBJID, TH.OBJID, TH.Hid, 1,  TH.DATO, TH.TIL_DATO;                				
				if TH.ltekst > 0 then
          execute immediate 'BEGIN SAVE_AH_BESKRIVELSE(:a, :b); END;' using iId, TH.hid;
				End if;	               							   

				execute immediate 'update MUSHENDELSE set MERKE=''X'' Where HID = :b ' using  TH.HID;										
			  END LOOP;	
			commit;  
		  exception 
        when no_data_found then
          dbms_output.put_line( ' 54 --- no data found; ingen tilstandsvurderinger i db? ' );
      END;	
    			
		End if;	


    jobbnummer:= 3;	
		if jobnummer_in <= jobbnummer then	
      PrimusOppdJobbNrIVersjon( 54, jobbnummer );
		  DECLARE		  
        iId Integer;		  
        iHVT INTEGER;
        lastObjid integer;
        lastHID integer;
			BEGIN  			  
			  Select TYPEID into iHVT from ADM_HENDELSETYPE_L Where (tekst ='Tilstandsvurdering') OR (tekst ='Tillståndsvärdering');		  
			  
        lastObjid := -1;
        lastHID := -1;
			  
        For TH in (select DISTINCT TL.objid
                                 , TL.HID
                                 , TL.BESKRIVELSE
                                 , TL.KODE
                                 , TL.TILSTANDID
                                 , HD.DATO
                              FROM TILSTAND_H TL
                                 , HENDELSE HD 
                             WHERE TL.MERKE is NULL 
                               AND TL.OBJID is not NULL 
--                               AND TL.kode is not null 
                               AND TL.HID=HD.HID 
                             order by tl.objid
                                    , tl.hid
                                    , tl.tilstandid desc)
				Loop	
          if lastObjid <> th.objid OR lastHID <> th.hid then 
            iId := 0;    
            execute immediate 'BEGIN GETNEW_OBJID(:a, :b); END;' using OUT iId, TH.hid;				
            execute immediate 'Insert INTO ADM_HENDELSE(OBJID, HID_OPPRETTET, HOVEDTYPE) values (:a, :b, :c)' using iId, TH.HID, iHVT;								   				
            execute immediate 'BEGIN SAVE_AHUT_DATERING(:a, :b, :C, :d); END;' using iId, TH.hid, th.dato, th.dato;				
            execute immediate 'Insert Into OBJEKT_ADM_HENDELSE(ADMH_OBJID, OBJID, OBJID_SISTE, HID, NR) values (:a, :b, :c, :d, :e)' using iId, TH.OBJID, TH.OBJID, TH.Hid, 1;
            if TH.BESKRIVELSE is not NULL then
               execute immediate 'Insert Into OBJ_BESKRIVELSE_H(OBJID, HID, BESKRIVELSE) values (:a, :b, :c)' USING iId, TH.hid, th.BESKRIVELSE; 
               execute immediate 'UPDATE ADM_HENDELSE set HID_BESKRIVELSE = :a where objid = :b' using TH.hid, iId;						 
                End if;			
          
            execute immediate 'Insert Into TILSTAND(OBJID,HID, TILSTANDSKODE, BESKRIVELSE) 
                       values (:a, :b, :c, :d)' using iId, TH.Hid, TH.KODE, TH.BESKRIVELSE;
                      execute immediate 'Update ADM_HENDELSE set HID_TILSTAND=:a Where objid=:b' using TH.HID, iId;								   				
          end if;
          lastObjid := th.objid;
          lastHID := th.hid;
          
          execute immediate 'update TILSTAND_H set MERKE=''X'' Where TILSTANDID = :b ' using TH.TILSTANDID;						
			  END LOOP;	
        commit;  				  
		  exception
        when no_data_found then
          dbms_output.put_line( ' 54 -- no data found; ingen data funnet for tilstand_h - overføring til adm_hendelse ' );
      END;	
    			
		End if;		
		
	
    jobbnummer := 100;	
    commit;	
    return 'OK';
	exception
		when others then
			ROLLBACK;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus54 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(54, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);			
			return 'FEIL!';
	end;

end;
/
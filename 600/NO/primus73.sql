CREATE OR REPLACE FUNCTION primus73(jobbnummer OUT NUMBER, jobnummer_in NUMBER)
  RETURN VARCHAR2
AUTHID CURRENT_USER
IS
  BEGIN
    dbms_output.enable;

    BEGIN
	
	jobbnummer := 1;
      IF jobnummer_in <= jobbnummer  THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- Kopiere bygningsoppgave bygningsdelstypeid til bygning_behandling_bygningsdel
       EXECUTE IMMEDIATE
        'begin
		FOR REC IN (SELECT AHK.PARENT_ID AS OBJID, AHK.HID_OPPRETTET AS HID, OG.BYGNINGSDELSTYPEID AS BYGNINGSDELSTYPEID
						  FROM ADM_HENDELSE AHK INNER JOIN OPPGAVE OG ON AHK.PARENT_ID = OG.OBJID
						  WHERE AHK.HOVEDTYPE = 20 AND AHK.PARENT_ID IN (SELECT OBJID FROM BYGNING_BEHANDLING))
			  LOOP
				INSERT INTO BYGNING_BEHANDLING_BYGNINGSDEL(OBJID, HID, BYGNINGSDELSTYPEID) VALUES (REC.OBJID, REC.HID, REC.BYGNINGSDELSTYPEID);
				COMMIT;
				UPDATE BYGNING_BEHANDLING SET HID_BYGNINGSDEL = REC.HID WHERE OBJID = REC.OBJID;
				COMMIT;
			  END LOOP;
		  
		  END;';
      END IF;
      jobbnummer := 2;
      IF jobnummer_in <= jobbnummer  THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- Kopiere bygningsoppgave kostnad til bygning_behandling_kostnad
       EXECUTE IMMEDIATE
         'BEGIN
          FOR REC IN (SELECT AHK.PARENT_ID AS OBJID, AHK.HID_OPPRETTET AS HID, AHKOST.KOSTNAD AS KOSTNAD , AHKOST.VALUTA_ID AS VALUTA_ID, AHKOST.KOMMENTAR AS KOMMENTAR
                      FROM ADM_HENDELSE AHK
                        LEFT JOIN ADM_H_KOSTNAD AHKOST ON AHK.OBJID = AHKOST.OBJID AND AHK.HID_KOSTNAD = AHKOST.HID
                      WHERE AHK.HOVEDTYPE = 20 AND AHK.PARENT_ID IN (SELECT OBJID FROM BYGNING_BEHANDLING))
          LOOP
            INSERT INTO BYGNING_BEHANDLING_KOSTNAD(OBJID, HID, KOSTNAD, VALUTA_ID, KOMMENTAR)
            VALUES(REC.OBJID, REC.HID, REC.KOSTNAD, REC.VALUTA_ID, REC.KOMMENTAR);
            COMMIT;
            UPDATE BYGNING_BEHANDLING SET HID_KOSTNAD = REC.HID WHERE OBJID = REC.OBJID;
            COMMIT;
          END LOOP;
          END;';
        COMMIT;
      END IF;
      jobbnummer := 3;
      IF jobnummer_in <= jobbnummer THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- Kopiere bygningsoppgave materiale til bygning_behandling_materiale
      
	   EXECUTE IMMEDIATE
        'begin      
		  FOR REC IN (SELECT AHK.PARENT_ID AS OBJID, AHK.HID_OPPRETTET AS HID, K.MATERIALBRUK AS MATERIALE
						  FROM ADM_HENDELSE AHK
							INNER JOIN KOMMENTAR K ON AHK.OBJID = K.OBJID
						  WHERE AHK.HOVEDTYPE = 20 AND AHK.PARENT_ID IN (SELECT OBJID FROM BYGNING_BEHANDLING))
			  LOOP
				INSERT INTO BYGNING_BEHANDLING_MATERIALE(OBJID, HID, MATERIALE)
				VALUES(REC.OBJID, REC.HID, REC.MATERIALE);
				COMMIT;
				UPDATE BYGNING_BEHANDLING SET HID_MATERIALE = REC.HID WHERE OBJID = REC.OBJID;
				COMMIT;
			  END LOOP;
		  End;';
		  
        COMMIT;
      END IF;
	  
      jobbnummer := 4;
      IF jobnummer_in <= jobbnummer THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- Kopiere bygningsoppgave tidsbruk til bygning_behandling_tidsbruk
      EXECUTE IMMEDIATE
      '       
	   begin
		 FOR REC IN (SELECT AHK.PARENT_ID AS OBJID, AHK.HID_TIDSBRUK AS HID, AHTID.TIMER AS TIDSBRUK
                      FROM ADM_HENDELSE AHK
                        INNER JOIN ADM_H_TIDSBRUK AHTID ON AHK.OBJID = AHTID.OBJID AND AHK.HID_TIDSBRUK = AHTID.HID
                      WHERE AHK.HOVEDTYPE = 20 AND AHK.PARENT_ID IN (SELECT OBJID FROM BYGNING_BEHANDLING))
          LOOP
            INSERT INTO BYGNING_BEHANDLING_TIDSBRUK(OBJID, HID, TIDSBRUK)
            VALUES(REC.OBJID, REC.HID, REC.TIDSBRUK);
            COMMIT;
            UPDATE BYGNING_BEHANDLING SET HID_TIDSBRUK = REC.HID WHERE OBJID = REC.OBJID;
            COMMIT;
          END LOOP;
		 End;';
        
        COMMIT;
      END IF;
      -- Set hid_slettet for alle bygningsoppgavekommentarer
      jobbnummer := 5;
      IF jobnummer_in <= jobbnummer THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        DECLARE
          NHID NUMBER;
        BEGIN
          SELECT HIDSEQ.NEXTVAL
          INTO NHID
          FROM DUAL;
          EXECUTE IMMEDIATE 'insert into hendelse (hid, dato, signid) values (:a, sysdate, 1)' USING NHID;
          EXECUTE IMMEDIATE
          'UPDATE ADM_HENDELSE AH SET HID_SLETTET = :a
           WHERE EXISTS (SELECT * FROM BYGNING_BEHANDLING BB WHERE AH.PARENT_ID = BB.OBJID)
            AND AH.HOVEDTYPE = 20 AND AH.HID_SLETTET IS NULL' USING NHID;
        END;
        COMMIT;
      END IF;
      jobbnummer := 6;
      IF jobnummer_in <= jobbnummer  THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- Kopier resterende oppgaver til oppgave2
        EXECUTE IMMEDIATE
        'INSERT INTO OPPGAVE2(OBJID, UUID, HID_OPPRETTET, HID_SLETTET, BESKRIVELSE, START_DATO, FRIST_DATO, UTFORT_FRA_DATO, UTFORT_TIL_DATO)
         SELECT AH.OBJID, AH.UUID, AH.HID_OPPRETTET, AH.HID_SLETTET, OB.BESKRIVELSE, SOD.FRADATO, OG.FRIST, SOD.FRADATO, SOD.TILDATO
         FROM ADM_HENDELSE AH
         INNER JOIN OPPGAVE OG ON AH.OBJID = OG.OBJID
         INNER JOIN OBJ_BESKRIVELSE_H OB ON AH.OBJID = OB.OBJID AND AH.HID_BESKRIVELSE = OB.HID
         LEFT JOIN SUPEROBJEKT_DATERING_H SOD ON AH.OBJID = SOD.OBJID AND AH.HID_DATERING = SOD.HID
         WHERE AH.HOVEDTYPE = 19';
        COMMIT;
      END IF;
      jobbnummer := 7;
      IF jobnummer_in <= jobbnummer   THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- Kopier oppgavehendelse personer til oppgave2_person
        EXECUTE IMMEDIATE
        'INSERT INTO OPPGAVE2_PERSON(OBJID, HID, NR, OBJID_SISTE, JP_OBJID, ROLLEID, REKKEFOLGE)
         SELECT AHP.OBJID, AHP.HID, AHP.NR, AHP.OBJID_SISTE, JP.OBJID, RJP.ROLLEID, AHP.REKKEFOLGE
         FROM ADM_HENDELSE_PERSON AHP
         INNER JOIN OPPGAVE2 O2 ON AHP.OBJID = O2.OBJID
         INNER JOIN JURPERSON JP ON AHP.JPNR = JP.JPNR
         LEFT JOIN ROLLEJP_L RJP ON AHP.ROLLEKODE = RJP.ROLLEKODE AND AHP.ROLLEKODE IS NOT NULL';
        COMMIT;
      END IF;
      jobbnummer := 8;
      IF jobnummer_in <= jobbnummer THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
          -- Kopier admhendelse kommentar til superobjekt_kommentar
       EXECUTE IMMEDIATE
        'DECLARE
         PREV_OBJID NUMBER := 0;
         NR NUMBER := 1;
         BEGIN
           FOR REC IN (SELECT AH.PARENT_ID AS OBJID, OB.HID AS HID, AH.PARENT_ID AS OBJID_SISTE, OB.BESKRIVELSE AS BESKRIVELSE
                       FROM ADM_HENDELSE AH
                        LEFT JOIN OBJ_BESKRIVELSE_H OB ON AH.OBJID = OB.OBJID AND AH.HID_BESKRIVELSE = OB.HID
                       WHERE HOVEDTYPE = 20 AND HID_SLETTET IS NULL AND OB.HID IS NOT NULL
                       ORDER BY AH.PARENT_ID, OB.HID)
           LOOP
             IF REC.OBJID = PREV_OBJID THEN
               NR := NR + 1;
             ELSE
               NR := 1;
             END IF;
             PREV_OBJID := REC.OBJID;
             DBMS_OUTPUT.put_line (''OBJID: '' || REC.OBJID || '' HID: '' || REC.HID || '' NR: '' || NR);
             INSERT INTO SUPEROBJEKT_KOMMENTAR(OBJID, HID, NR, OBJID_SISTE, BESKRIVELSE) VALUES(REC.OBJID, REC.HID, NR, REC.OBJID_SISTE, REC.BESKRIVELSE);
           END LOOP;
        END;';
        COMMIT;
      END IF;
      jobbnummer := 9;
      IF jobnummer_in <= jobbnummer  THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- OPPRETT PROSEDYRE FRA INTERVALL OG OPPGAVE
      EXECUTE IMMEDIATE
        '
		DECLARE
          SEQ NUMBER := 0;
          O_OBJID NUMBER := 0;
          NR NUMBER := 1;
        BEGIN
          FOR REC IN
          (SELECT AH.OBJID AS OBJID, AH.HID_OPPRETTET AS HID, OB.BESKRIVELSE AS BESKRIVELSE, 
		  IV.HID AS I_HID, IV.INTERVALLTYPEID AS I_INTERVALLTYPEID, IV.INTERVALL AS I_INTERVALL, IV.DAGER AS I_DAGER, 
		  IV.MAANEDTYPEID AS I_MAANEDTYPEID, IV.STARTDATO AS I_STARTDATO, IV.STOPTYPEID AS I_STOPTYPEID, 
		  IV.ANTALL AS I_ANTALL, IV.TELLER AS I_TELLER, IV.STOPDATO AS I_STOPDATO
           FROM ADM_HEND_INTERVALL AHI
             INNER JOIN ADM_HENDELSE AH ON AHI.OBJID = AH.OBJID AND AH.HID_SLETTET IS NULL
             LEFT JOIN OBJ_BESKRIVELSE_H OB ON AH.OBJID = OB.OBJID AND AH.HID_BESKRIVELSE = OB.HID
             INNER JOIN INTERVALL IV ON AHI.INTERVALLID = IV.INTERVALLID AND IV.HID_SLETTET IS NULL
             INNER JOIN OPPGAVE2 O2 ON AHI.OBJID = O2.OBJID
           ORDER BY AH.OBJID)
          LOOP
            IF O_OBJID != REC.OBJID THEN
              -- PROSEDYRE SUPEROBJEKTID
              SELECT OBJIDSEQ.NEXTVAL INTO SEQ FROM DUAL;
              INSERT INTO SUPEROBJEKT(OBJID, SUPEROBJEKTTYPEID) values(SEQ, 9);
              COMMIT;
              -- PROSEDYRE
              INSERT INTO PROSEDYRE(OBJID, UUID, HID_OPPRETTET, BESKRIVELSE) VALUES(SEQ, GETORACLEUUID(), REC.HID, REC.BESKRIVELSE);
              COMMIT;
              -- KOPIERE ADM_HENDELSE_PERSON TIL PROSEDYRE_PERSON
              FOR REC3 IN
              (SELECT AHP.HID AS HID, AHP.NR AS NR, JP.OBJID AS JP_OBJID, RJP.ROLLEID AS ROLLEID, AHP.REKKEFOLGE AS REKKEFOLGE
               FROM ADM_HENDELSE_PERSON AHP
                 INNER JOIN ADM_HENDELSE AH ON AHP.OBJID_SISTE = AH.OBJID AND AH.OBJID = REC.OBJID
                 INNER JOIN JURPERSON JP ON AHP.JPNR = JP.JPNR
                 LEFT JOIN ROLLEJP_L RJP ON AHP.ROLLEKODE = RJP.ROLLEKODE AND AHP.ROLLEKODE IS NOT NULL)
              LOOP
                INSERT INTO PROSEDYRE_PERSON(OBJID, HID, NR, OBJID_SISTE, JP_OBJID, ROLLEID, REKKEFOLGE) VALUES(SEQ, REC3.HID, REC3.NR, SEQ, REC3.JP_OBJID, REC3.ROLLEID, REC3.REKKEFOLGE);
              END LOOP;
              COMMIT;
              -- PROSEDYRE INTERVALL
              INSERT INTO PROSEDYRE_INTERVALL(OBJID, HID, INTERVALLTYPEID, INTERVALL, DAGER, MAANEDTYPEID, STARTDATO, STOPTYPEID, ANTALL, TELLER, STOPDATO) VALUES(SEQ, REC.I_HID, REC.I_INTERVALLTYPEID, REC.I_INTERVALL, REC.I_DAGER, REC.I_MAANEDTYPEID, REC.I_STARTDATO, REC.I_STOPTYPEID, REC.I_ANTALL, REC.I_TELLER, REC.I_STOPDATO);
              COMMIT;
              -- PROSEDYRE -> PROSEDYRE_INTERVALL RELASJON
              UPDATE PROSEDYRE SET HID_INTERVALL = REC.I_HID WHERE OBJID = SEQ;
              COMMIT;
              -- SUPEROBJEKT -> PROSEDYRE RELASJON
				  FOR REC2 IN
				  (SELECT OBJID, HID, NR, OBJID_SISTE
				   FROM OBJEKT_ADM_HENDELSE
				   WHERE ADMH_OBJID = REC.OBJID)
				  LOOP
					INSERT INTO SUPEROBJEKT_PROSEDYRE(OBJID, HID, NR, OBJID_SISTE, PROSEDYRE_OBJID) VALUES(REC2.OBJID, REC2.HID, REC2.NR, REC2.OBJID_SISTE, SEQ);
				  END LOOP;
			  
              COMMIT;
              O_OBJID := REC.OBJID;
              NR := 1;
            ELSE
              NR := NR + 1;
            END IF;
            -- PROSEDYRE -> OPPGAVE2 RELASJON
            INSERT INTO SUPEROBJEKT_OPPGAVE2(OBJID, HID, NR, OBJID_SISTE, OPPGAVE2_OBJID) VALUES(SEQ, REC.I_HID, NR, SEQ, REC.OBJID);
          END LOOP;
        END;';
		
      END IF;
      jobbnummer := 10;
      -- Kopiere objekt->oppgave relasjoner til superobjekt_oppgave2
      IF jobnummer_in <= jobbnummer
      THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        EXECUTE IMMEDIATE
        'INSERT INTO SUPEROBJEKT_OPPGAVE2(OBJID, HID, NR, OBJID_SISTE, OPPGAVE2_OBJID)
         SELECT OAH.OBJID, OAH.HID, OAH.NR, OAH.OBJID_SISTE, OAH.ADMH_OBJID
          FROM OBJEKT_ADM_HENDELSE OAH
         WHERE OAH.ADMH_OBJID IN (SELECT OBJID FROM OPPGAVE2)';
        COMMIT;
      END IF;	
	
	
      jobbnummer := 11;
      IF jobnummer_in <= jobbnummer
      THEN
        PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        
        -- objekt_altnr tabell
        EXECUTE IMMEDIATE
        'CREATE TABLE OBJEKT_ALTNR
        ( OBJID NUMBER(8,0) NOT NULL ENABLE
        , HID NUMBER(8,0) NOT NULL ENABLE
        , NR NUMBER(7,0) NOT NULL ENABLE
        , OBJID_SISTE NUMBER(8,0)
        , ALTNRTYPEID NUMBER(8,0)
        , ALTNR VARCHAR2( 150 CHAR )
        , BESKRIVELSE VARCHAR2(50 CHAR)
        , FRADATO DATE
        , TILDATO DATE
        , REKKEFOLGE NUMBER(3,0)
        , CONSTRAINT PK_OBJEKT_ALTNR PRIMARY KEY (OBJID, HID, NR)
        , CONSTRAINT FK_OBJECT_ALTNR_OBJID FOREIGN KEY (OBJID)
            REFERENCES OBJEKT (OBJID) ENABLE
        , CONSTRAINT FK_OBJEKT_ALTNR_OBJID_SISTE FOREIGN KEY (OBJID_SISTE)
            REFERENCES OBJEKT (OBJID) ENABLE
        , CONSTRAINT FK_OBJEKT_ALTNR_HID FOREIGN KEY (HID)
            REFERENCES HENDELSE (HID) ENABLE
        , CONSTRAINT FK_OBJEKT_ALTNR_ALTNRTYPEID FOREIGN KEY (ALTNRTYPEID)
            REFERENCES ALTNR_L (TYPEID) ENABLE)';
        COMMIT;

      END IF;
      jobbnummer := 12;
      IF jobnummer_in <= jobbnummer
      THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- Kopier altnr til objekt_altnr
        EXECUTE IMMEDIATE '		
			DECLARE        
			  O_OBJID NUMBER := 0;
			  O_NR NUMBER := 1;
			BEGIN
			  
			  FOR REC IN
			  (SELECT OBJID, HID, NR, ALTNRID, ALTNR, BESKRIVELSE,  REKKEFOLGE FROM ALTNR  ORDER BY OBJID, HID)
			  LOOP
				IF O_OBJID != REC.OBJID THEN                                       
				  O_OBJID := REC.OBJID;
				  O_NR := 1;
				ELSE O_NR := O_NR + 1;
				END IF;				
				INSERT INTO OBJEKT_ALTNR(OBJID, HID, NR, OBJID_SISTE, ALTNRTYPEID, ALTNR, BESKRIVELSE,  REKKEFOLGE) 
				VALUES(O_OBJID, REC.HID, O_NR, O_OBJID, REC.ALTNRID, REC.ALTNR, REC.BESKRIVELSE,  REC.REKKEFOLGE);
			  END LOOP;
			END;';
        COMMIT;
      END IF;

      jobbnummer := 13;
      IF jobnummer_in <= jobbnummer
      THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- Drop altnr tabell
        EXECUTE IMMEDIATE 'DROP TABLE ALTNR PURGE';
        COMMIT;
      END IF;
      jobbnummer := 14;
      IF jobnummer_in <= jobbnummer
      THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- Oppdater skade statusid
        EXECUTE IMMEDIATE
        'UPDATE SUPEROBJEKT_STATUS_H SOS1 SET STATUSID =
        (SELECT SOS2.STATUSID + 4
         FROM SUPEROBJEKT_STATUS_H SOS2
           INNER JOIN SUPEROBJEKT SO ON SOS2.OBJID = SO.OBJID AND SOS2.HID = SO.HID_STATUS
           INNER JOIN ADM_HENDELSE AH ON SO.OBJID = AH.OBJID AND AH.HOVEDTYPE = 18
         WHERE SOS2.OBJID = SOS1.OBJID AND SOS2.HID = SOS1.HID)
        WHERE SOS1.KODE IS NULL AND EXISTS
        (SELECT NULL
         FROM SUPEROBJEKT_STATUS_H SOS2
           INNER JOIN SUPEROBJEKT SO ON SOS2.OBJID = SO.OBJID AND SOS2.HID = SO.HID_STATUS
           INNER JOIN ADM_HENDELSE AH ON SO.OBJID = AH.OBJID AND AH.HOVEDTYPE = 18
         WHERE SOS2.OBJID = SOS1.OBJID AND SOS2.HID = SOS1.HID)';
        COMMIT;
      END IF;
      jobbnummer := 15;
      IF jobnummer_in <= jobbnummer
      THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- Set status koder
        EXECUTE IMMEDIATE
        'UPDATE SUPEROBJEKT_STATUS_H SOSH SET KODE =
         (SELECT SOSL.KODE
          FROM SUPEROBJEKT_STATUS_L SOSL
          WHERE SOSL.STATUSID = SOSH.STATUSID)
         WHERE EXISTS
         (SELECT NULL
          FROM SUPEROBJEKT_STATUS_L SOSL
          WHERE SOSL.STATUSID = SOSH.STATUSID)';
        COMMIT;
      END IF;
      jobbnummer := 16;
      IF jobnummer_in <= jobbnummer
      THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- Oppdater tilstandsvurderingsformål med uuid fra Kulturnav, slik at de blir felles i alle databaser og kulturnav
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET UUID = ''61f5c4a5-93fc-4bd8-93fb-5561b9829503'' WHERE FORMAALTYPEID = 1';
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET UUID = ''fcb65c3b-e0c0-441a-b873-7cef3136ecc3'' WHERE FORMAALTYPEID = 2';
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET UUID = ''8fbcd0a5-e865-4d87-9bde-a5f059ab6be1'' WHERE FORMAALTYPEID = 3';
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET UUID = ''e38d99f1-4bab-4593-86e7-c81240e20179'' WHERE FORMAALTYPEID = 4';
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET UUID = ''8e982345-a5d9-4254-b608-8b4d48f53bf2'' WHERE FORMAALTYPEID = 5';
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET UUID = ''08cf66a4-a1cf-4400-af11-eda94a4eec2e'' WHERE FORMAALTYPEID = 6';
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET UUID = ''90ee69fc-a82c-41cc-9d64-4d93b49b8507'' WHERE FORMAALTYPEID = 7';
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET UUID = ''c5c37746-0cb5-45b1-b73b-4fc0fb5abd06'' WHERE FORMAALTYPEID = 8';
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET UUID = ''3f95f913-68f1-4a40-b124-055c3afe6a4c'' WHERE FORMAALTYPEID = 11';
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET UUID = ''53cc2bad-42fb-42ea-8679-cbe040746349'' WHERE FORMAALTYPEID = 12';
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET UUID = ''fc7ca3f5-cd6a-4036-a417-ae818f3a1d49'' WHERE FORMAALTYPEID = 13';
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET UUID = ''84b98f36-21d3-4bfe-b933-8c98a983f147'' WHERE FORMAALTYPEID = 14';
        COMMIT;
      END IF;

      jobbnummer := 17;
      IF jobnummer_in <= jobbnummer
      THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- Endre admfilterfeltid for formål bygniningstilstandsvurdering
        EXECUTE IMMEDIATE 'UPDATE ADM_H_FORMAALTYPE_L SET ADMFILTERFELTID = 12 WHERE FILTERFELTID = 3 AND ADMFILTERFELTID = 11';
        COMMIT;
      END IF;
      
      jobbnummer := 18;
      IF jobnummer_in <= jobbnummer
      THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );
        -- Filter for formål tilstandsvurdering
        EXECUTE IMMEDIATE ' INSERT INTO ADM_H_FORMAALTYPE_L_FILTER(FILTERID, FORMAALTYPEID, ADMHENDELSETYPEID)
        SELECT FORMAALTYPEID, FORMAALTYPEID, ADMFILTERFELTID
        FROM ADM_H_FORMAALTYPE_L
        WHERE HID_SLETTET IS NULL';
	  End if;	

     jobbnummer := 19;
      IF jobnummer_in <= jobbnummer
      THEN
	  PrimusOppdJobbNrIVersjon( 73, jobbnummer );       
	   -- Filter for formål billedkunst tilstandsvurdering
       EXECUTE IMMEDIATE '
	   DECLARE
            FILTERID NUMBER := 100;
        BEGIN
          FOR REC IN (SELECT FORMAALTYPEID FROM ADM_H_FORMAALTYPE_L  WHERE ADMFILTERFELTID = 11 AND HID_SLETTET IS NULL)
          LOOP
            INSERT INTO ADM_H_FORMAALTYPE_L_FILTER(FILTERID, FORMAALTYPEID, ADMHENDELSETYPEID) VALUES(FILTERID, REC.FORMAALTYPEID, 12);
            FILTERID := FILTERID + 1;
          END LOOP;
        END;';

        COMMIT;
      END IF;

      jobbnummer := 20;
      IF jobnummer_in <= jobbnummer
      THEN
        PrimusOppdJobbNrIVersjon( 73, jobbnummer );       
        -- Filter for formål billedkunst tilstandsvurdering
        EXECUTE IMMEDIATE 'grant insert
                               , update 
                              on primus.objekt_altnr 
                              to pri_reg';
      END IF;
      
      RETURN 'OK';
      EXCEPTION
      WHEN OTHERS THEN
      ROLLBACK;
      SYS.DBMS_OUTPUT.PUT_LINE('Primus73 feilet! Jobbnummer: ' || jobbnummer);
      PRIMUSOPPGRADERINGSTOPP(73, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
      RETURN 'FEIL!';
    END;
  END;

/

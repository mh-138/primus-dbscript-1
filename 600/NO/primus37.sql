create or replace function primus37(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is

  constraintName varchar2(30 char);
begin
	dbms_output.enable;

	/*
   
	*/

	begin

  jobbnummer:=1;  	 
	if jobnummer_in <= jobbnummer then
    begin    
      select uc.constraint_name
        into constraintName
        from user_constraints uc
       inner join user_cons_columns ucc
               on uc.constraint_name = ucc.constraint_name
              and ucc.column_name = 'HID'
              and ucc.position = 1
      where uc.table_name = 'ADM_HENDELSE_PERSON'
        and uc.constraint_type = 'R';
      
      EXECUTE IMMEDIATE 'alter table ADM_HENDELSE_PERSON drop constraint ' || constraintName; 
    exception
      when no_data_found then
        dbms_output.put_line( '-- feil uten betydning - fant ingen foreign key jobb 10' );
    end;      
	End if; 
  jobbnummer:=2;  
	if jobnummer_in <= jobbnummer then	
	 EXECUTE IMMEDIATE 'alter table ADM_HENDELSE_PERSON add constraint FK_HID_AHP foreign key(HID) references HENDELSE(HID)';
	End if; 
	
	jobbnummer:=3;	
	if jobnummer_in <= jobbnummer then
	  EXECUTE IMMEDIATE 'alter table SIGNATUR add (EMAIL VARCHAR2(100 CHAR))';
	End if;
		
	jobbnummer:=100;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus37 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(37, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
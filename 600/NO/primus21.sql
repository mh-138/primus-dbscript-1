create or replace function primus21(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is

begin
  dbms_output.enable;
  
  begin
	
	jobbnummer:=1;	
	 if  jobnummer_in <= jobbnummer then
				declare
					nnewhid number;
					nbehandlid number;
					nbehandlidParent number;
				  begin
					 select hidseq.nextval into nnewhid from dual;
					 insert into hendelse (hid, signid, dato) values (nnewhid, 1, sysdate);

          if svenskEllerNorskDatabase = 'NOR' then
            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l(behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Bleking'', null, :b||''/'' , :c, ''9c0915ca-98f0-453a-8bb9-75966cc41037'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l(behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Demontering'', null, :b||''/'' , :c, ''82057d52-0d61-4733-a227-872379745f38'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Elektrolyse'', null, :b||''/'', :c ,''118a4f45-6a16-44ab-8159-945f9facbf21'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l(behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Elektrolytisk reduksjon'',:b, :c||''/''|| :d||''/'', :e,''1e7feaf0-53b2-4264-be69-6309d2386e45'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Elektrolytisk reduksjon; konsoliderbar'',:b, :c || ''/'' || :d ||''/'', :e,''b2307c79-110a-4194-8bb4-1fc0068a4576'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Fiksering'',null,:b||''/'', :c,''0c2c8488-1087-4945-bc0d-0241ba9ebaf3'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Gløding'',null, :b||''/'', :c,''b76c4529-0e73-4ed5-be35-fc51917bbd0b'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Impregnering'', null, :b||''/'', :c,''450b0059-d554-428b-94d5-c979cdf4e756'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Impregnering; oppløsning'', :b, :c ||''/'' || :d ||''/'', :e,''b3f62ac3-cffd-4ccd-ab2c-2bfd133b3219'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Impregnering; oppløsning; vacuum'',:b, :c|| ''/'' || :d ||''/'', :e,''16baa04c-47e9-4f18-88ce-cd55439a58af'') '
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Inhibering'',null,:b||''/'', :c,''013299c1-cea6-43ae-806d-7e369675bcbb'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Kompleksbinder'', null,:b||''/'', :c,''785978ea-a278-4c3c-9737-2de551822539'') '
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Konsolidering'',null,:b||''/'', :c,''c8abe8c8-9452-41f4-b997-635ad1d8c5d4'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Lakkering'',null,:b||''/'', :c,''d6b1c1b2-7fa1-4a33-9e3f-e39a7725d495'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Laminering'', null, :b||''/'', :c,''d22c2ba8-b8bf-4fe2-99d8-74422b600792'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Liming'', null, :b||''/'', :c,''3e9262af-376b-4b21-9af7-16fada70cf99'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Nøytraliseinrg'', null, :b||''/'', :c,''085d5ceb-5dde-4095-aa60-a7013bfaa618'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Omkonservering'', null, :b||''/'', :c,''e51735de-c243-46ce-b1b9-9f0fd4c037e1'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Overflatebehandling'', null, :b||''/'', :c,''81879e2a-3d7c-4b7a-9e97-9a4fe6949cd0'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Rensing'',null,:b||''/'', :c,''d75e635b-8556-438f-99db-2e65934866d8'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values(:a, ''Rensing; kjemisk'',:b, :c || ''/'' || :d ||''/'', :e,''00ec5ac8-f63a-4bbf-81f6-c17363ff656c'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Rensing; mekanisk; manuell'',:b, :c || ''/'' || :d ||''/'', :e,''0e9c39df-8a22-4293-a6f2-cdf1acab5066'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Rensing;  mekanisk; sandblåsing'',:b, :c || ''/'' || :d ||''/'', :e,''3efdfbe5-1c2b-465d-ba5a-ffbb12f16ac6'') '
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Rensing; tørr'',:b, :c || ''/'' || :d ||''/'', :e,''9dfce649-f627-4d77-8ce2-2f4a625a847e'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Rensing; våt'',:b, :c || ''/'' || :d ||''/'', :e,''93ca47c1-2885-471f-a843-1279785c3673'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values  (:a, ''Rensing; laser'',:b, :c || ''/'' || :d ||''/'', :e,''19cb4503-1829-48ec-a50e-143db1f04707'') '
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Retusjering'',null,:b||''/'', :c,''1358678a-d30c-4898-a43e-e491b03e248a'') '
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Tørking'',null,:b||''/'', :c,''6d1e70ac-aa80-4b8c-b2f4-1c6fc385058b'')'
            using nbehandlid,  nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Tørking; i løsemiddel'',:b, :c || ''/'' || :d ||''/'', :e,''9783d957-a21f-474c-97c2-124626c0a9da'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Tørking; i luft'',:b, :c || ''/'' || :d ||''/'', :e,''34407612-bb27-463e-84be-9a76da11ef0d'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Tørking ; varmeskap'',:b, :c || ''/'' || :d ||''/'', :e,''1a301c74-8393-4406-9b0b-74bc0233652b'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Tørking; frysetørking'',:b, :c || ''/'' || :d ||''/'', :e,''f01d2777-f860-4377-92d4-451d0b20e65e'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Utvasking'',null,:b||''/'', :c,''5c5b4839-99ed-40f3-847e-38cb930ad1d7'')'
            using nbehandlid,  nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Utvasking av salter, alkalisk sulfitt'',:b, :c || ''/'' || :d ||''/'', :e,''69d09472-1fb1-4bfd-a47d-61558064269d'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Utvasking av salter, elektrolyse'', :b, :c || ''/'' || :d ||''/'', :e,''cffd2fcb-1321-47bc-9fc5-5219a3ea9081'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Utvasking av salter, NaOH'',:b, :c || ''/'' || :d ||''/'', :e,''683e7b96-f0b2-4c89-870e-dc520db67bef'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Utvasking av salter, natriumsesquikarbonat'',:b, :c || ''/'' || :d ||''/'', :e,''459150ca-020a-40d2-9f88-b97b4ad68359'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Utvasking; vann'',:b, :c || ''/'' || :d ||''/'', :e,''e5b80ed7-f8e0-48b4-8968-5ea0a9f5dd4c'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Utvasking; vann; destillert'',:b, :c || ''/'' || :d ||''/'', :e,''88c20ca3-48d0-4580-a074-623c87d85983'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Utvasking; vann; deionisert'',:b, :c || ''/'' || :d ||''/'', :e,''21bcc9fc-a2f9-4e21-9acb-9d9190872beb'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Voksing'',null,:b||''/'', :c,''cc6ee919-fa79-43fd-9430-d7fc0cd7ada8'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, beskrivelse, m_path, hid_opprettet, uuid, merke, filterfeltid)
            values (:a, ''Daglige rutiner'', null, :b||''/'', :c, getoracleuuid(), ''Y'', 3)'
            using nbehandlid,  nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, beskrivelse, m_path, hid_opprettet, uuid, merke, filterfeltid)
            values (:a, ''Periodisk vedlikehold'', null, :b||''/'', :c, getoracleuuid(), ''Y'', 3)'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, beskrivelse, m_path, hid_opprettet, uuid, merke, filterfeltid)
            values (:a, ''Drift, løpende vedlikehold'', null, :b||''/'', :c, getoracleuuid(), ''Y'', 3)'
            using nbehandlid, nbehandlid, nnewhid;
          else
            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l(behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Blekning'', null, :b||''/'' , :c, ''9c0915ca-98f0-453a-8bb9-75966cc41037'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l(behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Demontering'', null, :b||''/'' , :c, ''82057d52-0d61-4733-a227-872379745f38'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Elektrolys'', null, :b||''/'', :c ,''118a4f45-6a16-44ab-8159-945f9facbf21'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l(behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Elektrolytisk reduktion'',:b, :c||''/''|| :d||''/'', :e,''1e7feaf0-53b2-4264-be69-6309d2386e45'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Elektrolytisk reduktion; konsoliderbar'',:b, :c || ''/'' || :d ||''/'', :e,''b2307c79-110a-4194-8bb4-1fc0068a4576'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Fixering'',null,:b||''/'', :c,''0c2c8488-1087-4945-bc0d-0241ba9ebaf3'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Gløding'',null, :b||''/'', :c,''b76c4529-0e73-4ed5-be35-fc51917bbd0b'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Impregnering'', null, :b||''/'', :c,''450b0059-d554-428b-94d5-c979cdf4e756'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Impregnering; oppløsning'', :b, :c ||''/'' || :d ||''/'', :e,''b3f62ac3-cffd-4ccd-ab2c-2bfd133b3219'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Impregnering; oppløsning; vacuum'',:b, :c|| ''/'' || :d ||''/'', :e,''16baa04c-47e9-4f18-88ce-cd55439a58af'') '
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Hämning'',null,:b||''/'', :c,''013299c1-cea6-43ae-806d-7e369675bcbb'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Kompleksbinder'', null,:b||''/'', :c,''785978ea-a278-4c3c-9737-2de551822539'') '
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Konsolidering'',null,:b||''/'', :c,''c8abe8c8-9452-41f4-b997-635ad1d8c5d4'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Lackering'',null,:b||''/'', :c,''d6b1c1b2-7fa1-4a33-9e3f-e39a7725d495'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Laminering'', null, :b||''/'', :c,''d22c2ba8-b8bf-4fe2-99d8-74422b600792'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Limning'', null, :b||''/'', :c,''3e9262af-376b-4b21-9af7-16fada70cf99'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Neutralisering'', null, :b||''/'', :c,''085d5ceb-5dde-4095-aa60-a7013bfaa618'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Omkonservering'', null, :b||''/'', :c,''e51735de-c243-46ce-b1b9-9f0fd4c037e1'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Ytbehandling'', null, :b||''/'', :c,''81879e2a-3d7c-4b7a-9e97-9a4fe6949cd0'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Rengöring'',null,:b||''/'', :c,''d75e635b-8556-438f-99db-2e65934866d8'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values(:a, ''Rengöring; kemisk'',:b, :c || ''/'' || :d ||''/'', :e,''00ec5ac8-f63a-4bbf-81f6-c17363ff656c'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Rengöring; mekanisk; manuell'',:b, :c || ''/'' || :d ||''/'', :e,''0e9c39df-8a22-4293-a6f2-cdf1acab5066'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Rengöring;  mekanisk; sandblästring'',:b, :c || ''/'' || :d ||''/'', :e,''3efdfbe5-1c2b-465d-ba5a-ffbb12f16ac6'') '
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Rengöring; torr'',:b, :c || ''/'' || :d ||''/'', :e,''9dfce649-f627-4d77-8ce2-2f4a625a847e'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Rengöring; våt'',:b, :c || ''/'' || :d ||''/'', :e,''93ca47c1-2885-471f-a843-1279785c3673'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values  (:a, ''Rengöring; laser'',:b, :c || ''/'' || :d ||''/'', :e,''19cb4503-1829-48ec-a50e-143db1f04707'') '
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Retuschering'',null,:b||''/'', :c,''1358678a-d30c-4898-a43e-e491b03e248a'') '
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Torkning'',null,:b||''/'', :c,''6d1e70ac-aa80-4b8c-b2f4-1c6fc385058b'')'
            using nbehandlid,  nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Torkning; i lösningsmedel'',:b, :c || ''/'' || :d ||''/'', :e,''9783d957-a21f-474c-97c2-124626c0a9da'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Torkning; i luft'',:b, :c || ''/'' || :d ||''/'', :e,''34407612-bb27-463e-84be-9a76da11ef0d'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Torkning ; värmeskåp'',:b, :c || ''/'' || :d ||''/'', :e,''1a301c74-8393-4406-9b0b-74bc0233652b'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Torkning; frystorkning'',:b, :c || ''/'' || :d ||''/'', :e,''f01d2777-f860-4377-92d4-451d0b20e65e'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Urtvättning'',null,:b||''/'', :c,''5c5b4839-99ed-40f3-847e-38cb930ad1d7'')'
            using nbehandlid,  nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Urtvättning av salter, alkalisk sulfit'',:b, :c || ''/'' || :d ||''/'', :e,''69d09472-1fb1-4bfd-a47d-61558064269d'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Urtvättning av salter, elektrolys'', :b, :c || ''/'' || :d ||''/'', :e,''cffd2fcb-1321-47bc-9fc5-5219a3ea9081'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Urtvättning av salter, NaOH'',:b, :c || ''/'' || :d ||''/'', :e,''683e7b96-f0b2-4c89-870e-dc520db67bef'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Urtvättning av salter, natriumsesquikarbonat'',:b, :c || ''/'' || :d ||''/'', :e,''459150ca-020a-40d2-9f88-b97b4ad68359'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Urtvättning; vatten'',:b, :c || ''/'' || :d ||''/'', :e,''e5b80ed7-f8e0-48b4-8968-5ea0a9f5dd4c'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Urtvättning; vatten; destillerat'',:b, :c || ''/'' || :d ||''/'', :e,''88c20ca3-48d0-4580-a074-623c87d85983'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values
            (:a, ''Urtvättning; vatten; avjoniserat'',:b, :c || ''/'' || :d ||''/'', :e,''21bcc9fc-a2f9-4e21-9acb-9d9190872beb'')'
            using nbehandlid, nbehandlidParent,nbehandlidParent, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            nbehandlidParent := nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l
            (behandlingstypeid, navn, parent_id, m_path, hid_opprettet, uuid)
            values (:a, ''Vaxning'',null,:b||''/'', :c,''cc6ee919-fa79-43fd-9430-d7fc0cd7ada8'')'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, beskrivelse, m_path, hid_opprettet, uuid, merke, filterfeltid)
            values (:a, ''Dagliga rutiner'', null, :b||''/'', :c, getoracleuuid(), ''Y'', 3)'
            using nbehandlid,  nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, beskrivelse, m_path, hid_opprettet, uuid, merke, filterfeltid)
            values (:a, ''Periodiskt underhåll'', null, :b||''/'', :c, getoracleuuid(), ''Y'', 3)'
            using nbehandlid, nbehandlid, nnewhid;

            EXECUTE IMMEDIATE 'select behandlingtypeidseq.nextval from dual' INTO nbehandlid;
            EXECUTE IMMEDIATE 'insert into behandlingstype_l (behandlingstypeid, navn, beskrivelse, m_path, hid_opprettet, uuid, merke, filterfeltid)
            values (:a, ''Drift, löpande underhåll'', null, :b||''/'', :c, getoracleuuid(), ''Y'', 3)'
            using nbehandlid, nbehandlid, nnewhid;
          end if;
        end;
		End if;
	
    jobbnummer :=2;
	if jobnummer_in <= jobbnummer then
		declare
		  nnewhid number;
		  nmatidparent number;
		  nmatid number;
		begin
		  select hidseq.nextval into nnewhid from dual;
		  insert into hendelse (hid, signid, dato) values (nnewhid, 1, sysdate);

		  select materiale_LIDseq.nextval into nmatid from dual;
		  nmatidparent := nmatid;
		  insert into materiale_l (id, navn, parent_id, m_path, hid_opprettet, UUID, filterfelt, merke)
		  values
		   (nmatid, 'Baser', -1, nmatid || '/', nnewhid,'109711f9-963a-4668-857f-8e9b146b2063', 'BEHAN', 'Y');
		  
		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Natriumhydroksid',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'a693292d-2d9b-4fc9-82bf-fa463e545a22', 'BEHAN','Y');
		  
		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'natrium sesquikarbonat',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'f356f805-34c4-40a3-8687-1e30b9542245', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Natriumsulfitt',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'3eb700b5-d748-4e83-a93f-8509ba974bf2', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Ammoniakk',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'394d9be0-8029-4b2a-8ef3-e59fc5924e5a', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Natriumkarbonat',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'e5c049cc-559f-45ef-b247-79d4bf2157b4', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  nmatidparent := nmatid;
		  insert into materiale_l (id, navn, parent_id, m_path, hid_opprettet, UUID, filterfelt, merke)
		  values
		   (nmatid, 'Glassfiberduk', -1, nmatid || '/', nnewhid,'8e4ab331-d962-4caf-9905-7ad111f476ec', 'BEHAN', 'Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  nmatidparent := nmatid;
		  insert into materiale_l (id, navn, parent_id, m_path, hid_opprettet, UUID, filterfelt, merke)
		  values
		   (nmatid, 'Impregneringsmiddel/ lakk/ lim', -1, nmatid || '/', nnewhid,'fffba908-698e-4b90-8329-9426e8fd175f', 'BEHAN', 'Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Beva',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'758a7335-1e21-4ba3-8b7d-065342447890', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Cyanoacrylat',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'21e073cf-eb82-4b0c-9ade-2bd8720e412e', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Cellulosenitrat',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'d1dd810f-fdfb-4fd8-8aa4-baef7f55c340', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Ercalene (Nitrocellulose lakk)',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'cbd7e77d-9b45-4ee2-b2e2-44002337dfd3', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Epoxy, flytende',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'cc8f69ab-20b3-4b6c-8294-390812ae2575', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Epoxy, kitt',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'952313cf-bb14-4836-95fc-951626960412', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Frigilene (Nitrocellulose lakk)',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'d6113275-8c25-4d83-98c5-9bf7edb39fe9', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Incralakk (Paraloid B 44+ BTA)',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'46485951-c635-4ee8-b8aa-575fbffe65dd', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Karlsons lim',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'8798eb48-7d9e-4c49-ae93-f5afe9ec73a3', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Mowelith',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'114b3c10-6ca1-4752-a8a1-661c6885d44e', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Natriumcarboxylmetylcellulose (SCMC)',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'ef9999b1-90e8-4c47-a62d-8d66ef527d25', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Paraloid B-48N',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'685055b7-68b8-4974-83f8-40b27aeabd0c', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Paraloid B-67',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'544795e4-ac33-4bd5-9b79-93a7d8517565', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Paraloid B-72',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'58d6d865-ade5-40c2-b8ae-a8709d14d42e', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Plextol',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'d5feeab8-b160-4432-aad8-f8effdc979b2', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Polyvinylacetat(PVA)',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'2ceebb17-fd73-4f27-a342-12d873a5cf32', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Polyvinylklorid(PVC)',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'e17c1784-0bac-43b0-812c-90269044182c', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  nmatidparent := nmatid;
		  insert into materiale_l (id, navn, parent_id, m_path, hid_opprettet, UUID, filterfelt, merke)
		  values
		   (nmatid, 'Inhibitor', -1, nmatid || '/', nnewhid,'d11c1db2-65f8-4449-a3ad-af4768a42dd6', 'BEHAN', 'Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Benzotriazo (BTA)',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'009cf498-f0bb-47fd-b39d-bb1c9281dd98', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  nmatidparent := nmatid;
		  insert into materiale_l (id, navn, parent_id, m_path, hid_opprettet, UUID, filterfelt, merke)
		  values
		   (nmatid, 'Japanpapir', -1, nmatid || '/', nnewhid,'c3a6cca0-32ee-4096-b89f-68371c8d39c5', 'BEHAN', 'Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  nmatidparent := nmatid;
		  insert into materiale_l (id, navn, parent_id, m_path, hid_opprettet, UUID, filterfelt, merke)
		  values
		   (nmatid, 'Kompleksbinder', -1, nmatid || '/', nnewhid,'84e2b32b-ef22-4397-a0d4-6ca9042a6906', 'BEHAN', 'Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Etyldiamintetraeddiksyre (EDTA)',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'51ef7654-0f7f-4d47-9ff0-2f0627c2ef23', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Diethylenetriaminepentaacetate (DTPA)',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'c0df8206-832c-4cde-87f5-58f05ccc96ab', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  nmatidparent := nmatid;
		  insert into materiale_l (id, navn, parent_id, m_path, hid_opprettet, UUID, filterfelt, merke)
		  values
		   (nmatid, 'Impregneringsmiddel, organisk materiale', -1, nmatid || '/', nnewhid,'1238fde0-66be-4fcf-a01f-82dead6cbbe0', 'BEHAN', 'Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Glycerol',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'edd74364-2d3a-4841-a2a1-f85f5af299f1', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Mannitol',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'f5c2fd47-9665-4432-9d07-bcb28091c17b', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'PEG ',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'367338f6-a4d7-45e9-9993-db15fdcbe129', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'PEG 200',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'fbff3ce4-62d0-4372-8541-4f3ba64d9aa1', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'PEG 300',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'791230bb-8f65-4a72-baa1-3d68603e5570', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'PEG 400',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'48916cc7-07bc-427b-b47d-03e34c6690e7', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'PEG 1500',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'bb080bc1-7419-4199-a7e9-a98ea0a1617c', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'PEG 2000',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'4dfac54d-baa4-494b-b40a-c0279179bb3d', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'PEG 3000',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'c2539f3e-feea-47e2-b33f-83511877bb6c', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'PEG 4000',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'e8db713d-e59f-4849-9ce4-8404205d7aae', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Sukrose',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'d29251c3-5e47-4994-ad43-32a399e5c62c', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  nmatidparent := nmatid;
		  insert into materiale_l (id, navn, parent_id, m_path, hid_opprettet, UUID, filterfelt, merke)
		  values
		   (nmatid, 'Løsemidler', -1, nmatid || '/', nnewhid,'f0d6a32a-ee1e-4030-a9bb-3d239152f901', 'BEHAN', 'Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'aceton',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'0e88d786-4dd4-492a-bbb8-c275533db12a', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'butanol',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'14c4cac6-ad5a-4b9e-9329-c8fcb1e75e36', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'etanol',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'add9fdcd-981d-4613-94d2-01d6e6bdf5dd', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'metanol',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'7329290d-ba23-461c-a3cf-65b84cbcedae', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'propanol',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'a334f118-534b-40f0-a629-537eb831d1a2', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'rødsprit',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'dab7996e-d6e6-4c31-9fda-c83f290a973e', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'toluen',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'8582a73e-71b8-47c3-a128-ffac1fb72aa0', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'vann',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'7d76e39e-4a20-48e1-903d-bb755ef80e69', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'vann; avionisert',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'e42f6a0a-0e8e-4b68-a4b8-3093305f38f7', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'white spirit',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'4b5fa280-c983-4996-8add-b1a2e4cdde9e', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'xylen',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'53c1f2b2-4fef-4d91-b914-78897084ac7f', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  nmatidparent := nmatid;
		  insert into materiale_l (id, navn, parent_id, m_path, hid_opprettet, UUID, filterfelt, merke)
		  values
		   (nmatid, 'Syrer', -1, nmatid || '/', nnewhid,'405dc5db-b00e-449f-9d33-350d4ecefa08', 'BEHAN', 'Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Hydrogenperoksid',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'13bb1449-1499-4a43-897f-c26e5c33e1aa', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'oksalsyre',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'7e1213e7-f578-44cd-941e-3764f2fb3daf', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Salpetersyre',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'92e4ff91-67aa-4584-853c-c15187640cec', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'saltsyre',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'1b1e5865-bc0f-4117-bdc0-9d0a29cc26f4', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'sitronsyre',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'4bc2c8b5-0539-4967-a32c-a6e1698859d7', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Svovelsyre',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'d15e6e20-3991-4d88-a91d-58a3a7fc7cc5', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'eddiksyre',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'7e89d4aa-7d1d-446b-8d0f-10ed82a8156e', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'fosforsyre',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'c05bc85d-79a5-478f-8bdd-f3cbcfab2d15', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'fluorsyre',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'9ab3d76d-8610-4fe9-90b4-e1016c03a577', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'garvesyre?',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'5ec2bd12-30e7-4525-a45b-9f1ef61ade60', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  nmatidparent := nmatid;
		  insert into materiale_l (id, navn, parent_id, m_path, hid_opprettet, UUID, filterfelt, merke)
		  values
		   (nmatid, 'Vaskemiddel', -1, nmatid || '/', nnewhid,'6f8a26c7-bec2-4844-93ed-2bee093e3dce', 'BEHAN', 'Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Vaskemiddel; uionisk?; Synperonic A7',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'5c7e338e-36ad-4d58-809e-6b272dbe5538', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Vaskemiddel; uionisk?; Triton X-100',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'c23249ae-7c7e-422b-8586-d764c42a7681', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  nmatidparent := nmatid;
		  insert into materiale_l (id, navn, parent_id, m_path, hid_opprettet, UUID, filterfelt, merke)
		  values
		   (nmatid, 'Voks', -1, nmatid || '/', nnewhid,'ee6e6842-6092-4571-a8ee-07da8ef832c5', 'BEHAN', 'Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Voks, annen',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'1171b856-4cfa-4098-a63f-4b3e7d1e913c', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Voks, cosmolloid',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'2497cc84-9ce9-4af0-a5c8-37c8cee56a06', 'BEHAN','Y');

		  select materiale_LIDseq.nextval into nmatid from dual;
		  insert into materiale_l
		   (id, navn, parent_id, m_path, hid_opprettet, uuid, filterfelt,merke)
		  values
		   (nmatid, 'Voks, mikroskrystallinsk',nmatidParent, nmatidParent || '/' || nmatid ||'/', nnewhid,'57677b47-9155-4d02-bb69-104f42529dec', 'BEHAN','Y');
		  select materiale_LIDseq.nextval into nmatid from dual;
		  nmatidparent := nmatid;
		  insert into materiale_l (id, navn, parent_id, m_path, hid_opprettet, UUID, filterfelt, merke)
		  values
		   (nmatid, '?', -1, nmatid || '/', nnewhid,'', 'BEHAN', 'Y');
		end;
End if;
     	
	
	
	
	
	jobbnummer:= 3;
      commit;
	jobbnummer := 4;
         return 'OK';		   
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus21 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(21, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
end;
/

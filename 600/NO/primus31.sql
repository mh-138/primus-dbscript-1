create or replace function primus31(jobbnummer out number, jobnummer_in number)
	return varchar2
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
	Forklaring!!!
	*/

	begin
		jobbnummer:=1;
		if jobnummer_in <= jobbnummer then
      /*
      ISO 4217 Currency
      http://www.iso.org/iso/home/standards/currency_codes.htm
      */
      EXECUTE IMMEDIATE
        'CREATE TABLE valuta_l
          ( valuta_id NUMBER(8,0)
          , alpha_kode VARCHAR2(3 CHAR)
          , valuta VARCHAR2(500 CHAR)
          , CONSTRAINT pk_valuta_l PRIMARY KEY (valuta_id)
        ) TABLESPACE primusdt';
     
    End if;
    
  jobbnummer:=2; 
     executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(008, ''ALL'', ''Lek'')');
  jobbnummer:=3; 
    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(012, ''DZD'', ''Algerian Dinar'')');
  jobbnummer:=4; 
    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(032, ''ARS'', ''Argentine Peso'')');
  jobbnummer:=5; 
    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(036, ''AUD'', ''Australian Dollar'')');
  jobbnummer:=6; 
    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(044, ''BSD'', ''Bahamian Dollar'')');
  jobbnummer:=7; 
    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(048, ''BHD'', ''Bahraini Dinar'')');
  jobbnummer:=8; 
    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(050, ''BDT'', ''Taka'')');
  jobbnummer:=9; 
    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(051, ''AMD'', ''Armenian Dram'')');
  jobbnummer:=10; 
    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(052, ''BBD'', ''Barbados Dollar'')');
  jobbnummer:=11; 
    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(060, ''BMD'', ''Bermudian Dollar'')');
  jobbnummer:=12; 
    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(064, ''BTN'', ''Ngultrum'')');
  jobbnummer:=13; 
    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(068, ''BOB'', ''Boliviano'')');
  jobbnummer:=14; 
     executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(072, ''BWP'', ''Pula'')');
  jobbnummer:=15; 
     executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(084, ''BZD'', ''Belize Dollar'')');
  jobbnummer:=16; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(090, ''SBD'', ''Solomon Islands Dollar'')');
  jobbnummer:=17; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(096, ''BND'', ''Brunei Dollar'')');
  jobbnummer:=18; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(104, ''MMK'', ''Kyat'')');
  jobbnummer:=19; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(108, ''BIF'', ''Burundi Franc'')');
  jobbnummer:=20; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(116, ''KHR'', ''Riel'')');
  jobbnummer:=21; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(124, ''CAD'', ''Canadian Dollar'')');
  jobbnummer:=22; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(132, ''CVE'', ''Cabo Verde Escudo'')');
  jobbnummer:=23; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(136, ''KYD'', ''Cayman Islands Dollar'')');
  jobbnummer:=24; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(144, ''LKR'', ''Sri Lanka Rupee'')');
  jobbnummer:=25; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(152, ''CLP'', ''Chilean Peso'')');
  jobbnummer:=26; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(156, ''CNY'', ''Yuan Renminbi'')');
  jobbnummer:=27; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(170, ''COP'', ''Colombian Peso'')');
  jobbnummer:=28; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(174, ''KMF'', ''Comoro Franc'')');
  jobbnummer:=29; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(188, ''CRC'', ''Costa Rican Colon'')');
  jobbnummer:=30; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(191, ''HRK'', ''Croatian Kuna'')');
  jobbnummer:=31; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(192, ''CUP'', ''Cuban Peso'')');
  jobbnummer:=32; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(203, ''CZK'', ''Czech Koruna'')');
  jobbnummer:=33; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(208, ''DKK'', ''Danish Krone'')');
  jobbnummer:=34; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(214, ''DOP'', ''Dominican Peso'')');
  jobbnummer:=35; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(222, ''SVC'', ''El Salvador Colon'')');
  jobbnummer:=36; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(230, ''ETB'', ''Ethiopian Birr'')');
  jobbnummer:=37; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(232, ''ERN'', ''Nakfa'')');
  jobbnummer:=38; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(238, ''FKP'', ''Falkland Islands Pound'')');
  jobbnummer:=39; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(242, ''FJD'', ''Fiji Dollar'')');
  jobbnummer:=40; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(262, ''DJF'', ''Djibouti Franc'')');
  jobbnummer:=41; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(270, ''GMD'', ''Dalasi'')');
  jobbnummer:=42; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(292, ''GIP'', ''Gibraltar Pound'')');
  jobbnummer:=43; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(320, ''GTQ'', ''Quetzal'')');
  jobbnummer:=44; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(324, ''GNF'', ''Guinea Franc'')');
  jobbnummer:=45; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(328, ''GYD'', ''Guyana Dollar'')');
  jobbnummer:=46; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(332, ''HTG'', ''Gourde'')');
  jobbnummer:=47; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(340, ''HNL'', ''Lempira'')');
  jobbnummer:=48; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(344, ''HKD'', ''Hong Kong Dollar'')');
  jobbnummer:=49; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(348, ''HUF'', ''Forint'')');
  jobbnummer:=50; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(352, ''ISK'', ''Iceland Krona'')');
  jobbnummer:=51; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(356, ''INR'', ''Indian Rupee'')');
  jobbnummer:=52; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(360, ''IDR'', ''Rupiah'')');
  jobbnummer:=53; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(364, ''IRR'', ''Iranian Rial'')');
  jobbnummer:=54; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(368, ''IQD'', ''Iraqi Dinar'')');
  jobbnummer:=55; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(376, ''ILS'', ''New Israeli Sheqel'')');
  jobbnummer:=56; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(388, ''JMD'', ''Jamaican Dollar'')');
  jobbnummer:=57; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(392, ''JPY'', ''Yen'')');
  jobbnummer:=58; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(398, ''KZT'', ''Tenge'')');
  jobbnummer:=59; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(400, ''JOD'', ''Jordanian Dinar'')');
  jobbnummer:=60; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(404, ''KES'', ''Kenyan Shilling'')');
  jobbnummer:=61; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(408, ''KPW'', ''North Korean Won'')');
  jobbnummer:=62; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(410, ''KRW'', ''Won'')');
  jobbnummer:=63; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(414, ''KWD'', ''Kuwaiti Dinar'')');
  jobbnummer:=64; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(417, ''KGS'', ''Som'')');
  jobbnummer:=65; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(418, ''LAK'', ''Kip'')');
  jobbnummer:=66; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(422, ''LBP'', ''Lebanese Pound'')');
  jobbnummer:=67; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(426, ''LSL'', ''Loti'')');
  jobbnummer:=68; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(430, ''LRD'', ''Liberian Dollar'')');
  jobbnummer:=69; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(434, ''LYD'', ''Libyan Dinar'')');
  jobbnummer:=70; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(446, ''MOP'', ''Pataca'')');
  jobbnummer:=71; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(454, ''MWK'', ''Kwacha'')');
  jobbnummer:=72; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(458, ''MYR'', ''Malaysian Ringgit'')');
  jobbnummer:=73; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(462, ''MVR'', ''Rufiyaa'')');
  jobbnummer:=74; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(478, ''MRO'', ''Ouguiya'')');
  jobbnummer:=75; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(480, ''MUR'', ''Mauritius Rupee'')');
  jobbnummer:=76; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(484, ''MXN'', ''Mexican Peso'')');
  jobbnummer:=77; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(496, ''MNT'', ''Tugrik'')');
  jobbnummer:=78; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(498, ''MDL'', ''Moldovan Leu'')');
  jobbnummer:=79; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(504, ''MAD'', ''Moroccan Dirham'')');
  jobbnummer:=80; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(512, ''OMR'', ''Rial Omani'')');
  jobbnummer:=81;  executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(516, ''NAD'', ''Namibia Dollar'')');
  jobbnummer:=82; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(524, ''NPR'', ''Nepalese Rupee'')');
  jobbnummer:=83; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(532, ''ANG'', ''Netherlands Antillean Guilder'')');
  jobbnummer:=84; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(533, ''AWG'', ''Aruban Florin'')');
  jobbnummer:=85; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(548, ''VUV'', ''Vatu'')');
  jobbnummer:=86; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(554, ''NZD'', ''New Zealand Dollar'')');
  jobbnummer:=87; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(558, ''NIO'', ''Cordoba Oro'')');
  jobbnummer:=88; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(566, ''NGN'', ''Naira'')');
  jobbnummer:=89; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(578, ''NOK'', ''Norwegian Krone'')');
  jobbnummer:=90; executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(586, ''PKR'', ''Pakistan Rupee'')');
  jobbnummer:=91;   executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(590, ''PAB'', ''Balboa'')');
  jobbnummer:=92;    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(598, ''PGK'', ''Kina'')');
  jobbnummer:=93;    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(600, ''PYG'', ''Guarani'')');
  jobbnummer:=94;    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(604, ''PEN'', ''Nuevo Sol'')');
  jobbnummer:=95;    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(608, ''PHP'', ''Philippine Peso'')');
  jobbnummer:=96;    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(634, ''QAR'', ''Qatari Rial'')');
  jobbnummer:=97;    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(643, ''RUB'', ''Russian Ruble'')');
  jobbnummer:=98;    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(646, ''RWF'', ''Rwanda Franc'')');
  jobbnummer:=99;    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(654, ''SHP'', ''Saint Helena Pound'')');
  jobbnummer:=100;    executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(678, ''STD'', ''Dobra'')');
  jobbnummer:=101;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(682, ''SAR'', ''Saudi Riyal'')');
  jobbnummer:=102;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(690, ''SCR'', ''Seychelles Rupee'')');
  jobbnummer:=103;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(694, ''SLL'', ''Leone'')');
  jobbnummer:=104;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(702, ''SGD'', ''Singapore Dollar'')');
  jobbnummer:=105;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(704, ''VND'', ''Dong'')');
  jobbnummer:=106;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(706, ''SOS'', ''Somali Shilling'')');
  jobbnummer:=107;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(710, ''ZAR'', ''Rand'')');
  jobbnummer:=108;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(728, ''SSP'', ''South Sudanese Pound'')');
  jobbnummer:=109;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(748, ''SZL'', ''Lilangeni'')');
  jobbnummer:=110;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(752, ''SEK'', ''Swedish Krona'')');
  jobbnummer:=111;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(756, ''CHF'', ''Swiss Franc'')');
  jobbnummer:=112;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(760, ''SYP'', ''Syrian Pound'')');
  jobbnummer:=113;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(764, ''THB'', ''Baht'')');
  jobbnummer:=114;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(776, ''TOP'', ''Pa’anga'')');
  jobbnummer:=115;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(780, ''TTD'', ''Trinidad and Tobago Dollar'')');
  jobbnummer:=116;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(784, ''AED'', ''UAE Dirham'')');
  jobbnummer:=117;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(788, ''TND'', ''Tunisian Dinar'')');
  jobbnummer:=118;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(800, ''UGX'', ''Uganda Shilling'')');
  jobbnummer:=119;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(807, ''MKD'', ''Denar'')');
  jobbnummer:=120;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(818, ''EGP'', ''Egyptian Pound'')');
  jobbnummer:=121;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(826, ''GBP'', ''Pound Sterling'')');
  jobbnummer:=122;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(834, ''TZS'', ''Tanzanian Shilling'')');
  jobbnummer:=123;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(840, ''USD'', ''US Dollar'')');
  jobbnummer:=124;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(858, ''UYU'', ''Peso Uruguayo'')');
  jobbnummer:=125;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(860, ''UZS'', ''Uzbekistan Sum'')');
  jobbnummer:=126;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(882, ''WST'', ''Tala'')');
  jobbnummer:=127;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(886, ''YER'', ''Yemeni Rial'')');
  jobbnummer:=128;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(901, ''TWD'', ''New Taiwan Dollar'')');
  jobbnummer:=129;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(931, ''CUC'', ''Peso Convertible'')');
  jobbnummer:=130;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(932, ''ZWL'', ''Zimbabwe Dollar'')');
  jobbnummer:=131;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(934, ''TMT'', ''Turkmenistan New Manat'')');
  jobbnummer:=132;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(936, ''GHS'', ''Ghana Cedi'')');
  jobbnummer:=133;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(937, ''VEF'', ''Bolivar'')');
  jobbnummer:=134;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(938, ''SDG'', ''Sudanese Pound'')');
  jobbnummer:=135;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(940, ''UYI'', ''Uruguay Peso en Unidades Indexadas (URUIURUI)'')');
  jobbnummer:=136;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(941, ''RSD'', ''Serbian Dinar'')');
  jobbnummer:=137;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(943, ''MZN'', ''Mozambique Metical'')');
  jobbnummer:=138;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(944, ''AZN'', ''Azerbaijanian Manat'')');
  jobbnummer:=139;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(946, ''RON'', ''New Romanian Leu'')');
  jobbnummer:=140;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(947, ''CHE'', ''WIR Euro'')');
  jobbnummer:=141;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(948, ''CHW'', ''WIR Franc'')');
  jobbnummer:=142;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(949, ''TRY'', ''Turkish Lira'')');
  jobbnummer:=143;       executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(950, ''XAF'', ''CFA Franc BEAC'')');
  jobbnummer:=144;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(951, ''XCD'', ''East Caribbean Dollar'')');
  jobbnummer:=145;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(952, ''XOF'', ''CFA Franc BCEAO'')');
  jobbnummer:=146;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(953, ''XPF'', ''CFP Franc'')');
  jobbnummer:=147;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(955, ''XBA'', ''Bond Markets Unit European Composite Unit (EURCO)'')');
  jobbnummer:=148;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(956, ''XBB'', ''Bond Markets Unit European Monetary Unit (E.M.U.-6)'')');
  jobbnummer:=149;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(957, ''XBC'', ''Bond Markets Unit European Unit of Account 9 (E.U.A.-9)'')');
  jobbnummer:=150;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(958, ''XBD'', ''Bond Markets Unit European Unit of Account 17 (E.U.A.-17)'')');
  jobbnummer:=151;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(959, ''XAU'', ''Gold'')');
  jobbnummer:=152;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(960, ''XDR'', ''SDR (Special Drawing Right)'')');
  jobbnummer:=153;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(961, ''XAG'', ''Silver'')');
  jobbnummer:=154;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(962, ''XPT'', ''Platinum'')');
  jobbnummer:=155;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(963, ''XTS'', ''Codes specifically reserved for testing purposes'')');
  jobbnummer:=156;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(964, ''XPD'', ''Palladium'')');
  jobbnummer:=157;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(965, ''XUA'', ''ADB Unit of Account'')');
  jobbnummer:=158;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(967, ''ZMW'', ''Zambian Kwacha'')');
  jobbnummer:=159;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(968, ''SRD'', ''Surinam Dollar'')');
  jobbnummer:=160;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(969, ''MGA'', ''Malagasy Ariary'')');
  jobbnummer:=161;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(970, ''COU'', ''Unidad de Valor Real'')');
  jobbnummer:=162;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(971, ''AFN'', ''Afghani'')');
  jobbnummer:=163;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(972, ''TJS'', ''Somoni'')');
  jobbnummer:=164;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(973, ''AOA'', ''Kwanza'')');
  jobbnummer:=165;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(974, ''BYR'', ''Belarussian Ruble'')');
  jobbnummer:=166;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(975, ''BGN'', ''Bulgarian Lev'')');
  jobbnummer:=167;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(976, ''CDF'', ''Congolese Franc'')');
  jobbnummer:=168;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(977, ''BAM'', ''Convertible Mark'')');
  jobbnummer:=169;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(978, ''EUR'', ''Euro'')');
  jobbnummer:=170;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(979, ''MXV'', ''Mexican Unidad de Inversion (UDI)'')');
  jobbnummer:=171;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(980, ''UAH'', ''Hryvnia'')');
  jobbnummer:=172;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(981, ''GEL'', ''Lari'')');
  jobbnummer:=173;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(984, ''BOV'', ''Mvdol'')');
  jobbnummer:=174;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(985, ''PLN'', ''Zloty'')');
  jobbnummer:=175;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(986, ''BRL'', ''Brazilian Real'')');
  jobbnummer:=176;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(990, ''CLF'', ''Unidad de Fomento'')');
  jobbnummer:=177;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(994, ''XSU'', ''Sucre'')');
  jobbnummer:=178;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(997, ''USN'', ''US Dollar (Next day)'')');
  jobbnummer:=179;           executeScript(jobbnummer,jobnummer_in,'insert into valuta_l(valuta_id, alpha_kode, valuta) values(999, ''XXX'', ''The codes assigned for transactions where no currency is involved'')');


	jobbnummer:=180;           
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'CREATE TABLE oppgave
          (	objid NUMBER(8,0)
          , materialbruk VARCHAR2(4000 char)
          , CONSTRAINT pk_oppgave PRIMARY KEY (objid)
        ) TABLESPACE primusdt';
     
    End if;
	jobbnummer:=181;           
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'CREATE TABLE adm_h_kostnad
          ( objid NUMBER(8,0)
          , hid NUMBER(8,0)
          , kostnad NUMBER(10,0)
          , valuta_id NUMBER(8,0)
          , CONSTRAINT pk_adm_h_kostnad PRIMARY KEY (objid, hid)
          , CONSTRAINT FK_ADM_H_KOSTNAD_OBJID FOREIGN KEY (objid)
              REFERENCES ADM_HENDELSE (objid) ENABLE
          , CONSTRAINT FK_ADM_H_KOSTNAD_HENDELSE FOREIGN KEY (hid)
              REFERENCES HENDELSE (hid) ENABLE
          , CONSTRAINT FK_ADM_H_VALUTA_ID FOREIGN KEY (valuta_id)
              REFERENCES valuta_l (valuta_id) ENABLE
        ) TABLESPACE primusdt';
   
    End if;
	jobbnummer:=182;           
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'CREATE TABLE adm_h_tidsbruk
        ( objid NUMBER(8,0)
          , hid NUMBER(8,0)
          , timer NUMBER(10,2)
          , CONSTRAINT pk_adm_h_tidsbruk PRIMARY KEY (objid, hid)
          , CONSTRAINT FK_ADM_H_TIDSBRUK_OBJID FOREIGN KEY (objid)
        REFERENCES ADM_HENDELSE (objid) ENABLE
          , CONSTRAINT FK_ADM_H_TIDSBRUK_HENDELSE FOREIGN KEY (hid)
        REFERENCES HENDELSE (hid) ENABLE
        ) TABLESPACE primusdt';
      
    End if;
	jobbnummer:=183;
    if jobnummer_in <= jobbnummer then
      EXECUTE IMMEDIATE
        'ALTER TABLE adm_hendelse add
        ( hid_kostnad NUMBER(8,0)
        , hid_tidsbruk NUMBER(8,0)
        , CONSTRAINT fk_adm_hendelse_kostnad FOREIGN KEY (objid, hid_kostnad)
        REFERENCES adm_h_kostnad(objid, hid)
        , CONSTRAINT fk_adm_hendelse_tidsbruk FOREIGN KEY (objid, hid_tidsbruk)
        REFERENCES adm_h_tidsbruk(objid, hid))';
        End if;
     jobbnummer:=184;
	  commit;
	 jobbnummer:=185;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus31 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(31, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
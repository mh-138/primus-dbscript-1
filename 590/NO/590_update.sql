set serveroutput on;
sys.dbms_output.enable;
-- legger inn prosedyre som skal h�ndtere oppgradering.


-- legger inn alle prodedyrer
@@primus4.sql
@@primus5.sql
@@primus6.sql
@@primus7.sql
@@primus8.sql
@@primus9.sql
@@primus10.sql
@@primus11.sql
@@primus12.sql
@@primus13.sql
@@primus14.sql
@@primus15.sql

-- kj�rer prosedyre som skal h�ndtere oppgradering
declare
	resultat varchar2(1000 char);
begin
	resultat := oppgraderPrimus(4,15);

	if resultat = 'OK' then
		-- oppgradering uten feil, s� da settes riktig Primus versjon og�s.
		update environment set verdi = '9' where variabel = 'Minor';
		update environment set verdi = '0' where variabel = 'Release';
	else
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('****      Feil ved oppgradering      ****');
		dbms_output.put_line('**** Sjekk tabellen: PRIMUSDBVERSION ****');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
	end if;
exception
	when others then
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('****      Feil ved oppgradering      ****');
		dbms_output.put_line('**** Sjekk tabellen: PRIMUSDBVERSION ****');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
end;
/
5.90 Primus Data base Changes:

A variable name 'dbversion' is added in table Environment for version control.
 Insert Into Primus.Environment (Variabel, Verdi) Values ('dbversion', 590); 
 
 New table:
   Table name           Column
  TEKNIKKTYPE_L       TYPEID, BESKRIVELSE
  OBJEKT_TEKNIKK      OBJID, HID, NR, OBJID_SISTE, TYPEID, HIERARKISKEID, KOMMENTAR  
  MOTIV_DATERING_H    OBJID, HID, DATO_FRA, DATO_TIL, KOMMENTAR
  
 New columns:
   Table name           Column  
  JURPERSON          STATUS, GRUPPE
  STED               GRUPPE
  GRUPPE             OPT_OBJEKT, OPT_AKTOR, OPT_STED
  OBJEKT             HID_MOTIV_DATERING
  
  
Drop the following tables. 
 drop table PRIMUS.DEKORTEKNIKK_H;  
 drop table PRIMUS.DEKORTEKNIKK_L;
 drop table PRIMUS.TEKNIKK_H;
				
 
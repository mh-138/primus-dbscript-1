set serveroutput on;
sys.dbms_output.enable;
-- legger inn prosedyre som skal håndtere oppgradering.


-- legger inn alle prodedyrer
@@primus1.sql
@@primus2.sql
@@primus3.sql
@@primus4.sql
@@primus5.sql
@@primus6.sql
@@primus7.sql
@@primus8.sql
@@primus9.sql
@@primus10.sql
@@primus11.sql
@@primus12.sql
@@primus13.sql
@@primus14.sql
@@primus15.sql

-- kjører prosedyre som skal håndtere oppgradering
declare
	resultat varchar2(1000 char);
begin
	resultat := oppgraderPrimus(1,15);
  dbms_output.put_line('---- resultat -----:  ' || resultat);
	if resultat = 'OK' then
		-- oppgradering uten feil, så da settes riktig Primus versjon ogås.
		update environment set verdi = '9' where variabel = 'Minor';
		update environment set verdi = '0' where variabel = 'Release';
    commit;
	else
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('****      Feil ved oppgradering      ****');
		dbms_output.put_line('**** Sjekk tabellen: PRIMUSDBVERSION ****');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
	end if;
exception
	when others then
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('****      Feil ved oppgradering      ****');
		dbms_output.put_line('**** Sjekk tabellen: PRIMUSDBVERSION ****');
		dbms_output.put_line('****                                 ****');
		dbms_output.put_line('*****************************************');
		dbms_output.put_line('*****************************************');
end;
/
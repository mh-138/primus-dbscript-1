create or replace function primus12(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;

	/*
	Klargj�r dekorteknikk_h verdier for overf�ring til objekt_teknikk tabellen
	*/

	begin
	  jobbnummer := 1;
	  if jobnummer_in <= jobbnummer then
		execute immediate 'Update PRIMUS.DEKORTEKNIKK_H DH  Set DH.NEW_ID=(SELECT DL.NEW_ID
							 FROM PRIMUS.DEKORTEKNIKK_L DL	WHERE DL.ID=DH.HIERARKISKEID)';
      End if;
      jobbnummer:=2;
	  if jobnummer_in <= jobbnummer then	  
		execute immediate 'update PRIMUS.DEKORTEKNIKK_H DH set DH.NEW_NR=(SELECT max(OT.nr)+ DH.NR
		                   FROM OBJEKT_TEKNIKK OT WHERE OT.objid =dh.objid AND ot.hid=dh.hid )';
      End if;						   
 	  jobbnummer := 3;	 
	  if jobnummer_in <= jobbnummer then
		execute immediate 'update PRIMUS.DEKORTEKNIKK_H DH  set DH.NEW_NR=DH.NR WHERE DH.NEW_NR is NULL';
	  End if;	
      jobbnummer := 4;	  
	  if jobnummer_in <= jobbnummer then
			DECLARE
			  nObjid number;
			  nHid number;
			  nnewNr number;
			  nnewID number;
			  nobjid_siste number;
			  vkommentar varchar2(100 char);
			  strSQL varchar2(200 char);
			  TYPE EmpCurTyp is REF CURSOR;
			  v_emp_cursor EmpCurTyp;
			BEGIN
			  strSQL := 'Select OBJID, HID, NEW_NR, NEW_ID, OBJID_SISTE, KOMMENTAR From PRIMUS.DEKORTEKNIKK_H';		
		  
				  open v_emp_cursor for strSQL;
					Loop
					fetch v_emp_cursor into nobjid, nhid, nnewNr, nnewid, nobjid_siste,vkommentar;
					exit when v_emp_cursor%NOTFOUND;
					execute immediate 'Insert into PRIMUS.OBJEKT_TEKNIKK
								( OBJID
								, HID
								, NR
								, OBJID_SISTE
								, HIERARKISKEID
								, KOMMENTAR
								, TYPEID)
							Values
								( :a
								, :b
								, :c
								, :d
								, :e
								, :f
								, 1)' USING nobjid
										   , nhid
										   , nnewnr
										   , nobjid_siste
										   , nnewid
										   , vkommentar;
						END LOOP;
			END;
	  End if;	
      jobbnummer:=5;	  
	  if jobnummer_in <= jobbnummer then
		commit;
	  End if;
      jobbnummer:=6;	  	  
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus12 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(12, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;		
/

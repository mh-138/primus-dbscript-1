create or replace function primus15(jobbnummer out number, jobnummer_in number) 
	return varchar2
	AUTHID CURRENT_USER
	is
	uuid String(500);
	nuuid String(500);
	cnt NUMBER;
	grp NUMBER;	
	dbspraak varchar2(3 char);
begin
	dbms_output.enable;

	/*
		Oppretter samlinger for navn og sted. Oppretter egne samlinger for sted og navn hvis det er
		mer enn en samling i databasen.
		For databaser med en smaling s� utvides denne til � takle navn og sted.
	*/
	select svenskellernorskdatabase into dbspraak from dual;

	begin
     jobbnummer:=1;
 	  Select Count(*) into cnt  From PRIMUS.GRUPPE;	 	
	  IF cnt = 1 then
		BEGIN
			jobbnummer:=2;
			  if jobnummer_in <= jobbnummer then 				
				execute immediate 'Update PRIMUS.JURPERSON set GRUPPE= (Select Gruppeid From PRIMUS.GRUPPE)';
				execute immediate 'Update PRIMUS.STED      set GRUPPE= (Select Gruppeid From PRIMUS.GRUPPE)';
				execute immediate 'update primus.gruppe set opt_objekt = ''Y'', opt_aktor = ''Y'', opt_sted = ''Y''';        
			  End if;	
		END;
	  ELSE
		BEGIN
		  jobbnummer:=2;
		   if jobnummer_in <= jobbnummer then 	     		
			execute immediate 'Update PRIMUS.GRUPPE set OPT_OBJEKT=''Y''';
		   End if;	
		
			jobbnummer:=3;--- navnsamling
		   if jobnummer_in <= jobbnummer then 	     									
				Select sys_guid() into uuid  from dual;
				nuuid := substr(uuid,1,8)||'-'||substr(uuid,9,4)||'-'||substr(uuid,13,4)||'-'||substr(uuid,17,4)||'-'||substr(uuid,20);
				if dbspraak = 'NOR' then
					execute immediate 'Insert into Primus.GRUPPE(GRUPPEID, EIERID, BETEGNELSE, UUID, OPT_AKTOR)
								   values ((select max(GRUPPEID)+1 from primus.GRUPPE), 
								           (select EIERID from primus.EIER WHERE ROWNUM=1),
								            :aktor, :uuid, :opt_aktor)' using 'Navn', nuuid, 'Y';
				elsif dbspraak = 'SWE' then
					execute immediate 'Insert into Primus.GRUPPE(GRUPPEID, EIERID, BETEGNELSE, UUID, OPT_AKTOR)
								   values ((select max(GRUPPEID)+1 from primus.GRUPPE), 
								           (select EIERID from primus.EIER WHERE ROWNUM=1),
								            :aktor, :uuid, :opt_aktor)' using 'Namn', nuuid, 'Y';
				end if;
			end if;
			
		   jobbnummer:=4;
		   if jobnummer_in <= jobbnummer then 	     					
			execute immediate 'Update PRIMUS.JURPERSON set GRUPPE= (select max(GRUPPEID) from primus.GRUPPE)';
		   End if;	
		   
		   jobbnummer:=5;  --- Signatur_gruppe navnsamling---------
		   if jobnummer_in <= jobbnummer then 	     					               			 
				for i in (select Distinct signid, count(*) from primus.SIGNATUR_GRUPPE WHERE rollid in (2,3) group by Signid order by Signid)
				Loop
					Insert Into Primus.Signatur_Gruppe (Signid, Gruppeid, Rollid, Opt_Foto, Opt_Plassering, Opt_Utlaan, Opt_Utstilling, Opt_Mappe, Opt_Jp, Opt_Sted, Opt_Kjoop, Opt_Forsikring, Opt_Idnr, Opt_Publis, Opt_Samling)
					Select Distinct Signid, (select max(GRUPPEID) from primus.GRUPPE), 2, Opt_Foto, Opt_Plassering, Opt_Utlaan, Opt_Utstilling, Opt_Mappe, Opt_Jp, Opt_Sted, Opt_Kjoop, Opt_Forsikring, Opt_Idnr, Opt_Publis, Opt_Samling From Primus.Signatur_Gruppe
								Where  Signid= i.Signid and Rownum=1 ;
				 End Loop;					 				   
			End if;
			------------- End of signatur_gruppe -------------						
			jobbnummer:=6;---- Stedsamling
		   if jobnummer_in <= jobbnummer then 	     									
				Select sys_guid() into uuid  from dual;
				nuuid := substr(uuid,1,8)||'-'||substr(uuid,9,4)||'-'||substr(uuid,13,4)||'-'||substr(uuid,17,4)||'-'||substr(uuid,20);
				if dbspraak = 'NOR' then
					execute immediate 'Insert into Primus.GRUPPE(GRUPPEID, EIERID, BETEGNELSE, UUID, OPT_STED)
								   values ((select max(GRUPPEID)+1 from primus.GRUPPE), 
								           (select EIERID from primus.EIER WHERE ROWNUM=1),
										   :sted, :uuid, :opt_sted)' using 'Sted', nuuid, 'Y';
				elsif dbspraak = 'SWE' then
					execute immediate 'Insert into Primus.GRUPPE(GRUPPEID, EIERID, BETEGNELSE, UUID, OPT_STED)
								   values ((select max(GRUPPEID)+1 from primus.GRUPPE), 
								           (select EIERID from primus.EIER WHERE ROWNUM=1),
										   :sted, :uuid, :opt_sted)' using 'Ort', nuuid, 'Y';
				end if;
			end if;
			
		   jobbnummer:=7;
		   if jobnummer_in <= jobbnummer then 	     					
			execute immediate 'Update PRIMUS.STED set GRUPPE= (select max(GRUPPEID) from primus.GRUPPE)';
		   End if;	
		   
		   
		   jobbnummer:=8;  --- Signatur_gruppe stedsamling---------
		   if jobnummer_in <= jobbnummer then 	     					               			 
				for i in (select Distinct signid, count(*) from primus.SIGNATUR_GRUPPE WHERE rollid in (2,3) group by Signid order by Signid)
				Loop
					Insert Into Primus.Signatur_Gruppe (Signid, Gruppeid, Rollid, Opt_Foto, Opt_Plassering, Opt_Utlaan, Opt_Utstilling, Opt_Mappe, Opt_Jp, Opt_Sted, Opt_Kjoop, Opt_Forsikring, Opt_Idnr, Opt_Publis, Opt_Samling)
					Select Distinct Signid, (select max(GRUPPEID) from primus.GRUPPE), 2, Opt_Foto, Opt_Plassering, Opt_Utlaan, Opt_Utstilling, Opt_Mappe, Opt_Jp, Opt_Sted, Opt_Kjoop, Opt_Forsikring, Opt_Idnr, Opt_Publis, Opt_Samling From Primus.Signatur_Gruppe
								Where  Signid= i.Signid and Rownum=1 ;
				 End Loop;
			 END if;		 
				 ------------- End of signatur_gruppe -------------
			
		End;
	 End if;
			 

		jobbnummer:=11;
		commit;
		return 'OK';
	exception
		when others then
			rollback;
			SYS.DBMS_OUTPUT.PUT_LINE('Primus15 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(15, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/

create or replace function primus7(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER	
	is
	dbspraak varchar2(3 char);
begin
	dbms_output.enable;
	
	/*
	Forklaring!!!
	*/
	select svenskellernorskdatabase into dbspraak from dual;

	begin
	 jobbnummer:=1;
	  if jobnummer_in <= jobbnummer then	
		if dbspraak = 'NOR' then
			execute immediate 'Insert into PRIMUS.TEKNIKKTYPE_L (TYPEID,  BESKRIVELSE) VALUES (1, ''Dekor'')';
		elsif dbspraak = 'SWE' then
			execute immediate 'Insert into PRIMUS.TEKNIKKTYPE_L (TYPEID,  BESKRIVELSE) VALUES (1, ''Dekor'')';
		end if;
	  End if;
	 jobbnummer:= 2;	      
	  if jobnummer_in <= jobbnummer then		  
		if dbspraak = 'NOR' then
			execute immediate 'Insert into PRIMUS.TEKNIKKTYPE_L (TYPEID,  BESKRIVELSE) VALUES (2, ''Hoved'')';
		elsif dbspraak = 'SWE' then
			execute immediate 'Insert into PRIMUS.TEKNIKKTYPE_L (TYPEID,  BESKRIVELSE) VALUES (2, ''Huvud'')';
		end if;
	  End if;	
	 jobbnummer:= 3;	      
	  if jobnummer_in <= jobbnummer then	
		commit;
	  End if; 								 
	jobbnummer:= 4;	      
	   return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus7 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(7, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
	
end;
/
		
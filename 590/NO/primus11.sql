create or replace function primus11(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is
  sqltext varchar2(500 char);
  TYPE EmpCurTyp is REF CURSOR;
  v_emp_cursor EmpCurTyp;
  th dekorteknikk_l%ROWTYPE;
  
  nNewid number;
  nNewPID number;
  vNavn varchar2(250 char);
  vBeskr varchar2(500 char);
  nPARENTID NUMBER(6);          
  vM_PATH VARCHAR2(100 CHAR); 
  nHID_OPPRETTET NUMBER(8);
  nHID_SLETTET NUMBER(8);          
  vUUID VARCHAR2(40);       
  vMERKE varchar2(1 char);   
  vautoritet varchar2(10 char);
  vAUTORITET_STATUS           VARCHAR2(1 CHAR);
  vAUTORITET_DATASET          VARCHAR2(100 CHAR); 
  vAUTORITET_KILDE            VARCHAR2(100 CHAR); 
  vFILTERFELT                 VARCHAR2(5 CHAR);   
  
begin
	dbms_output.enable;

	/*
		Overf�rer alle verdier fra dekorteknikk_l til teknikk_l 
	*/

	begin

    
           sqltext := 'Select NEW_id
							, NAVN
							, BESKRIVELSE
							, TM_PATH
							, HID_OPPRETTET
							, NEW_PID
							, HID_SLETTET
							, UUID
							, MERKE
							, AUTORITET
							, AUTORITET_STATUS
							, AUTORITET_DATASET
							, AUTORITET_KILDE
							, FILTERFELT
						 From PRIMUS.DEKORTEKNIKK_L';
   	 jobbnummer:=1;
	  if jobnummer_in <= jobbnummer then
			DECLARE
			BEGIN
				open v_emp_cursor for sqltext ;
				Loop
				fetch v_emp_cursor INTO nnewid, vnavn, vbeskr, vm_path, nHID_OPPRETTET, nnewpid, nHID_SLETTET, vUUID, vMerke, 
										vAutoritet, vAutoritet_status, vAutoritet_dataset, vautoritet_kilde, vfilterfelt;
				exit when v_emp_cursor%NOTFOUND;
				execute immediate 'Insert into PRIMUS.TEKNIKK_L
							( ID
							, NAVN
							, PARENT_ID
							, BESKRIVELSE
							, M_PATH
							, HID_OPPRETTET
							, HID_SLETTET
							, UUID
							, MERKE
							, AUTORITET
							, AUTORITET_STATUS
							, AUTORITET_DATASET
							, AUTORITET_KILDE
							, FILTERFELT)
						values
							( :a
							, :b
							, :c
							, :d
							, :e
							, :f
							, :g
							, :h
							, :i
							, :j
							, :k
							, :l
							, :m
							, :n)' using nnewid
									   , vnavn 
									   , nnewpid
									   , vbeskr
									   , vm_path
									   , nHID_OPPRETTET
									   , nHID_SLETTET
									   , vUUID
									   , vMERKE
									   , vAUTORITET
									   , vAUTORITET_STATUS
									   , vAUTORITET_DATASET
									   , vAUTORITET_KILDE
									   , vFILTERFELT;
					End Loop;
			End;
		End if;
    jobbnummer:=2;
	  if jobnummer_in <= jobbnummer then
		commit;
	  End if;	
	jobbnummer:=3;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus11 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(11, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;

end;
/
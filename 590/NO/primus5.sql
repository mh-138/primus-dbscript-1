create or replace function primus5(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;
	
	/*
	Funksjonen legger inn de nye tabellene teknikktype_l og objekt_teknikk.
	*/
	
	begin
		jobbnummer:=1;
		if jobnummer_in <= jobbnummer then		
		  EXECUTE IMMEDIATE ('CREATE TABLE PRIMUS.TEKNIKKTYPE_L( TYPEID NUMBER (3,0), BESKRIVELSE VARCHAR2 (15 CHAR)
							  , CONSTRAINT PK_TEKNIKKTYPE_L PRIMARY KEY (TYPEID)) tablespace primusdt');		
        End if;							  
		
		
		jobbnummer:= 2;	      
		if jobnummer_in <= jobbnummer then		
		  EXECUTE IMMEDIATE ('CREATE TABLE PRIMUS.OBJEKT_TEKNIKK (OBJID NUMBER(8,0), HID NUMBER(8,0), NR NUMBER(3,0), 
							 OBJID_SISTE NUMBER(8,0), TYPEID NUMBER (3,0), HIERARKISKEID NUMBER(6,0), 
							 KOMMENTAR VARCHAR2(100 CHAR), CONSTRAINT PK_OBJEKT_TEKNIKK_L PRIMARY KEY (OBJID, HID, NR),
							 CONSTRAINT FK_OBJEKT_TEKNIKK_OBJID FOREIGN KEY (OBJID) REFERENCES PRIMUS.OBJEKT (OBJID),
							 CONSTRAINT FK_OBJEKT_TEKNIKK_OBJIDS FOREIGN KEY (OBJID_SISTE) REFERENCES PRIMUS.OBJEKT (OBJID),			  
							 CONSTRAINT FK_OBJEKT_TEKNIKK_HID FOREIGN KEY (HID) REFERENCES PRIMUS.HENDELSE (HID),
							 CONSTRAINT FK_OBJEKT_TEKNIKK_TID FOREIGN KEY (TYPEID) REFERENCES PRIMUS.TEKNIKKTYPE_L (TYPEID),			  
							 CONSTRAINT FK_OBJEKT_TEKNIKK_ID FOREIGN KEY (HIERARKISKEID) REFERENCES PRIMUS.TEKNIKK_L (ID)) tablespace primusdt');	
        End if; 							 
		jobbnummer:= 3;	    
		if jobnummer_in <= jobbnummer then
			execute immediate ('grant insert, update on primus.objekt_teknikk to pri_reg');
		end if;
  		jobbnummer := 4;
		return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus5 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(5, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
	
end;
/

create or replace function primus13(jobbnummer out number, jobnummer_in number) 
	return varchar2 
	AUTHID CURRENT_USER
	is
begin
	dbms_output.enable;
	
	/*
	Legger inn nye prosedyrer for oppdatering av verdier i objekt_teknikk, gruppering_h,
	pubiseringsnivaajp_h og motiv_datering_h.
	*/
	
	begin
	  jobbnummer:=1;		
	  if jobnummer_in <= jobbnummer then
		 EXECUTE IMMEDIATE ('create or replace 
			PROCEDURE teknikk_update (pobjid IN NUMBER, pnr IN NUMBER, oldh IN NUMBER, 
				   newh IN NUMBER, pteknikk IN NUMBER, pkomm IN STRING, ptypeid in number) IS
			BEGIN
			  IF oldh = newh THEN
				UPDATE OBJEKT_TEKNIKK
				   SET HIERARKISKEID = pteknikk, KOMMENTAR = pkomm, typeid=ptypeid
				   WHERE ((OBJID = pobjid) AND (NR = pnr) AND (HID = oldh));
			  ELSE
				UPDATE OBJEKT_TEKNIKK
				   SET OBJID_SISTE = NULL
				   WHERE ((OBJID = pobjid) AND (NR = pnr) AND (HID = oldh));
				INSERT INTO OBJEKT_TEKNIKK (OBJID, HID, NR, OBJID_SISTE, HIERARKISKEID, KOMMENTAR, TYPEID)
				   VALUES (pobjid, newh, pnr, pobjid, pteknikk, pkomm, ptypeid);
			  END IF;
			End Teknikk_Update;');
	  End if;		
      jobbnummer:=2;	 
	  if jobnummer_in <= jobbnummer then 
		EXECUTE IMMEDIATE ('create or replace PROCEDURE GRUPPERING_H_UPDATE (pobjid IN NUMBER, poldhid IN NUMBER, 
			 pnewhid IN NUMBER, ptypeid IN NUMBER, pkriteriumid in number, pkomm in string) IS  
			BEGIN
			  if poldHid =pnewhid then
				 UPDATE GRUPPERING_H  SET TYPEID = ptypeid, KRITERIUMID=pkriteriumid,  KOMMENTAR=pkomm
				 WHERE (objid = pobjid AND hid = poldhid);
			  ELSE
				 INSERT INTO GRUPPERING_H (OBJID, HID, TYPEID, KRITERIUMID, KOMMENTAR)
				   VALUES (pobjid, pnewhid, ptypeid, pkriteriumid, pkomm);
				 UPDATE OBJEKT SET HID_GRUPPERING = pnewhid WHERE (objid = pobjid);
			  END IF;
			End Gruppering_H_Update;');

	  End if;		
      jobbnummer:=3;	 
	  if jobnummer_in <= jobbnummer then 
			EXECUTE IMMEDIATE ('create or replace 
			PROCEDURE PUBLISERINGSNIVAAJP_H_update (pJPNR IN NUMBER, 
				pnewhid IN NUMBER,  ppid IN NUMBER) IS
			BEGIN
			 begin
				 INSERT INTO PUBLISERINGSNIVAAJURPERSON_H (JPNR, HID, PUBLISERINGSNIVAALISTEID) VALUES (pjpnr, pnewhid, ppid);
				 UPDATE JURPERSON SET HID_PUBLISERINGSNIVAA = pnewhid WHERE (JPNR = pjpnr); 
			  Exception
			   when others then
				UPDATE PUBLISERINGSNIVAAJURPERSON_H  SET PUBLISERINGSNIVAALISTEID = ppid WHERE (jpnr = pjpnr AND hid = pnewhid);
			  end;
			End Publiseringsnivaajp_H_Update;');
	End if;		
    jobbnummer:=4;	 
	if jobnummer_in <= jobbnummer then 		
		EXECUTE IMMEDIATE ('Create Or Replace Procedure Motiv_Datering_Update (Pobjid In Number, 
			Phid In Number, pfradato IN DATE, ptildato IN DATE, pkomm IN STRING) IS  
				  dummy NUMBER;
				BEGIN
				   Begin
					 Insert Into Motiv_Datering_H (Objid, Hid, DATO_FRA, DATO_TIL, Kommentar) Values (Pobjid, Phid, Pfradato, Ptildato, Pkomm);
					 UPDATE OBJEKT SET HID_MOTIV_DATERING = phid WHERE (OBJID = pobjid); 
  				   Exception
				   When Others Then
					Update Motiv_Datering_H  Set DATO_FRA = Pfradato, DATO_TIL=ptildato, KOMMENTAR=pkomm Where (objid = pobjid And Hid = Phid);
				  end;
				End Motiv_Datering_Update;');
	End if;		
	jobbnummer:=5;
	if jobnummer_in <= jobbnummer then 			
	   EXECUTE IMMEDIATE ('create or replace PROCEDURE PUBLISERINGSNIVAAOB_H_update (pobjid IN NUMBER, 
	    pnewhid IN NUMBER,  ppid IN NUMBER, pisObjekt IN NUMBER) IS
		BEGIN
		 begin
			 INSERT INTO PUBLISERINGSNIVAAOBJEKT_H  (OBJID, HID, PUBLISERINGSNIVAALISTEID) VALUES (pobjid, pnewhid, ppid);
			 if pisObjekt = 1 then
			  UPDATE OBJEKT SET HID_PUBLISERINGSNIVAA = pnewhid WHERE (OBJID = pobjid); 
			 else 
			  UPDATE UTSTILLING SET HID_PUBLISERINGSNIVAA = pnewhid WHERE (OBJID = pobjid); 			  
			 end if; 	  
		  Exception
		   when others then
			UPDATE PRIMUS.PUBLISERINGSNIVAAOBJEKT_H  SET PUBLISERINGSNIVAALISTEID = ppid WHERE (OBJID = pobjid AND hid = pnewhid);
		  end;
		End Publiseringsnivaaob_H_Update;');
	End if;
   jobbnummer:=6;
    if jobnummer_in <= jobbnummer then 			
      EXECUTE IMMEDIATE ('create or replace 
		PROCEDURE MATERIALETEKST_H_UPDATE (pobjid IN NUMBER, 
						pnewhid IN NUMBER,  pmaterialtekst IN STRING) IS
		BEGIN
		 begin
			 INSERT INTO PRIMUS.MATERIALETEKST_H (OBJID, HID, MATERIALETEKST) VALUES (pobjid, pnewhid, pmaterialtekst);
			 UPDATE PRIMUS.OBJEKT SET hid_materialetekst = pnewhid WHERE (OBJID = pobjid); 
		  Exception
		   when others then
			UPDATE PRIMUS.MATERIALETEKST_H  SET MATERIALETEKST = pmaterialtekst WHERE (OBJID = pobjid AND hid = pnewhid);
		  end;
		End MATERIALETEKST_H_UPDATE;');
	 End if;

   jobbnummer:=7;
    if jobnummer_in <= jobbnummer then 			
      EXECUTE IMMEDIATE ('create or replace 
		PROCEDURE TEKNIKKTEKST_H_UPDATE (pobjid IN NUMBER, 
						pnewhid IN NUMBER,  pTtekst IN STRING) IS
		BEGIN
		 begin
			 INSERT INTO PRIMUS.TEKNIKKTEKST_H (OBJID, HID, TEKNIKKTEKST) VALUES (pobjid, pnewhid, pTtekst);
			 UPDATE PRIMUS.OBJEKT SET hid_teknikktekst = pnewhid WHERE (OBJID = pobjid); 
		  Exception
		   when others then
			UPDATE PRIMUS.TEKNIKKTEKST_H  SET TEKNIKKTEKST = pTtekst WHERE (OBJID = pobjid AND hid = pnewhid);
		  end;
		End TEKNIKKTEKST_H_UPDATE;');
	 End if;	 
	 
   jobbnummer:=8;
	 if jobnummer_in <= jobbnummer then 			   
	   EXECUTE IMMEDIATE ('alter table PUBLISERINGSNIVAAOBJEKT_H modify(PUBLISERINGSNIVAALISTEID null)');  
	 End if;
	   
   jobbnummer:=9;	
	   if jobnummer_in <= jobbnummer then 			   
		EXECUTE IMMEDIATE ('alter table PUBLISERINGSNIVAAJURPERSON_H modify(PUBLISERINGSNIVAALISTEID null)');  
	   End if;   
   jobbnummer:=10;	   
 	 return 'OK';
	exception
		when others then
			SYS.DBMS_OUTPUT.PUT_LINE('Primus13 feilet! Jobbnummer: ' || jobbnummer);
			PRIMUSOPPGRADERINGSTOPP(13, 'FEIL', jobbnummer, 'Oppgradering feilet: ' || SQLERRM);
			return 'FEIL!';
	end;
	
end;	
/
	
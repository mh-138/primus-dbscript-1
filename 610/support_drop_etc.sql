alter table varemerke_h add ( jpobjid number( 8 ) );
alter table varemerke_h add ( rolleid number( 8 ) );
alter table varemerke_h add ( rollestatusid number( 4 ) );
alter table varemerke_h add constraint fk_jpobjid_jurperson foreign key ( jpobjid ) references jurperson( objid );
alter table varemerke_h add constraint fk_rolleid_rollejp foreign key ( rolleid ) references rollejp_l( rolleid );
alter table varemerke_h add constraint fk_rollestatusid_rollejp_sta foreign key ( rollestatusid ) references rollejpstatus_l( statusid );

alter table varemerke_h drop constraint fk_rollestatusid_rollejp_sta;
alter table varemerke_h drop constraint fk_rolleid_rollejp;
alter table varemerke_h drop constraint fk_jpobjid_jurperson;
alter table varemerke_h drop column rollestatusid;
alter table varemerke_h drop column rolleid;
alter table varemerke_h drop column jpobjid;





alter table superobjekt drop constraint FK_HID_PUBLINGR_PUBLINGRESS_H;
alter table superobjekt drop constraint FK_HID_PUBLOVER_PUBLOVER_H;
alter table superobjekt drop constraint FK_HID_PUBLTEKST_PUBLTEKST_H;


alter table utstilling drop constraint FK_UT_PUBLISERBARINGRESS;
alter table utstilling drop constraint FK_UT_PUBLISERBARTEKST;

alter table publiserbaringress_h add ( sprakkode varchar2( 3 char ) default 'NOR' constraint CK_PUBLISERBARINGR_H_SPRAKKODE not null );
alter table publiserbaringress_h drop constraint PK_PUBLISERBARINGRESS_H cascade;
alter table publiserbaringress_h add ( constraint PK_PUBLISERBARINGRESS_H primary key (objid, hid, sprakkode ) );

alter table publiserbaroverskrift_h add( sprakkode varchar2( 3 char ) default 'NOR' constraint CK_PUBLISERBAROVER_H_SPRAKKODE not null );
alter table publiserbaroverskrift_h drop constraint PK_PUBLISERBAROVERSKRIFT_H cascade;
alter table publiserbaroverskrift_h add ( constraint PK_PUBLISERBAROVERSKRIFT_H primary key (objid, hid, sprakkode ) );

alter table publiserbartekst_h add ( sprakkode varchar2( 3 char ) default 'NOR' constraint CK_PUBLISERBARTEKST_H_SPRAKKOD not null  );
alter table publiserbartekst_h drop constraint PK_PUBLISERBARTEKST_H cascade;
alter table publiserbartekst_h add ( constraint PK_PUBLISERBARTEKST_H primary key (objid, hid, sprakkode ) );

create table superobjekt_publiseringsinfo
  ( objid number( 8 )
  , sprakkode varchar2( 3 char ) default 'NOR' constraint CK_SUPEROBJEKT_PUBINFO_SPRKKDE not null
  , hid_publiserbaroverskrift number( 9 )
  , hid_publiserbaringress number( 9 )
  , hid_publiserbartekst number( 9 )
  , constraint pk_superobjekt_pubinfo primary key ( objid, sprakkode )
  , constraint fk_superobjekt_pubinfo_so foreign key ( objid ) references primus.superobjekt( objid )
  , constraint fk_superobjekt_pbinf_oversk foreign key ( objid, sprakkode, hid_publiserbaroverskrift ) references primus.publiserbaroverskrift_h ( objid, sprakkode, hid )
  , constraint fk_superobjekt_pbinf_ingress foreign key ( objid, sprakkode, hid_publiserbaringress ) references primus.publiserbaringress_h ( objid, sprakkode, hid )
  , constraint fk_superobjekt_pbinf_tekst foreign key ( objid, sprakkode, hid_publiserbartekst ) references primus.publiserbartekst_h ( objid, sprakkode, hid )
  ) tablespace primusdt;

insert into superobjekt_publiseringsinfo ( objid, hid_publiserbaroverskrift, hid_publiserbaringress, hid_publiserbartekst)
select objid, hid_publiserbaroverskrift, hid_publiserbaringress, hid_publiserbartekst from primus.superobjekt;

alter table superobjekt drop column hid_publiserbaroverskrift;
alter table superobjekt drop column hid_publiserbaringress;
alter table superobjekt drop column hid_publiserbartekst;






select * from all_constraints where constraint_type = 'R' and r_constraint_name='PK_PUBLISERBARINGRESS_H';


select sopi.sprakkode
     , Pt.Beskrivelse
  from superobjekt_publiseringsinfo sopi
inner join primus.publiserbartekst_h pt on pt.hid = sopi.hid_publiserbartekst and pt.objid = sopi.objid and pt.sprakkode = sopi.sprakkode  
 where hid_publiserbartekst is not null;


